import math

import matplotlib.pyplot as plt
import numpy as np

"""
A set of helpers to process and plot the method duration data logged to wandb.
"""


def get_earliest_start_time(dataframes):
    """Given dataframes with a 'Wall Time' column, representing the logging time and several method execution durations
    this function returns the earliest start time for the first method execution of all dataframes.
    """
    earliest_start = math.inf
    for df in dataframes:
        first_end = df["Wall Time"][0]
        time_columns = [
            col
            for col in df.columns
            if not "_MAX" in col
            and not "_MIN" in col
            and not "_step" in col
            and not "Wall" in col
        ]
        max_runtime = np.nanmax(df[time_columns].loc[0].values)
        if earliest_start > first_end - max_runtime:
            earliest_start = first_end - max_runtime
    return earliest_start


def get_last_end_time(dataframes):
    """Returns the largest Wall Time of the given dataframes."""
    last_end = 0
    for df in dataframes:
        last_wall_time = df["Wall Time"][df.shape[0] - 1]
        if last_end < last_wall_time:
            last_end = last_wall_time
    return last_end


def get_runtime(dataframes):
    return get_last_end_time(dataframes) - get_earliest_start_time(dataframes)


def get_time_dict(
    dataframe,
    offset,
    format_string_fn=lambda device_idx: f"Group: d{device_idx} - train_global_time",
    device_indices=range(5),
):
    """Returns a dictionary with device_idx as keys and a list of tuples for each device
    where each tuple consists of the method start time and duration.
    Example:
        {0: [(0.0002541542053222656, 4.316676139831543),
             (34.571627140045166, 2.5839357376098637),
             (60.37545871734619, 2.6242687702178955)],
         1: [(4.9257495403289795, 6.421962022781372),
             (37.768064975738525, 4.779199123382568),
             (63.61771893501282, 4.6916210651397705)]"""
    time_dict = {}
    for i in device_indices:
        time_dict[i] = []
    num_devices = len(device_indices)
    for idx, row in dataframe.iterrows():
        device_idx = device_indices[
            idx % num_devices
        ]  # in case there is only one device with index > 0
        time_dict[device_idx].append(
            (
                (row["Wall Time"] - offset) - row[format_string_fn(device_idx)],
                row[format_string_fn(device_idx)],
            )
        )
    return time_dict


def get_device_activity_plot(
    data, bar_height=2, num_devices=5, device_space=10, y_offset=5, colors=None
):
    """Plots the activity of the devices over time. The data is a list of dictionaries, where each dictionary
    corresponds to a method"""
    if colors is None:
        colors = ["blue", "darkorange", "lightgrey", "seagreen"]
    fig, ax = plt.subplots()
    num_methods = len(data)

    for method_idx, device_dict in enumerate(data):
        for device_idx in device_dict.keys():
            ax.broken_barh(
                device_dict[device_idx],  # (xmin, xwidth)
                (
                    device_space * device_idx
                    + bar_height * (method_idx - num_methods / 2)
                    + y_offset,
                    bar_height,
                ),  # (ymin, yheight)
                facecolors=f"{colors[method_idx % len(colors)]}",
            )

    # ax.set_xlim(left=350)
    # ax.set_ylim(bottom=0)
    ax.set_xlabel("seconds since start")
    ax.set_ylabel("Device ID")
    ax.set_yticks(
        [(x * device_space + y_offset) for x in range(num_devices)],
        labels=["d0", "d1", "d2", "d3", "d4"][:num_devices],
    )  # Modify y-axis tick labels
    ax.grid(True)  # Make grid lines visible

    return plt

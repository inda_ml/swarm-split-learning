import os
from collections import defaultdict

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import wandb
import math

# For plotting
STRATEGY_MAPPING = {
    "split": "Vanilla SL",
    "swarm_smart": "Swarm SL (Smart)",
    "swarm_seq": "Swarm SL (Seq)",
    "swarm_rand": "Swarm SL (Rand)",
    "swarm_max": "Swarm SL (Greedy)",
    "fed": "Vanilla FL",
    "psl_sequential__": "PSSL (Seq)",
    "fed___": "Vanilla FL",
    "swarm_sequential__": "Swarm SL (Seq)",
    "swarm_smart__": "Swarm SL (Smart)",
    "swarm_rand__": "Swarm SL (Rand)",
    "swarm_max_battery__": "Swarm SL (Greedy)",
    "split___": "Vanilla SL",
    "psl_sequential_static_at_resnet_decoderpth": "PSSL (Seq) AE Static",
    "psl_sequential__resnet_decoderpth": "PSSL (Seq) AE",
    "psl_sequential_static_at_": "PSSL (Seq) Static",
}

LABEL_MAPPING = {
    "num_devices": "Number of Devices",
    "runtime": "Runtime [s]",
    "round": "Round",
    "total network battery": "Total System Battery",
    "device battery": "Device Battery",
    "accuracy": "Accuracy",
    "train accuracy": "Train Accuracy",
    "val accuracy": "Validation Accuracy",
    "device": "Device",
}

DPI = 300


def scale_parallel_time(run_df, scale_factor=1.0):
    """
    Scales the time by the provided scale_factor.
    Args:
        run_df: The dataframe of the project
        scale_factor: (float) the factor to shorten time e.g. 2 halves the total time
    Returns:
        run_df: the dataframe with scaled timestamps
    """
    if scale_factor == 1:
        return run_df
    if "_timestamp" in run_df.columns:
        start_time = run_df["_timestamp"].min()
        for col in run_df.columns:
            if col.endswith(".start") or col.endswith(".end") or col == "_timestamp":
                run_df[col] = (run_df[col] - start_time) / scale_factor + start_time
            if col.endswith(".duration") or col == "_runtime":
                run_df[col] = run_df[col] / scale_factor
    return run_df


def get_scale_factors(group):
    """
    Determines the scale factor to account for parallelism.
    For each set of runs (i.e. one run of controller, d0, d1, ...), the time overhead introduced by running a
    parallel operation sequentially is determined and the resulting factor to scale the runs down as well.
    If no parallel operations were simulated, no time is deduced and the scale factor will equal 1.
    Args:
        group: the group of runs
    Returns:
        A list of factors to scale down each set of runs.
    """
    columns_to_count = [
        "parallel_client_train_time",
        "parallel_client_backprop_time",
        "parallel_client_model_update_time",
        "parallel_fed_time",
    ]
    scale_factors = []
    num_runs = len(next(iter(group.values())))
    max_runtime = [0] * num_runs
    elapsed_time = [0] * num_runs
    parallel_time = [0] * num_runs
    for name, runs in group.items():
        for i, run_df in enumerate(runs):
            if "_runtime" in run_df.columns:  # assure that run_df is not empty
                if run_df["_runtime"].max() > max_runtime[i]:
                    max_runtime[i] = run_df["_runtime"].max()
                for col_name in columns_to_count:
                    if f"{col_name}.parallel_time" in run_df.columns:
                        elapsed_time[i] += run_df[f"{col_name}.elapsed_time"].sum()
                        parallel_time[i] += run_df[f"{col_name}.parallel_time"].sum()
                if "parallel_client_eval_time.parallel_time" in run_df.columns:
                    if "evaluate_batch_time.duration" in run_df.columns:
                        elapsed_time[i] += run_df[
                            "parallel_client_eval_time.elapsed_time"
                        ].sum()
                        parallel_time[i] += (
                            run_df["parallel_client_eval_time.parallel_time"].sum()
                            - run_df["evaluate_batch_time.duration"].sum()
                        )  # evaluate batch time measured at server -> sequential either way
    for i, max_rt in enumerate(max_runtime):
        if max_rt > 0:
            scale_factors.append(max_rt / (max_rt - elapsed_time[i] + parallel_time[i]))
        else:
            scale_factors.append(1.0)
    return scale_factors


def save_dataframes(project_name, strategies, base_dir="./dataframes"):
    """
    Fetches the dataframes from wandb and saves them to the base_dir.
    Args:
        project_name: (str) the name of the project in wandb
        strategies: (list) a list of strategies to fetch the dataframes for
        base_dir: (str) the base directory to save the dataframes to
    Notes:
        - The wandb project is expected to have the following structure:
            project_name
            ├── split
            │   ├── job: train
            │   │   ├── d0
            │   │   ├── d1
            │   │   ├──  ...
            │   ├── job: test
            │   │   ├── d0
            │   │   ├── controller
            ├── swarm_smart
            │   ├── ...
        If an experiment was run multiple times, there may be several runs for each device_id.

        - The dataframes are saved in the following structure:
            base_dir
            ├── project_name
                ├── strategy e.g. split
                    ├── job e.g. train
                        ├── device_id e.g. d0
                            ├── df_0.csv
                            ├── df_1.csv
                            ├── ...
    """
    print(project_name)
    wandb.login()
    api = wandb.Api(timeout=120)
    run_groups = {}
    runs = api.runs(project_name)

    for strategy in strategies:
        for job in ["train", "test"]:
            filtered_runs = [
                run for run in runs if run.group == strategy and run.job_type == job
            ]
            runs_by_name = defaultdict(list)
            for run in filtered_runs:
                runs_by_name[run.name].append(run)
            run_groups[(strategy, job)] = runs_by_name
    # then fetch dataframes for each run
    print("downloading data")
    history_groups = {}
    for (strategy, job), group in run_groups.items():
        print(f"  {strategy} {job}")
        unscaled_runs = defaultdict(list)
        for name, runs in group.items():
            print(f"    {name}")
            for run in runs:
                history_df = pd.DataFrame(run.scan_history())
                unscaled_runs[name].append(history_df)
        # rescale if parallelism was only simulated
        if job == "train" and len(unscaled_runs) > 0:
            scale_factors = get_scale_factors(unscaled_runs)
            scaled_runs = defaultdict(list)
            for name, runs in unscaled_runs.items():
                for i, run in enumerate(runs):
                    scaled_runs[name].append(scale_parallel_time(run, scale_factors[i]))
            history_groups[(strategy, job)] = scaled_runs
        else:
            history_groups[(strategy, job)] = unscaled_runs
    # save dataframe
    print("saving data")
    for (strategy, job), group in history_groups.items():
        print(f"  {strategy} {job}")
        for device_id, runs in group.items():
            print(f"    {device_id}")
            for idx, run_df in enumerate(runs):
                dir_path = os.path.join(
                    base_dir, project_name, strategy, job, device_id
                )
                os.makedirs(dir_path, exist_ok=True)
                file_path = os.path.join(dir_path, f"df_{idx}.csv")
                run_df.to_csv(file_path)


def load_dataframes(project_name, base_dir="./dataframes"):
    """
    Loads saved dataframes from the given project.
    Args:
        project_name: (str) the name of the project folder
        base_dir: (str) the base directory to fetch the dataframes from
    Notes:
        - The dataframes are assumed to be stored in the following structure:
            base_dir
            ├── project_name
                ├── strategy e.g. split
                    ├── job e.g. train
                        ├── device_id e.g. d0
                            ├── df_0.csv
                            ├── df_1.csv
                            ├── ...
    """
    history_groups = {}
    project_path = os.path.join(base_dir, project_name)
    for root, dirs, files in os.walk(project_path):
        for file in files:
            if file.endswith(".csv"):
                # Extract keys from the directory structure
                path = os.path.relpath(root, base_dir)
                project_dir, strategy, job, device_id = path.split(os.sep)

                # Load dataframe from csv
                df = pd.read_csv(os.path.join(root, file), low_memory=False)

                # Add dataframe to dictionary
                if (strategy, job) not in history_groups:
                    history_groups[(strategy, job)] = {}
                if device_id not in history_groups[(strategy, job)]:
                    history_groups[(strategy, job)][device_id] = []
                history_groups[(strategy, job)][device_id].append(df)
    return history_groups


def get_total_flops(
    groups,
    total_model_flops,
    client_model_flops,
    strategy_autoencoder_mapping,
    batch_size=64,
):
    """
    Returns the total number of FLOPs for each group.
    Args:
        groups: The runs of one project, according to the structure of the wandb project
        total_model_flops: (int) the total number of FLOPs of the model
        client_model_flops: (int) the total number of FLOPs of the client model
    Returns:
        flops_per_group: (dict) the total number of FLOPs for each group
    """
    flops_per_group = {"strategy": [], "flops": []}
    for (strategy, job), group in groups.items():
        # determine model FLOPs depending on whether an autoencoder was used
        if strategy_autoencoder_mapping[strategy]:  # AE
            total_model_fw_flops = total_model_flops["ae"]
        else:  # no AE
            total_model_fw_flops = total_model_flops["plain"]
        total_model_bw_flops = 2 * total_model_flops["plain"]
        client_model_bw_flops = 2 * client_model_flops["plain"]
        if job == "train":
            flops = 0
            num_runs = 1  # avoid division by 0
            num_clients = len(group.items()) - 1  # minus controller
            for name, runs in group.items():
                if (
                    name != "controller"
                ):  # exclude controller to not count the FLOPs multiple times
                    num_runs = len(runs)  # runs have equal length anyway
                    for run_df in runs:
                        for col_name in run_df.columns:
                            if col_name == "train_accuracy.num_samples":
                                flops += run_df[col_name].sum() * (
                                    total_model_fw_flops + total_model_bw_flops
                                )  # 1x forward + 2x backward
                            if col_name == "val_accuracy.num_samples":
                                flops += (
                                    run_df[col_name].sum() * total_model_fw_flops
                                )  # 1x forward
                            if col_name == "adaptive_learning_threshold_applied":
                                # deduce client model flops twice as client backprop is avoided
                                if (
                                    run_df[col_name].dtype == "object"
                                ):  # if boolean values were logged
                                    # assumptions: compute avg number of samples per batch
                                    avg_samples_per_epoch = sum(
                                        run_df["train_accuracy.num_samples"].dropna()
                                    ) / len(
                                        run_df["train_accuracy.num_samples"].dropna()
                                    )
                                    avg_num_batches = (
                                        math.ceil(
                                            avg_samples_per_epoch
                                            / num_clients
                                            / batch_size
                                        )
                                        * num_clients
                                    )
                                    avg_samples_per_batch = (
                                        avg_samples_per_epoch / avg_num_batches
                                    )
                                    flops -= (
                                        len(run_df[col_name].dropna())
                                        * client_model_bw_flops
                                        * avg_samples_per_batch
                                    )
                                else:  # numbers of samples skipped are logged -> sum up
                                    flops -= (
                                        run_df[col_name].sum() * client_model_bw_flops
                                    )
            flops = flops / num_runs
            flops_per_group["strategy"].append(STRATEGY_MAPPING[strategy])
            flops_per_group["flops"].append(round(flops / 1000000000, 3))  # in GFLOPs
    return flops_per_group


def get_communication_overhead(groups):
    """
    Returns the total communication size in GB for each group.
    Args:
        groups: The runs of one project, according to the structure of the wandb project
    Returns:
        communication_sizes: (dict) the total communication size for each group
    """
    communication_sizes = {"strategy": [], "communicationsize": []}
    for (strategy, job), group in groups.items():
        if job == "train":
            payload_size = 0
            num_runs = 1
            for name, runs in group.items():
                num_runs = len(runs)
                for run_df in runs:
                    for col in run_df.columns:
                        if col.startswith("/Device/"):
                            payload_size += run_df[col].sum()
            payload_size = payload_size / num_runs
            communication_sizes["strategy"].append(STRATEGY_MAPPING[strategy])
            communication_sizes["communicationsize"].append(
                round(payload_size / 1000000000, 3)
            )  # in GB
    return communication_sizes


def get_test_accuracy(history_groups):
    """
    Returns the test accuracy for each group.
    Args:
        history_groups: The runs of one project, according to the structure of the wandb project
    Returns:
        test_acc: (dict) the test accuracy for each group
    """
    result = {"strategy": [], "testaccuracy": []}
    for (strategy, job), group in history_groups.items():
        if job == "test":
            result["strategy"].append(STRATEGY_MAPPING[strategy])
            test_acc = []
            for run_df in group["d0"]:
                test_acc.append(
                    run_df["test_accuracy.value"].max()
                )  # max to filter NaN
            result["testaccuracy"].append(
                round(sum(test_acc) / len(test_acc), 3)
            )  # take the average for multiple runs
    return result


def remaining_devices_per_round(history_groups):
    """
    Returns the remaining devices per round for each group.
    Args:
        history_groups: The runs of one project, according to the structure of the wandb project
    Returns:
        results: (dict) the remaining devices (list(int)) per round (list(int)) for each group
    """
    results = {}
    for (strategy, job), group in history_groups.items():
        if job == "train":
            round_cols, value_cols = [], []
            for run_df in group["controller"]:
                round_cols.append(list(run_df["remaining_devices.round"].dropna()))
                value_cols.append(list(run_df["remaining_devices.devices"].dropna()))
            # if multiple columns exist (i.e. multiple runs) average in each round and if one run was shorter, use last value
            max_rounds = []
            # get the maximum number of rounds
            # for federated learning sometimes zero devices are logged. In that case, break after the first zero
            for col in value_cols:
                zeros = [(idx, i) for idx, i in enumerate(col) if int(i) == 0]
                if len(zeros) > 1:
                    max_rounds.append(zeros[1][0])
                else:
                    max_rounds.append(len(col))
            max_rounds = max(max_rounds)
            round = range(0, max_rounds)
            remaining_devices = []
            for i in round:
                values_in_round = []
                for j in range(len(value_cols)):
                    if len(value_cols[j]) > i:
                        values_in_round.append(value_cols[j][i])
                    else:
                        values_in_round.append(value_cols[j][-1])
                remaining_devices.append(sum(values_in_round) / len(values_in_round))
                if (
                    remaining_devices[-1] == 0
                ):  # stop if there was no device left in any of the runs
                    break
            results[(strategy, job)] = (round, remaining_devices)
    return results


def plot_remaining_devices(devices_per_round, save_path=None):
    """
    Plots the remaining devices per round for each group.
    Args:
        devices_per_round: (dict) the remaining devices (list(int)) per round (list(int)) for each group
        save_path: (str) the path to save the plot to
    """

    plt.figure(dpi=DPI)
    num_rounds = [0]
    max_devices = []
    for (strategy, job), (rounds, num_devices) in devices_per_round.items():
        num_rounds.append(len(rounds))
        max_devices.append(max(num_devices))
        plt.plot(rounds, num_devices, label=f"{STRATEGY_MAPPING[strategy]}")
    plt.xticks(range(0, max(num_rounds) + 1, max(1, (max(num_rounds) + 1) // 8)))
    plt.yticks(
        range(0, int(max(max_devices) + 1), max(1, int((max(max_devices) + 1)) // 5))
    )
    plt.xlabel(LABEL_MAPPING["round"])
    plt.ylabel(LABEL_MAPPING["num_devices"])
    plt.legend()
    plt.tight_layout()
    if save_path is None:
        plt.show()
    else:
        plt.savefig(f"{save_path}.pdf", format="pdf")
        plt.savefig(f"{save_path}.png", format="png")
        plt.close()


def accuracy_over_epoch(history_groups, phase="train"):
    """
    Returns the accuracy over the epoch for each group.
    Args:
        history_groups: The runs of one project, according to the structure of the wandb project
        phase: (str) the phase to get the accuracy for, either "train" or "val"
    Returns:
        results: (dict) the accuracy (list(float)) per round (list(int)) for each group
    """
    results = {}
    for (strategy, job), group in history_groups.items():
        if job == "train":
            round_cols, value_cols = [], []
            for run_df in group["controller"]:
                round_cols.append(list(run_df[f"{phase}_accuracy.round"].dropna()))
                value_cols.append(list(run_df[f"{phase}_accuracy.value"].dropna()))
                # if multiple columns exist (i.e. multiple runs) average in each round and if one run was shorter, use last value
            max_rounds = max([int(col[-1]) for col in round_cols]) + 1
            mean_acc, round_no = [], []
            for i in range(max_rounds):
                single_run_accs = []
                for run_idx, value_col in enumerate(
                    value_cols
                ):  # round_cols should have same length
                    if (
                        round_cols[run_idx].count(i) > 0
                    ):  # check if values were logged in this round
                        round_idx = round_cols[run_idx].index(
                            i
                        )  # get the index of the round (could be less than the round number due to missing values)
                        single_run_accs.append(value_col[round_idx])
                # print(i, single_run_accs)
                if len(single_run_accs) > 0:
                    mean_acc.append(sum(single_run_accs) / len(single_run_accs))
                    round_no.append(i)

            results[(strategy, job)] = (round_no, mean_acc)
    return results


def accuracy_over_time(history_groups, phase="train"):
    """
    Returns the accuracy over time for each group. No averaging implemented yet if there are multiple runs per group!
    Args:
        history_groups: The runs of one project, according to the structure of the wandb project
        phase: (str) the phase to get the accuracy for, either "train" or "val"
    Returns:
        results: (dict) the accuracy (list(float)) per round (list(int)) for each group
    """
    results = {}
    for (strategy, job), group in history_groups.items():
        if job == "train":
            run_df = group["controller"][0]  # no averaging
            time_acc = run_df[[f"{phase}_accuracy.value", "_runtime"]].dropna()
            results[(strategy, job)] = (
                time_acc["_runtime"],
                time_acc[f"{phase}_accuracy.value"],
            )
    return results


def plot_accuracies(accuracies_per_round, save_path=None, phase="train"):
    """
    Plots the accuracy over the epoch for each group.
    Args:
        accuracies_per_round: (dict) the accuracy (list(float)) per round (list(int)) for each group
        save_path: (str) the path to save the plot to
    """
    plt.figure(dpi=DPI)
    num_rounds = [0]
    for (strategy, job), (rounds, accs) in accuracies_per_round.items():
        plt.plot(rounds, accs, label=f"{STRATEGY_MAPPING[strategy]}")
        num_rounds.append(len(rounds))
    plt.xticks(range(0, max(num_rounds) + 1, max(1, (max(num_rounds) + 1) // 8)))
    plt.xlabel(LABEL_MAPPING["round"])
    plt.ylabel(LABEL_MAPPING[f"{phase} accuracy"])
    plt.legend()
    plt.tight_layout()
    if save_path is None:
        plt.show()
    else:
        plt.savefig(f"{save_path}.pdf", format="pdf")
        plt.savefig(f"{save_path}.png", format="png")
        plt.close()


def plot_accuracies_over_time(accuracies_per_time, save_path=None, phase="train"):
    """
    Plots the accuracy over the time for each group.
    Args:
        accuracies_per_time: (dict) the accuracy (list(float)) per time (list(float)) for each group
        save_path: (str) the path to save the plot to
    """
    plt.figure(dpi=DPI)
    for (strategy, job), (time, accs) in accuracies_per_time.items():
        plt.plot(time, accs, label=f"{STRATEGY_MAPPING[strategy]}")
    plt.xlabel(LABEL_MAPPING["runtime"])
    plt.ylabel(LABEL_MAPPING[f"{phase} accuracy"])
    plt.legend()
    plt.tight_layout()
    if save_path is None:
        plt.show()
    else:
        plt.savefig(f"{save_path}.pdf", format="pdf")
        plt.savefig(f"{save_path}.png", format="png")
        plt.close()


def battery_over_time(history_groups, num_intervals=1000):
    """
    Returns the average battery over time for each group.
    Args:
        history_groups: The runs of one project, according to the structure of the wandb project
        num_intervals: (int) the number of intervals to sample the battery for (required for averaging or aggregation)
    Returns:
        results: (dict) the average battery (list(float)) per interval (list(int)) for each group
        max_runtime_per_group: (dict) the maximum runtime per group
    """
    results = {}
    max_runtime_per_group = {}
    for (strategy, job), group in history_groups.items():
        if job == "train":
            average_batteries_per_device_group = {}
            max_runtime = []
            # get min start and max end time per group
            start_times = [[] for _ in range(len(list(group.values())[0]))]
            end_times = [[] for _ in range(len(list(group.values())[0]))]
            for device_id, device_group in group.items():  # device_id e.g. "d0"
                if device_id != "controller":
                    for i, run_df in enumerate(device_group):
                        start_times[i].append(run_df["_timestamp"].min())
                        end_times[i].append(run_df["_timestamp"].max())
            min_start_time = [min(time_list) for time_list in start_times]
            max_end_time = [max(time_list) for time_list in end_times]
            for device_id, device_group in group.items():  # device_id e.g. "d0"
                if device_id != "controller":  # controller does not record batteries
                    avg_battery_per_interval = []
                    # chunk the runtime into intervals and sample the battery for each interval and run
                    for i, run_df in enumerate(device_group):
                        df = run_df.copy()
                        df["_timestamp"] = pd.to_datetime(df["_timestamp"], unit="s")
                        df = df.set_index("_timestamp")
                        max_runtime.append(
                            df["_runtime"].max()
                        )  # to match the battery values to runtime again
                        intervals = pd.date_range(
                            start=pd.to_datetime(min_start_time[i], unit="s"),
                            end=pd.to_datetime(max_end_time[i], unit="s"),
                            periods=num_intervals + 1,
                        )
                        df["interval"] = pd.cut(
                            df.index, bins=intervals, labels=np.arange(num_intervals)
                        )
                        # average per interval and fill empty values before/after with the first/last values
                        df_resampled = (
                            df.groupby("interval")["battery"].mean().bfill().ffill()
                        )
                        avg_battery_per_interval.append(df_resampled)
                    # take average battery for each device_id and append to batteries per group
                    average_batteries_per_device_group[device_id] = pd.concat(
                        avg_battery_per_interval, axis=1
                    ).mean(axis=1)
            max_runtime_per_group[(strategy, job)] = max(max_runtime)
            results[(strategy, job)] = average_batteries_per_device_group
    return results, max_runtime_per_group


def aggregated_battery_over_time(history_groups, num_intervals=100):
    """
    Returns the aggregated battery over time for each group.
    Args:
        history_groups: The runs of one project, according to the structure of the wandb project
        num_intervals: (int) the number of intervals to sample the battery for (required for averaging or aggregation)
    Returns:
        results: (dict) the aggregated battery (list(float)) per interval (list(int)) for each group
        max_runtime_per_group: (dict) the maximum runtime per group
    """
    batteries_per_device, max_runtime_per_group = battery_over_time(
        history_groups, num_intervals=num_intervals
    )
    results = {}
    for (strategy, job), series in batteries_per_device.items():
        results[(strategy, job)] = pd.concat(series.values(), axis=1).sum(axis=1)
    return results, max_runtime_per_group


def battery_over_epoch(history_groups, num_intervals=1000):
    """
    Returns the battery over the epoch for each group.
    Args:
        history_groups: The runs of one project, according to the structure of the wandb project
        num_intervals: (int) the number of intervals to sample the battery for (required for averaging or aggregation)
    Returns:
        battery_over_epoch: (dict) the battery (list(float)) per round (list(int)) for each group
    """
    batteries, _ = battery_over_time(history_groups, num_intervals)
    battery_over_epoch = {}
    for (strategy, job), group in history_groups.items():
        if job == "train":
            max_epochs = 0
            round_runtime = None
            device_batteries = {}
            for device_id, device_group in group.items():  # device_id e.g. "d0"
                if device_id == "controller":  # controller does rounds over time
                    for run_df in device_group:
                        end_idx = run_df[
                            run_df["remaining_devices.devices"] > 0
                        ].last_valid_index()
                        round_per_time_df = run_df[: end_idx + 1][
                            ["remaining_devices.round", "_runtime"]
                        ].dropna()
                        if (
                            round_per_time_df["remaining_devices.round"].max()
                            > max_epochs
                        ):
                            round_runtime = round_per_time_df
            for device_id, battery_series in batteries[(strategy, job)].items():
                round_rt = round_runtime.copy()
                round_rt["_runtime"] = pd.to_datetime(round_rt["_runtime"], unit="s")
                round_rt = round_rt.set_index("_runtime")
                time_index = pd.date_range(
                    start=round_rt.index.min(),
                    end=round_rt.index.max(),
                    periods=len(battery_series),
                )
                battery_series.index = time_index
                round_rt["battery"] = battery_series.asof(round_rt.index)
                device_batteries[device_id] = round_rt.set_index(
                    "remaining_devices.round"
                )
            battery_over_epoch[(strategy, job)] = device_batteries
    return battery_over_epoch


def aggregated_battery_over_epoch(history_groups, num_intervals=1000):
    """
    Returns the aggregated battery over the epoch for each group.
    Args:
        history_groups: The runs of one project, according to the structure of the wandb project
        num_intervals: (int) the number of intervals to sample the battery for (required for averaging or aggregation)
    Returns:
        results: (dict) the aggregated battery (list(float)) per round (list(int)) for each group
    """
    batteries_per_device = battery_over_epoch(
        history_groups, num_intervals=num_intervals
    )
    results = {}
    for (strategy, job), series in batteries_per_device.items():
        results[(strategy, job)] = pd.concat(series.values(), axis=1).sum(axis=1)
    return results


def plot_batteries_over_time(
    batteries_over_time, max_runtimes, save_path=None, aggregated=True
):
    """
    Plots the battery over time for each group.
    Args:
        batteries_over_time: (dict) the battery (list(float)) per interval (list(int)) for each group
        max_runtimes: (dict) the maximum runtime per group
        save_path: (str) the path to save the plot to
        aggregated: (bool) whether the battery is aggregated or not
    """
    if aggregated:
        plt.figure(dpi=DPI)
        plt.rcParams.update({"font.size": 13})
        for (strategy, job), series in batteries_over_time.items():
            runtime = max_runtimes[(strategy, job)]
            x_values = [
                runtime / series.size * i for i in range(1, series.size + 1)
            ]  # match series index with runtime
            y_values = series.values
            plt.plot(x_values, y_values, label=f"{STRATEGY_MAPPING[strategy]}")
        plt.xlabel(LABEL_MAPPING["runtime"])
        plt.ylabel(LABEL_MAPPING["total network battery"])
        plt.legend()
        plt.tight_layout()
        if save_path is None:
            plt.show()
        else:
            plt.savefig(f"{save_path}.pdf", format="pdf")
            plt.savefig(f"{save_path}.png", format="png")
            plt.close()
    else:
        for (strategy, job), series_dict in batteries_over_time.items():
            plt.figure(dpi=DPI)
            plt.rcParams.update({"font.size": 13})
            for device_id, series in series_dict.items():
                runtime = max_runtimes[(strategy, job)]
                x_values = [
                    runtime / series.size * i for i in range(1, series.size + 1)
                ]  # match series index with runtime
                y_values = series.values
                plt.plot(x_values, y_values, label=f"{device_id}")
            plt.xlabel(LABEL_MAPPING["runtime"])
            plt.ylabel(LABEL_MAPPING["device battery"])
            plt.legend()
            plt.tight_layout()
            if save_path is None:
                plt.show()
            else:
                plt.savefig(f"{save_path}_{strategy}.pdf", format="pdf")
                plt.savefig(f"{save_path}_{strategy}.png", format="png")
                plt.close()


def plot_batteries_over_epoch(batteries_over_epoch, save_path=None, aggregated=True):
    """
    Plots the battery over the epoch for each group.
    Args:
        batteries_over_epoch: (dict) the battery (list(float)) per round (list(int)) for each group
        save_path: (str) the path to save the plot to
        aggregated: (bool) whether the battery is aggregated or not
    """
    if aggregated:
        plt.figure(dpi=DPI)
        plt.rcParams.update({"font.size": 13})
        num_rounds = [0]
        for (strategy, job), series in batteries_over_epoch.items():
            x_values = series.index
            y_values = series.values
            plt.plot(x_values, y_values, label=f"{STRATEGY_MAPPING[strategy]}")
            num_rounds.append(len(series))
        plt.xticks(range(0, max(num_rounds) + 1, max(1, (max(num_rounds) + 1) // 8)))
        plt.ylabel(LABEL_MAPPING["total network battery"])
        plt.xlabel(LABEL_MAPPING["round"])
        plt.legend()
        plt.tight_layout()
        if save_path is None:
            plt.show()
        else:
            plt.savefig(f"{save_path}.pdf", format="pdf")
            plt.savefig(f"{save_path}.png", format="png")
            plt.close()
    else:
        for (strategy, job), series_dict in batteries_over_epoch.items():
            plt.figure(dpi=DPI)
            plt.rcParams.update({"font.size": 13})
            num_rounds = [0]
            for device_id, series in series_dict.items():
                x_values = series.index
                y_values = series.values
                plt.plot(x_values, y_values, label=f"{device_id}")
            num_rounds.append(len(series))
            plt.xticks(
                range(0, max(num_rounds) + 1, max(1, (max(num_rounds) + 1) // 8))
            )
            plt.ylabel(LABEL_MAPPING["device battery"])
            plt.xlabel(LABEL_MAPPING["round"])
            plt.legend()
            plt.tight_layout()
            if save_path is None:
                plt.show()
            else:
                plt.savefig(f"{save_path}_{strategy}.pdf", format="pdf")
                plt.savefig(f"{save_path}_{strategy}.png", format="png")
                plt.close()


def get_train_times(run_groups):
    results = {}
    for (strategy, job), group in run_groups.items():
        if job == "test":
            continue
        start_times = []
        server_train_times = {}
        client_train_times = {}
        for device_id, device_group in group.items():  # device_id e.g. "d0"
            if device_id != "controller":
                start_times.append(device_group[0]["_timestamp"][0])
        min_start_time = min(start_times)
        for device_id, device_group in group.items():  # device_id e.g. "d0"
            if device_id != "controller":
                run_df = device_group[0].copy()
                # create an empty dataframe in case device was never server
                server_train_times[device_id] = pd.DataFrame(columns=["start", "end"])
                if "train_global_time.start" in run_df.columns:
                    valid_train_global_times = run_df["train_global_time.start"] > 0
                    server_train_times[device_id] = (
                        run_df[valid_train_global_times][
                            ["train_global_time.start", "train_global_time.end"]
                        ]
                        .reset_index(drop=True)
                        .rename(
                            columns={
                                "train_global_time.start": "start",
                                "train_global_time.end": "end",
                            }
                        )
                        - min_start_time
                    )

                if "client_train_epoch_time.start" in run_df.columns:
                    valid_train_epoch_times = (
                        run_df["client_train_epoch_time.start"] > 0
                    )
                    valid_eval_times = run_df["client_evaluate_time.end"] > 0
                    train_epoch_start_times = run_df[valid_train_epoch_times][
                        ["client_train_epoch_time.start"]
                    ].reset_index(drop=True)
                    train_epoch_end_times = run_df[valid_train_epoch_times][
                        ["client_train_epoch_time.end"]
                    ].reset_index(drop=True)
                    eval_start_times = run_df[valid_eval_times][
                        ["client_evaluate_time.start"]
                    ].reset_index(drop=True)
                    eval_end_times = run_df[valid_eval_times][
                        ["client_evaluate_time.end"]
                    ].reset_index(drop=True)
                    client_train_times[device_id] = pd.concat(
                        [
                            pd.concat(
                                [train_epoch_start_times, train_epoch_end_times], axis=1
                            ).rename(
                                columns={
                                    "client_train_epoch_time.start": "start",
                                    "client_train_epoch_time.end": "end",
                                }
                            )
                            - min_start_time,
                            pd.concat(
                                [eval_start_times, eval_end_times], axis=1
                            ).rename(
                                columns={
                                    "client_evaluate_time.start": "start",
                                    "client_evaluate_time.end": "end",
                                }
                            )
                            - min_start_time,
                        ]
                    ).reset_index(drop=True)

                if "fed_train_time.start" in run_df.columns:
                    valid_fed_train_times = run_df["fed_train_time.start"] > 0
                    server_train_times[device_id] = (
                        run_df[valid_fed_train_times][
                            ["fed_train_time.start", "fed_train_time.end"]
                        ]
                        .reset_index(drop=True)
                        .rename(
                            columns={
                                "fed_train_time.start": "start",
                                "fed_train_time.end": "end",
                            }
                        )
                        - min_start_time
                    )
                    client_train_times[device_id] = server_train_times[device_id].copy()
        results[(strategy, job)] = server_train_times, client_train_times
    return results


def train_time_end(server_train_times, client_train_times):
    max_timestamp = 0
    for idx, (device_id, df) in enumerate(server_train_times.items()):
        if df["end"].max() > max_timestamp:
            max_timestamp = df["end"].max()
    for idx, (device_id, df) in enumerate(client_train_times.items()):
        if df["end"].max() > max_timestamp:
            max_timestamp = df["end"].max()
    return max_timestamp


def plot_batteries_over_time_with_activity(
    batteries_over_time, max_runtimes, training_times, save_path=None
):
    device_space = 200
    bar_height = 50
    y_offset = 10
    for (strategy, job), series_dict in batteries_over_time.items():
        server_train_times, client_train_times = training_times[(strategy, job)]
        xlim = (
            0,
            train_time_end(server_train_times, client_train_times) * 1.05,
        )  # set end 5% after last activity timestamp
        plt.figure(dpi=DPI)
        plt.rcParams.update({"font.size": 13})
        # battery_plot
        plt.subplot(2, 1, 1)
        for device_id, series in series_dict.items():
            runtime = max_runtimes[(strategy, job)]
            x_values = [
                runtime / series.size * i for i in range(1, series.size + 1)
            ]  # match series index with runtime
            y_values = series.values
            plt.plot(x_values, y_values, label=f"{device_id}")
        plt.ylabel(LABEL_MAPPING["device battery"])
        plt.legend()
        plt.tight_layout()
        plt.xlim(xlim)
        # plt.ylim(3000, 3800)
        # activity plot
        plt.subplot(2, 1, 2)
        for idx, (device_id, df) in enumerate(server_train_times.items()):
            x_values = []
            for _, row in df.iterrows():
                x_values.append((row["start"], row["end"] - row["start"]))
            plt.broken_barh(
                x_values,  # (xmin, xwidth)
                (device_space * idx + bar_height * 1 + y_offset, bar_height),
                label="Server" if idx == 0 else "",
            )  # avoid duplicate labels
        for idx, (device_id, df) in enumerate(client_train_times.items()):
            x_values = []
            for _, row in df.iterrows():
                x_values.append((row["start"], row["end"] - row["start"]))
            plt.broken_barh(
                x_values,  # (xmin, xwidth)
                (device_space * idx + bar_height * 2 + y_offset, bar_height),
                facecolors="darkorange",
                label="Client" if idx == 0 else "",
            )  # avoid duplicate labels
        plt.xlabel(LABEL_MAPPING["runtime"])
        plt.ylabel(LABEL_MAPPING["device"])
        plt.legend(loc="upper left")
        plt.tight_layout()
        plt.xlim(xlim)
        plt.yticks(
            [(device_space * x + 2 * bar_height + y_offset) for x in range(0, 5)],
            labels=["d0", "d1", "d2", "d3", "d4"][:5],
        )
        if save_path is None:
            plt.show()
        else:
            plt.savefig(f"{save_path}_{strategy}.pdf", format="pdf")
            plt.savefig(f"{save_path}_{strategy}.png", format="png")
            plt.close()


def plot_batteries_over_epoch_with_activity_at_time_scale(
    batteries_over_time, max_runtimes, training_times, save_path=None
):
    device_space = 200
    bar_height = 50
    y_offset = 10
    for (strategy, job), series_dict in batteries_over_time.items():
        server_train_times, client_train_times = training_times[(strategy, job)]
        train_times = pd.concat(
            [device_server_times for device_server_times in server_train_times.values()]
        ).sort_values("start")
        start_time, end_time = train_times["start"].min(), train_times["end"].max()
        xlim = (start_time, end_time)
        start_times = list(
            train_times["start"]
        )  # list(zip(train_times["start"], train_times["end"]))
        xticks = (
            [
                start_times[i]
                for i in range(0, len(start_times), max(1, len(start_times) // 8))
            ],
            [str(i) for i in range(0, len(start_times), max(1, len(start_times) // 8))],
        )
        plt.figure(dpi=DPI)
        plt.rcParams.update({"font.size": 13})
        # battery_plot
        plt.subplot(2, 1, 1)
        for device_id, series in series_dict.items():
            runtime = max_runtimes[(strategy, job)]
            x_values = [
                runtime / series.size * i for i in range(1, series.size + 1)
            ]  # match series index with runtime
            y_values = series.values
            plt.plot(x_values, y_values, label=f"{device_id}")
        plt.ylabel(LABEL_MAPPING["device battery"])
        plt.legend()
        plt.tight_layout()
        plt.xlim(xlim)
        plt.xticks(xticks[0], labels=xticks[1])
        # plt.ylim(3000, 3800)
        # activity plot
        plt.subplot(2, 1, 2)
        for idx, (device_id, df) in enumerate(server_train_times.items()):
            x_values = []
            for _, row in df.iterrows():
                x_values.append((row["start"], row["end"] - row["start"]))
            plt.broken_barh(
                x_values,  # (xmin, xwidth)
                (device_space * idx + bar_height * 1 + y_offset, bar_height),
                label="Server" if idx == 0 else "",
            )  # avoid duplicate labels
        for idx, (device_id, df) in enumerate(client_train_times.items()):
            x_values = []
            for _, row in df.iterrows():
                x_values.append((row["start"], row["end"] - row["start"]))
            plt.broken_barh(
                x_values,  # (xmin, xwidth)
                (device_space * idx + bar_height * 2 + y_offset, bar_height),
                facecolors="darkorange",
                label="Client" if idx == 0 else "",
            )  # avoid duplicate labels
        plt.xlabel(LABEL_MAPPING["round"])
        plt.ylabel(LABEL_MAPPING["device"])
        plt.legend(loc="upper left")
        plt.tight_layout()
        plt.xlim(xlim)
        plt.xticks(xticks[0], labels=xticks[1])
        plt.yticks(
            [(device_space * x + 2 * bar_height + y_offset) for x in range(0, 5)],
            labels=["d0", "d1", "d2", "d3", "d4"][:5],
        )
        if save_path is None:
            plt.show()
        else:
            plt.savefig(f"{save_path}_{strategy}.pdf", format="pdf")
            plt.savefig(f"{save_path}_{strategy}.png", format="png")
            plt.close()


def plot_batteries_over_epoch_with_activity_at_epoch_scale(
    batteries_over_epoch, training_times, save_path=None
):
    device_space = 200
    bar_height = 50
    y_offset = 10
    for (strategy, job), series_dict in batteries_over_epoch.items():
        server_train_times, client_train_times = training_times[(strategy, job)]
        train_times = pd.concat(
            [
                device_server_times.assign(device_id=device_id)
                for device_id, device_server_times in server_train_times.items()
            ]
        ).sort_values("start")

        # battery plot
        plt.figure(dpi=DPI)
        plt.rcParams.update({"font.size": 13})
        plt.subplot(2, 1, 1)
        num_rounds = []
        for device_id, series in series_dict.items():
            x_values = series.index
            y_values = series.values
            plt.plot(x_values, y_values, label=f"{device_id}")
        num_rounds.append(len(series))
        plt.xticks(range(0, max(num_rounds) + 1, max(1, (max(num_rounds) + 1) // 8)))
        plt.ylabel(LABEL_MAPPING["device battery"])
        plt.legend()
        plt.tight_layout()

        # device plot
        plt.subplot(2, 1, 2)
        if strategy == "fed":
            for device_idx, device_id in enumerate(server_train_times.keys()):
                for round_idx, row in train_times.iterrows():
                    current_device_id = row["device_id"]
                    if device_id == current_device_id:
                        plt.broken_barh(
                            [(round_idx, 1)],  # (xmin, xwidth)
                            (
                                device_space * device_idx + bar_height * 1 + y_offset,
                                bar_height,
                            ),
                        )

                        plt.broken_barh(
                            [(round_idx, 1)],
                            (
                                device_space * device_idx + bar_height * 2 + y_offset,
                                bar_height,
                            ),
                            facecolors="darkorange",
                        )
        else:
            server_train_times_with_device = []
            for idx, (device_id, df) in enumerate(server_train_times.items()):
                server_train_times_with_device += [
                    (row["start"], row["end"], device_id) for _, row in df.iterrows()
                ]
            server_train_times_with_device.sort(key=lambda x: x[0])

            client_train_rounds_with_device = {}
            for round, (start, end, device_id) in enumerate(
                server_train_times_with_device
            ):
                clients_in_round = []
                for client_device_idx, (client_device_id, df) in enumerate(
                    client_train_times.items()
                ):
                    if len(df[df["start"] > start]) > 0:
                        clients_in_round.append((client_device_idx, client_device_id))
                client_train_rounds_with_device[round] = [
                    (client_device_idx, client_device_id, len(clients_in_round))
                    for client_device_idx, client_device_id in clients_in_round
                ]

            for device_idx, device_id in enumerate(server_train_times.keys()):
                for round_idx, (_, _, current_device_id) in enumerate(
                    server_train_times_with_device
                ):
                    if device_id == current_device_id:
                        plt.broken_barh(
                            [(round_idx, 1)],  # (xmin, xwidth)
                            (
                                device_space * device_idx + bar_height * 1 + y_offset,
                                bar_height,
                            ),
                        )

                    for (
                        client_device_idx,
                        client_device_id,
                        num_clients_in_round,
                    ) in client_train_rounds_with_device[round_idx]:
                        plt.broken_barh(
                            [
                                (
                                    round_idx
                                    + (1 / num_clients_in_round) * client_device_idx,
                                    (1 / num_clients_in_round),
                                )
                            ],
                            (
                                device_space * client_device_idx
                                + bar_height * 2
                                + y_offset,
                                bar_height,
                            ),
                            facecolors="darkorange",
                        )
        plt.broken_barh(
            [(0, 0)], (0, 0), facecolors="darkorange", label="Client"
        )  # avoid duplicate labels
        plt.broken_barh([(0, 0)], (0, 0), label="Server")
        plt.xlabel(LABEL_MAPPING["round"])
        plt.ylabel(LABEL_MAPPING["device"])
        plt.legend(loc="upper left")
        plt.tight_layout()
        plt.xticks(range(0, max(num_rounds) + 1, max(1, (max(num_rounds) + 1) // 8)))
        plt.yticks(
            [(device_space * x + 2 * bar_height + y_offset) for x in range(0, 5)],
            labels=["d0", "d1", "d2", "d3", "d4"][:5],
        )
        if save_path is None:
            plt.show()
        else:
            plt.savefig(f"{save_path}_{strategy}.pdf", format="pdf")
            plt.savefig(f"{save_path}_{strategy}.png", format="png")
            plt.close()


def generate_plots(history_groups, project_name, base_path="./plots"):
    """
    Generates plots for the given history groups and saves them to the project_name folder.
    Args:
        history_groups: The runs of one project, according to the structure of the wandb project
        project_name: (str) the name of the project
        base_path: (str) the base path to save the plots to
    """
    project_path = f"{base_path}/{project_name}"
    os.makedirs(project_path, exist_ok=True)
    # batteries over time
    batteries_over_time, max_runtimes = aggregated_battery_over_time(
        history_groups, num_intervals=1000
    )
    plot_batteries_over_time(
        batteries_over_time,
        max_runtimes,
        save_path=f"{project_path}/total_batteries_over_time",
        aggregated=True,
    )

    batteries_over_time, max_runtimes = battery_over_time(
        history_groups, num_intervals=1000
    )
    plot_batteries_over_time(
        batteries_over_time,
        max_runtimes,
        save_path=f"{project_path}/batteries_over_time",
        aggregated=False,
    )

    # batteries over epoch
    batteries_over_epoch = aggregated_battery_over_epoch(
        history_groups, num_intervals=1000
    )
    plot_batteries_over_epoch(
        batteries_over_epoch,
        save_path=f"{project_path}/total_batteries_over_epoch",
        aggregated=True,
    )

    batteries_over_epoch = battery_over_epoch(history_groups, num_intervals=1000)
    plot_batteries_over_epoch(
        batteries_over_epoch,
        save_path=f"{project_path}/batteries_over_epoch",
        aggregated=False,
    )
    # remaining devices
    remaining_devices = remaining_devices_per_round(history_groups)
    plot_remaining_devices(
        remaining_devices, save_path=f"{project_path}/remaining_devices_per_epoch"
    )

    # accuracies
    train_accs = accuracy_over_epoch(history_groups, "train")
    plot_accuracies(
        train_accs, save_path=f"{project_path}/train_accuracy", phase="train"
    )

    val_accs = accuracy_over_epoch(history_groups, "val")
    plot_accuracies(val_accs, save_path=f"{project_path}/val_accuracy", phase="val")

    time_train_accs = accuracy_over_time(history_groups, "train")
    plot_accuracies_over_time(
        time_train_accs,
        save_path=f"{project_path}/train_accuracy_over_time",
        phase="train",
    )
    time_val_accs = accuracy_over_time(history_groups, "val")
    plot_accuracies_over_time(
        time_val_accs, save_path=f"{project_path}/val_accuracy_over_time", phase="val"
    )


def generate_metric_files(
    history_groups,
    project_name,
    total_model_flops,
    client_model_flops,
    strategy_autoencoder_mapping,
    base_path="./metrics",
    batch_size=64,
):
    """
    Generates metric file for the given history groups and saves them to the project_name folder.
    Args:
        history_groups: The runs of one project, according to the structure of the wandb project
        project_name: (str) the name of the project
        total_model_flops: (int) the total number of FLOPs of the model
        base_path: (str) the base path to save the metrics to
    """
    project_path = f"{base_path}/{project_name}"
    os.makedirs(project_path, exist_ok=True)

    # FLOPs, Communication Overhead, Test Accuracy
    test_acc = pd.DataFrame.from_dict(get_test_accuracy(history_groups)).set_index(
        "strategy"
    )
    comm_overhead = pd.DataFrame.from_dict(
        get_communication_overhead(history_groups)
    ).set_index("strategy")
    total_flops = pd.DataFrame.from_dict(
        get_total_flops(
            history_groups,
            total_model_flops,
            client_model_flops,
            strategy_autoencoder_mapping,
            batch_size,
        )
    ).set_index("strategy")
    df = pd.concat([test_acc, comm_overhead, total_flops], axis=1)
    df.to_csv(f"{project_path}/metrics.csv")

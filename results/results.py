import os

import hydra
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import wandb


def plot_communication_overhead(
    plot_groups: dict[str, list[float]],
    strategies: list[str],
    batteries: list[str],
    save_path: str,
):
    # adapted from https://matplotlib.org/stable/gallery/lines_bars_and_markers/barchart.html#sphx-glr-gallery-lines-bars-and-markers-barchart-py

    x = np.arange(len(strategies))  # the label locations
    bar_width = 0.25
    multiplier = 0

    fig, ax = plt.subplots(layout="constrained")

    for attribute, measurement in plot_groups.items():
        offset = bar_width * multiplier
        rects = ax.bar(x + offset, measurement, bar_width, label=attribute)
        ax.bar_label(rects, padding=3)
        multiplier += 1

    ax.set_xlabel("Learning algorithm")
    ax.set_ylabel("Sent + Received Data [GBytes]")
    ax.set_title("Communication overhead by algorithm")
    ax.set_xticks(
        x + bar_width * (len(batteries) - 1) / 2, strategies
    )  # center x ticks among groups
    ax.legend(loc="upper left", ncols=len(batteries))
    ax.set_ylim(
        0, max([item for sublist in plot_groups.values() for item in sublist]) * 1.15
    )  # add 15% margin to max value
    plt.tight_layout()
    plt.savefig(save_path)


def get_communication_overhead(
    history_group: dict[str, list[pd.DataFrame]], batteries: list[str]
) -> dict[str, list[float]]:
    communication_sizes = {}
    for name, group in history_group.items():
        payload_size = 0
        for history_df in group:
            for col in history_df.columns:
                if col.startswith("/Device/"):
                    payload_size += history_df[col].sum()
        communication_sizes[name] = payload_size / 1000000000  # in GB
    plot_groups = {battery: [] for battery in batteries}
    for idx, (method, size) in enumerate(communication_sizes.items()):
        plot_groups[batteries[idx % len(batteries)]].append(size)
    return plot_groups


def get_run_history(
    run_groups: dict[str, wandb.apis.public.Runs]
) -> dict[str, list[pd.DataFrame]]:
    """
    Retrieve the history of all runs in the run groups
    """
    history_groups = {}
    for name, group in run_groups.items():
        history = []
        for run in group:
            history_df = pd.DataFrame(run.scan_history())
            history.append(history_df)
        history_groups[name] = history
    return history_groups


def plot_batteries(history_groups: dict[str, list[pd.DataFrame]], save_path: str):
    for name, group in history_groups.items():
        plt.figure()
        for idx, run_df in enumerate(group):
            plt.step(
                run_df.dropna(subset=["battery"])["_runtime"],
                run_df.dropna(subset=["battery"])["battery"],
                label=f"Device {idx}",
            )

        plt.xlabel("Runtime [s]")
        plt.ylabel("Battery capacity")
        plt.title("Battery capacity over time")
        plt.ylim(bottom=0)
        plt.legend(loc="upper right")
        plt.tight_layout()
        plt.savefig(f"{save_path}/{name}_battery.png")


def retrieve_and_plot_results(
    strategies: list[str],
    batteries: list[str],
    entity: str = "swarmsl",
    project: str = "baseline",
):
    """
    Retrieve results from wandb and plot them
    """
    # fetch results by group
    wandb.login()
    print("retrieving results...")
    run_groups = get_run_groups(batteries, entity, project, strategies)
    history_groups = get_run_history(run_groups)

    # make data plottable
    print("plotting results...")
    plot_groups = get_communication_overhead(history_groups, batteries)

    # plot results
    cwd = os.getcwd()
    if not os.path.exists(f"{cwd}/{project}"):
        os.makedirs(f"{cwd}/{project}")  # create directory if it does not exist

    plot_communication_overhead(
        plot_groups,
        strategies,
        batteries,
        f"{cwd}/{project}/communication_overhead.png",
    )
    plot_batteries(history_groups, f"{cwd}/{project}")


def get_run_groups(
    batteries: list[str], entity: str, project: str, strategies: list[str]
) -> dict[str, wandb.apis.public.Runs]:
    api = wandb.Api(timeout=30)
    run_groups = {}
    for strategy in strategies:
        for battery in batteries:
            group = api.runs(
                f"{entity}/{project}", filters={"group": f"{strategy}_{battery}"}
            )
            run_groups[f"{strategy}_{battery}"] = group
    return run_groups


@hydra.main(config_path=None, version_base=None)  # use only for command line arguments
def main(cfg):
    batteries = cfg.batteries
    strategies = cfg.strategies
    entity = cfg.entity
    project = cfg.project
    retrieve_and_plot_results(strategies, batteries, entity, project)


if __name__ == "__main__":
    main()

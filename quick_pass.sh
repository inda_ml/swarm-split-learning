# shell script to start 2 devices with only a single batch of data each and run all controllers sequentially

conda init bash
conda activate slenv

# use trap to kill all processes in the subshell on ctrl-c
(trap 'kill 0' SIGINT;
  # testrun
  echo "Starting a quick test run with 2 devices"
  device_pids=()
  python3 main.py own_device_id=0 num_devices=2 experiment.max_epochs=1 experiment.max_rounds=1 experiment.load_single_batch_for_debugging=True wandb=False &
  device_pids+=($!)
  python3 main.py own_device_id=1 num_devices=2 experiment.max_epochs=1 experiment.max_rounds=1 experiment.load_single_batch_for_debugging=True wandb=False &
  device_pids+=($!)
  # run all controllers sequentially
  python3 main.py +method='fed' num_devices=2 experiment.max_epochs=1 experiment.max_rounds=1 wandb=False
  python3 main.py +method='split' num_devices=2 experiment.max_epochs=1 experiment.max_rounds=1 wandb=False
  python3 main.py +method='swarm_seq' num_devices=2 experiment.max_epochs=1 experiment.max_rounds=1 wandb=False
  python3 main.py +method='swarm_rand' num_devices=2 experiment.max_epochs=1 experiment.max_rounds=1 wandb=False
  python3 main.py +method='swarm_max' num_devices=2 experiment.max_epochs=1 experiment.max_rounds=1 wandb=False

  # kill device processes after controller has finished
  for pid in "${device_pids[@]}"; do
    kill "$pid"
  done
)
exit 1;

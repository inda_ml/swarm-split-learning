# shell script to run a test with a number 'n' of devices experiment 'e' and with model from round 'r'
# bash -i run_test.sh -n 2 -e "default_experiment" -r 1

conda init bash
conda activate slenv

# use trap to kill all processes in the subshell on ctrl-c
(trap 'kill 0' SIGINT;

# Parse arguments
while getopts ":n:e:r:" flag
do
    case "${flag}" in
        n) num_devices=${OPTARG};;
        e) experiment=${OPTARG};;
        r) round=${OPTARG};;
        *) echo "usage: $0 [-n] [-e] [-r]" >&2
          # invalid option passed
          exit 1 ;;
    esac
done

# Start devices
device_pids=()
echo "Starting $num_devices devices"
for (( i = 0; i < num_devices; i++ )); do
    python3 main.py own_device_id="$i" num_devices="$num_devices" battery="unlimited" experiment.job="test" &
    device_pids+=($!)
done

# run controller and wait for its termination
python3 main.py +method="test" +best_round="$round" num_devices="$num_devices" experiment="$experiment" battery="unlimited" experiment.job="test" &
controller_pid=$!
wait $controller_pid

# kill device processes after controller has finished
for pid in "${device_pids[@]}"; do
    kill "$pid"
done

)
exit 1;

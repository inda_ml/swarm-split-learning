# SwarmSplitLearning

In this repository, we introduce our own fully distributed variant of the well-known split learning algorithm.

## Wiki
See the [Wiki](https://git.rwth-aachen.de/inda_ml/swarm-split-learning/-/wikis/home) for a detailed documentation.

## Quick Start

For a faster installation, install the libmamba-solver first before creating the conda environment:

```bash
conda install -n base conda-libmamba-solver
conda config --set solver libmamba
```

Otherwise, the default conda solver will be used, which may take forever.

Update the file path to your desired directory for the environment by changing the value of `prefix:` at the end of
the [environment.yml](environment.yml) file.
Then run:

```bash
conda env create -f environment.yml
conda activate [ENV_NAME]
```

For tracking experiments using Weights and Biases, place your API key without any spaces in
the [wandb_key.txt](wandb_key.txt) file and make sure that `wandb: True` is set in
the [wandb config](edml/config/wandb.yaml).
Otherwise, metrics etc. will be printed to the console.

If you plan to commit to this repository, please install **pre-commit** for a consistent code formatting upon committing.
Therefore, run the following command in the repo:

```bash
 pre-commit install
```

Optionally, for formatting without committing, you may run:
```bash
pre-commit run --all-files
```



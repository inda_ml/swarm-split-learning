from torch import nn
from torch.autograd import Variable


class ClientWithAutoencoder(nn.Module):
    def __init__(self, model: nn.Module, autoencoder: nn.Module):
        super().__init__()
        self.model = model
        self.autoencoder = autoencoder.requires_grad_(False)
        self.trainable_layers_output = (
            None  # needed to continue the backprop skipping the AE
        )

    def forward(self, x):
        self.trainable_layers_output = self.model(x)
        return self.autoencoder(self.trainable_layers_output)


class ServerWithAutoencoder(nn.Module):
    def __init__(self, model: nn.Module, autoencoder: nn.Module):
        super().__init__()
        self.model = model
        self.autoencoder = autoencoder.requires_grad_(False)
        self.trainable_layers_input = (
            None  # needed to stop backprop before AE and send the gradients to client
        )

    def forward(self, x):
        self.trainable_layers_input = self.autoencoder(x)
        self.trainable_layers_input = Variable(
            self.trainable_layers_input, requires_grad=True
        )
        return self.model(self.trainable_layers_input)

from torch import nn

from edml.helpers.model_splitting import split_network_at_layer
from edml.models.provider.base import ModelProvider


class CutLayerModelProvider(ModelProvider):
    def __init__(self, model: nn.Module, cut_layer: int):
        self.cut_layer = cut_layer

        client, server = split_network_at_layer(network=model, cut_layer=cut_layer)
        super().__init__(client=client, server=server)

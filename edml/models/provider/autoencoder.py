from torch import nn

from edml.models.autoencoder import ClientWithAutoencoder, ServerWithAutoencoder
from edml.models.provider.base import ModelProvider


class AutoencoderModelProvider(ModelProvider):
    def __init__(
        self, model_provider: ModelProvider, encoder: nn.Module, decoder: nn.Module
    ):
        inner_client, inner_server = model_provider.models

        client_with_encoder = ClientWithAutoencoder(inner_client, encoder)
        server_with_decoder = ServerWithAutoencoder(inner_server, decoder)

        super().__init__(client=client_with_encoder, server=server_with_decoder)

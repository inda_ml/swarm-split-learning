import torch
from torch import nn


class SerializedModel(nn.Module):
    def __init__(self, model: nn.Module, path: str):
        super().__init__()
        model.load_state_dict(torch.load(path, map_location="cpu"))
        self.model = model

    def forward(self, x):
        return self.model(x)

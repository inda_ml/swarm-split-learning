from torch import nn


def has_autoencoder(model: nn.Module):
    if hasattr(model, "model") and hasattr(model, "autoencoder"):
        return True
    return False


def add_optimizer_params_function(model: nn.Module):
    # exclude AE params
    if has_autoencoder(model):

        def get_optimizer_params(model: nn.Module):
            return model.model.parameters

        model.get_optimizer_params = get_optimizer_params(model)
    else:

        def get_optimizer_params(model: nn.Module):
            return model.parameters

        model.get_optimizer_params = get_optimizer_params(model)
    return model


class ModelProvider:
    def __init__(self, client: nn.Module, server: nn.Module):
        self._client = add_optimizer_params_function(client)
        self._server = add_optimizer_params_function(server)

    @property
    def models(self) -> tuple[nn.Module, nn.Module]:
        return self._client, self._server

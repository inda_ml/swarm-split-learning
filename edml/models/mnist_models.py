import torch
import torch.nn as nn
import torch.nn.functional as F


# Simple example adapted from https://github.com/pytorch/examples/blob/main/mnist/main.py


class ClientNet(nn.Module):
    """
    Client-side neural network for the MNIST dataset.

    Implementation based on https://github.com/pytorch/examples/blob/main/mnist/main.py.
    """

    def __init__(self):
        super(ClientNet, self).__init__()
        self.conv1 = nn.Conv2d(1, 16, 3, 1)
        self.conv2 = nn.Conv2d(16, 64, 3, 1)

    def forward(self, x):
        x = self.conv1(x)
        x = F.relu(x)
        x = self.conv2(x)
        x = F.relu(x)
        x = F.max_pool2d(x, 2)
        return x


class ServerNet(nn.Module):
    """
    Server-side neural network for the MNIST dataset.

    Implementation based on https://github.com/pytorch/examples/blob/main/mnist/main.py.
    """

    def __init__(self):
        super(ServerNet, self).__init__()
        self.conv3 = nn.Conv2d(64, 128, 3, 1)
        self.dropout1 = nn.Dropout(0.25)
        self.dropout2 = nn.Dropout(0.5)
        self.fc1 = nn.Linear(12800, 300)
        self.fc2 = nn.Linear(300, 10)

    def forward(self, x):
        x = self.dropout1(x)
        x = self.conv3(x)
        x = F.relu(x)
        x = torch.flatten(x, 1)
        x = self.fc1(x)
        x = F.relu(x)
        x = self.dropout2(x)
        x = self.fc2(x)
        output = F.log_softmax(x, dim=1)
        return output

from torch import nn


class Decoder(nn.Module):
    """
    decoder model
    """

    def __init__(self):
        super(Decoder, self).__init__()
        self.t_convx = nn.ConvTranspose2d(4, 8, 1, stride=1)
        self.t_conva = nn.ConvTranspose2d(8, 16, 1, stride=1)
        self.t_convb = nn.ConvTranspose2d(16, 64, 1, stride=1)

    def forward(self, x):
        x = self.t_convx(x)
        x = self.t_conva(x)
        x = self.t_convb(x)
        return x


class Encoder(nn.Module):
    """
    encoder model
    """

    def __init__(self):
        super(Encoder, self).__init__()
        self.conva = nn.Conv2d(64, 16, 3, padding=1)
        self.convb = nn.Conv2d(16, 8, 3, padding=1)
        self.convc = nn.Conv2d(8, 4, 3, padding=1)

    def forward(self, x):
        x = self.conva(x)
        x = self.convb(x)
        x = self.convc(x)
        return x

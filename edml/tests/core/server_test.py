import unittest
from collections import OrderedDict
from copy import copy
from typing import Tuple, Any
from unittest.mock import Mock

import torch.utils.data
from omegaconf import DictConfig
from torch import nn, tensor
from torch.utils.data import DataLoader, Dataset, TensorDataset

from edml.core.battery import Battery
from edml.core.client import DeviceClient
from edml.core.device import Device
from edml.core.server import DeviceServer
from edml.helpers.logging import SimpleLogger


class ClientModel(nn.Module):
    def __init__(self):
        super().__init__()
        self.layer = nn.Linear(1, 1)
        self.output = nn.ReLU()

    def forward(self, x):
        return self.output(self.layer(x))


class ServerModel(nn.Module):
    def __init__(self):
        super().__init__()
        self.layer = nn.Linear(1, 2)
        # self.layer.weight = tensor([[-0.6857]])
        self.output = nn.Softmax(dim=1)

    def forward(self, x):
        return self.output(self.layer(x))


class ToyDataset(Dataset):
    def __init__(self, data: list, labels: list):
        self.length = len(data)
        self.data = torch.Tensor(data)
        self.labels = torch.Tensor(labels, dtype=int)

    def __getitem__(self, index: int) -> Tuple[Any, Any]:
        return self.data[index], self.labels[index]

    def __len__(self):
        return self.length


class PSLTest(unittest.TestCase):
    def setUp(self):
        cfg = DictConfig(
            {
                "optimizer": {
                    "_target_": "torch.optim.SGD",
                    "lr": 1,
                    "momentum": 0,
                    "weight_decay": 0,
                },
                "experiment": {"metrics": ["accuracy"]},
                "dataset": {"num_classes": 2, "average_setting": "micro"},
                "topology": {
                    "devices": [
                        {
                            "device_id": "d0",
                            "address": "localhost:50051",
                            "torch_device": "cuda:0",
                        },
                        {
                            "device_id": "d1",
                            "address": "localhost:50052",
                            "torch_device": "cuda:0",
                        },
                    ]
                },
                "own_device_id": "d0",
                "simulate_parallelism": False,
            }
        )
        # init models with fixed weights for repeatability
        server_state_dict = OrderedDict(
            [
                ("layer.weight", tensor([[-0.5], [-1.0]])),
                ("layer.bias", tensor([-0.5, 0.25])),
            ]
        )
        client_state_dict = OrderedDict(
            [("layer.weight", tensor([[-1.0]])), ("layer.bias", tensor([0.5]))]
        )
        server_model = ServerModel()
        server_model.load_state_dict(server_state_dict)
        server_model.get_optimizer_params = (
            server_model.parameters
        )  # using a model provider, this is created automatically
        client_model1 = ClientModel()
        client_model1.load_state_dict(client_state_dict)
        client_model1.get_optimizer_params = (
            server_model.parameters
        )  # using a model provider, this is created automatically
        client_model2 = ClientModel()
        client_model2.load_state_dict(client_state_dict)
        client_model2.get_optimizer_params = (
            server_model.parameters
        )  # using a model provider, this is created automatically
        self.server = DeviceServer(
            model=server_model, loss_fn=torch.nn.L1Loss(), cfg=cfg
        )
        self.client1 = DeviceClient(
            model=client_model1,
            cfg=cfg,
            train_dl=DataLoader(TensorDataset(tensor([[0.9]]), tensor([[0.0, 1.0]]))),
            val_dl=DataLoader(TensorDataset(tensor([[0.75]]), tensor([[0.0, 1.0]]))),
            test_dl=None,
        )
        client2_cfg = cfg.copy()
        client2_cfg["own_device_id"] = "d1"
        self.client2 = DeviceClient(
            model=client_model2,
            cfg=client2_cfg,
            train_dl=DataLoader(TensorDataset(tensor([[0.1]]), tensor([[1.0, 0.0]]))),
            val_dl=DataLoader(TensorDataset(tensor([[0.25]]), tensor([[1.0, 0.0]]))),
            test_dl=None,
        )

        def get_client_side_effect(fn):
            """
            Creates side effects for methods of the form METHOD_on(device_id) skipping the request dispatcher.
            """

            def side_effect(*args, **kwargs):
                # get the device id which is either a positional or keyword arg
                if len(args) > 0:
                    device_id = args[0]
                elif "client_id" in kwargs:
                    device_id = kwargs.pop("client_id")
                elif "device_id" in kwargs:
                    device_id = kwargs.pop("device_id")
                else:
                    return KeyError(
                        f"Could not find device_id in args or kwargs for function {fn}"
                    )
                # delegate to correct client then using the given method name
                if device_id == "d0":
                    return self.client1.__class__.__dict__[fn](
                        self.client1, *args, **kwargs
                    )
                elif device_id == "d1":
                    return self.client2.__class__.__dict__[fn](
                        self.client2, *args, **kwargs
                    )
                else:
                    return KeyError(f"Unknown device_id {device_id}")

            return side_effect

        def get_server_side_effect(fn):
            def side_effect(*args, **kwargs):
                return self.server.__class__.__dict__[fn](
                    self.server, *args, **kwargs
                ) + (
                    1,
                )  # Add (1,) as placeholder for DiagnosticMetricsContainer

            return side_effect

        node_device = Mock(Device)
        node_device.battery = Mock(Battery)
        node_device.logger = Mock(SimpleLogger)
        node_device.train_batch_on_client_only_on.side_effect = get_client_side_effect(
            "train_single_batch"
        )
        node_device.backpropagation_on_client_only_on.side_effect = (
            get_client_side_effect("backward_single_batch")
        )
        node_device.set_gradient_and_finalize_training_on_client_only_on.side_effect = (
            get_client_side_effect("set_gradient_and_finalize_training")
        )
        node_device.train_batch.side_effect = get_server_side_effect("train_batch")
        node_device.evaluate_on.side_effect = get_client_side_effect("evaluate")
        node_device.evaluate_batch.side_effect = get_server_side_effect(
            "evaluate_batch"
        )

        self.node_device1 = copy(node_device)
        self.node_device2 = copy(node_device)
        self.node_device1.client = self.client1
        self.node_device1.device_id = "d0"
        self.node_device2.device_id = "d1"
        self.client1.set_device(self.node_device1)
        self.client2.set_device(self.node_device2)
        self.server.set_device(self.node_device1)

    def test_train_parallel_sl(self):
        (
            client_weights,
            server_weights,
            model_metrics,
            optimizer_state,
            diagnostic_metrics,
        ) = self.server.train_parallel_split_learning(["d0", "d1"], round_no=0)
        self.assertDictEqual(self.client1.get_weights(), self.client2.get_weights())

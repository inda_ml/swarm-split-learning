import threading
import time
import unittest
from unittest.mock import patch

from edml.core.battery import Battery


class BatteryTest(unittest.TestCase):
    def setUp(self):
        self.battery = Battery(1000, 1, 0.1, 0.2, 0.3)
        self.battery.start_experiment()

    def test_update_flops(self):
        self.battery.update_flops(420000000)
        self.assertEqual(self.battery.remaining_capacity(), 958)

    def test_update_time_after_one_second(self):
        self.battery.__last_update__ = 0.0
        with patch("time.time", side_effect=[0.0, 1.0, 2.0, 3.0, 4.0, 5.0]):
            for i in range(0, 6):
                self.battery.update_time()
        self.assertEqual(self.battery.remaining_capacity(), 995)
        self.assertEqual(self.battery.initial_capacity(), 1000)

    def test_update_time_immediately(self):
        self.battery.update_time()
        self.assertAlmostEqual(self.battery.remaining_capacity(), 1000, delta=0.1)

    def test_update_communication_received(self):
        self.battery.update_communication_received(10000000)
        self.assertEqual(self.battery.remaining_capacity(), 998)

    def test_update_communication_sent(self):
        self.battery.update_communication_sent(10000000)
        self.assertEqual(self.battery.remaining_capacity(), 997)

    def test_exception_when_empty_after_flop_update(self):
        self.battery.__capacity__ = 0.00000001
        self.assertFalse(self.battery.is_empty())
        with self.assertRaises(Exception):
            self.battery.update_flops(1000)
            self.assertTrue(self.battery.is_empty())

    def test_exception_when_empty_after_time_update(self):
        self.battery.__capacity__ = 0.00000001
        self.assertFalse(self.battery.is_empty())
        with self.assertRaises(Exception):
            self.battery.update_time()
            self.assertTrue(self.battery.is_empty())


class BatteryTestNotUpdatingWhenExperimentNotStarted(unittest.TestCase):
    def setUp(self):
        self.battery = Battery(1000, 1, 0.1, 0.2, 0.3)

    def test_not_updating_flops(self):
        self.battery.update_flops(420000000)
        self.assertEqual(self.battery.remaining_capacity(), 1000)

    def test_not_updating_time(self):
        self.battery.update_time()
        self.assertEqual(self.battery.remaining_capacity(), 1000)

    def test_not_updating_communication_received(self):
        self.battery.update_communication_received(10000000)
        self.assertEqual(self.battery.remaining_capacity(), 1000)

    def test_not_updating_communication_sent(self):
        self.battery.update_communication_sent(10000000)
        self.assertEqual(self.battery.remaining_capacity(), 1000)


class BatteryMultiThreadedTest(unittest.TestCase):
    def setUp(self):
        self.battery = Battery(1000, 1, 0.1, 0.1, 0.1)
        self.battery.start_experiment()

        def bulk_update_flops():
            """with 0.1 deduction per MFLOP it should decrease capacity by 499.995"""
            for i in range(100000):
                self.battery.update_flops(i)

        def bulk_update_communication_received():
            """with 0.1 deduction per mbyte it should decrease capacity by 499.995"""
            for i in range(100000):
                self.battery.update_communication_received(i)

        def bulk_update_communication_sent():
            """with 0.1 deduction per mbyte it should decrease capacity by 499.995"""
            for i in range(100000):
                self.battery.update_communication_sent(i)

        self.bulk_update_flops = bulk_update_flops
        self.bulk_update_communication_received = bulk_update_communication_received
        self.bulk_update_communication_sent = bulk_update_communication_sent

    def test_multi_threaded_flops(self):
        t1 = threading.Thread(target=self.bulk_update_flops)
        t2 = threading.Thread(target=self.bulk_update_flops)
        t1.start()
        t2.start()
        t1.join()
        t2.join()
        self.assertAlmostEqual(
            self.battery.remaining_capacity(), 0.01, delta=1e-12
        )  # delta due to FP precision

    def test_multi_threaded_time_and_flops(self):
        start_time = time.time()
        self.battery.__last_update__ = start_time
        with patch("time.time", return_value=start_time + 42):
            t1 = threading.Thread(target=self.bulk_update_flops)
            t2 = threading.Thread(target=self.battery.update_time)
            t1.start()
            t2.start()
            t1.join()
            t2.join()
        self.assertAlmostEqual(self.battery.remaining_capacity(), 458.005, delta=1e-12)

    def test_multi_threaded_communication(self):
        t1 = threading.Thread(target=self.bulk_update_communication_received)
        t2 = threading.Thread(target=self.bulk_update_communication_sent)
        t1.start()
        t2.start()
        t1.join()
        t2.join()
        self.assertAlmostEqual(self.battery.remaining_capacity(), 0.01, delta=1e-12)

import os
import unittest
from copy import deepcopy

import torch
from omegaconf import OmegaConf

from edml.helpers.model_splitting import Part
from edml.models.autoencoder import ClientWithAutoencoder, ServerWithAutoencoder
from edml.tests.models.model_loading_helpers import (
    _get_model_from_model_provider_config,
)


def plot_grad_flow(named_parameters):
    from matplotlib import pyplot as plt
    import numpy as np
    from matplotlib.lines import Line2D

    """Plots the gradients flowing through different layers in the net during training.
    Can be used for checking for possible gradient vanishing / exploding problems.

    Usage: Plug this function in Trainer class after loss.backwards() as
    "plot_grad_flow(self.model.named_parameters())" to visualize the gradient flow"""
    ave_grads = []
    max_grads = []
    layers = []
    for n, p in named_parameters:
        if (p.requires_grad) and ("bias" not in n):
            layers.append(n)
            ave_grads.append(p.grad.abs().mean())
            max_grads.append(p.grad.abs().max())
    plt.bar(np.arange(len(max_grads)), max_grads, alpha=0.1, lw=1, color="c")
    plt.bar(np.arange(len(max_grads)), ave_grads, alpha=0.1, lw=1, color="b")
    plt.hlines(0, 0, len(ave_grads) + 1, lw=2, color="k")
    plt.xticks(range(0, len(ave_grads), 1), layers, rotation="vertical")
    plt.xlim(left=0, right=len(ave_grads))
    plt.ylim(bottom=-0.001)  # , top=0.02)  # zoom in on the lower gradient regions
    plt.xlabel("Layers")
    plt.ylabel("average gradient")
    plt.title("Gradient flow")
    plt.grid(True)
    plt.legend(
        [
            Line2D([0], [0], color="c", lw=4),
            Line2D([0], [0], color="b", lw=4),
            Line2D([0], [0], color="k", lw=4),
        ],
        ["max-gradient", "mean-gradient", "zero-gradient"],
    )
    plt.show()


class GetModelsTest(unittest.TestCase):
    def setUp(self):
        os.chdir(os.path.join(os.path.dirname(__file__), "../../../"))
        self.cfg = OmegaConf.create({"some_key": "some_value"})
        self.cfg.seed = OmegaConf.load(
            os.path.join(
                os.path.dirname(__file__),
                "../../config/seed/default.yaml",
            )
        )

    def test_load_resnet20(self):
        client, server = _get_model_from_model_provider_config(self.cfg, "resnet20")
        self.assertIsInstance(client, Part)
        self.assertIsInstance(server, Part)
        self.assertEqual(len(client.layers), 4)
        self.assertEqual(len(server.layers), 5)
        self.assertEqual(server(client(torch.zeros(1, 3, 32, 32))).shape, (1, 100))

    def test_load_resnet20_with_ae(self):
        client, server = _get_model_from_model_provider_config(
            self.cfg, "resnet20-with-autoencoder"
        )
        self.assertIsInstance(client, ClientWithAutoencoder)
        self.assertIsInstance(server, ServerWithAutoencoder)
        self.assertEqual(len(client.model.layers), 4)
        self.assertEqual(len(server.model.layers), 5)
        self.assertEqual(server(client(torch.zeros(1, 3, 32, 32))).shape, (1, 100))

    def test_training_resnet20_with_ae_as_non_trainable_layers(self):
        client_encoder, server_decoder = _get_model_from_model_provider_config(
            self.cfg, "resnet20-with-autoencoder"
        )
        client_params = deepcopy(str(client_encoder.model.state_dict()))
        encoder_params = deepcopy(str(client_encoder.autoencoder.state_dict()))
        server_params = deepcopy(str(server_decoder.model.state_dict()))
        decoder_params = deepcopy(str(server_decoder.autoencoder.state_dict()))

        # Training loop
        client_optimizer = torch.optim.Adam(client_encoder.get_optimizer_params())
        server_optimizer = torch.optim.Adam(server_decoder.get_optimizer_params())
        smashed_data = client_encoder(torch.zeros(7, 3, 32, 32))
        output_train = server_decoder(smashed_data)
        loss_train = torch.nn.functional.cross_entropy(
            output_train, torch.rand((7, 100))
        )
        loss_train.backward()
        server_optimizer.step()
        client_encoder.trainable_layers_output.backward(
            server_decoder.trainable_layers_input.grad
        )
        client_optimizer.step()

        # check that AE hasn't changed, but client and server have
        self.assertEqual(encoder_params, str(client_encoder.autoencoder.state_dict()))
        self.assertEqual(decoder_params, str(server_decoder.autoencoder.state_dict()))
        self.assertNotEqual(client_params, str(client_encoder.model.state_dict()))
        self.assertNotEqual(server_params, str(server_decoder.model.state_dict()))

    def test_load_resnet110(self):
        client, server = _get_model_from_model_provider_config(self.cfg, "resnet110")
        self.assertIsInstance(client, Part)
        self.assertIsInstance(server, Part)
        self.assertEqual(len(client.layers), 4)
        self.assertEqual(len(server.layers), 5)
        self.assertEqual(server(client(torch.zeros(1, 3, 32, 32))).shape, (1, 100))

    def test_load_resnet110_with_ae(self):
        client, server = _get_model_from_model_provider_config(
            self.cfg, "resnet110-with-autoencoder"
        )
        self.assertIsInstance(client, ClientWithAutoencoder)
        self.assertIsInstance(server, ServerWithAutoencoder)
        self.assertEqual(len(client.model.layers), 4)
        self.assertEqual(len(server.model.layers), 5)
        self.assertEqual(server(client(torch.zeros(1, 3, 32, 32))).shape, (1, 100))

import concurrent.futures
import math
import threading
import unittest
from inspect import signature
from unittest.mock import Mock, patch

import grpc
from grpc import StatusCode
from grpc_testing import server_from_dictionary, strict_real_time
from omegaconf import DictConfig
from torch import Tensor

from edml.core.battery import Battery, BatteryEmptyException
from edml.core.client import DeviceClient
from edml.core.device import RPCDeviceServicer, NetworkDevice, DeviceRequestDispatcher
from edml.core.server import DeviceServer
from edml.generated import connection_pb2, datastructures_pb2
from edml.generated.connection_pb2 import (
    EvalResponse,
    EvalBatchResponse,
    SetWeightsResponse,
    TrainEpochResponse,
)
from edml.generated.datastructures_pb2 import (
    Activations,
    Labels,
    Weights,
    DeviceInfo,
    BatteryStatus,
)
from edml.helpers.metrics import (
    ModelMetricResult,
    ModelMetricResultContainer,
    DiagnosticMetricResultContainer,
    DiagnosticMetricResult,
)
from edml.helpers.proto_helpers import (
    tensor_to_proto,
    proto_to_tensor,
    state_dict_to_proto,
    proto_to_state_dict,
    proto_to_weights,
    weights_to_proto,
    gradients_to_proto,
    metrics_to_proto,
    proto_to_metrics,
)
from edml.helpers.types import DeviceBatteryStatus
from edml.tests.controllers.test_helper import get_side_effect


class NetworkDeviceTest(unittest.TestCase):

    def setUp(self) -> None:
        self.device = NetworkDevice("0", None, Mock(Battery))
        self.device.set_client(Mock(DeviceClient))
        self.device.set_server(Mock(DeviceServer))

    def test_get_device_ids(self):
        self.device.devices = [
            DictConfig({"device_id": "0", "address": "42"}),
            DictConfig({"device_id": "1", "address": "43"}),
        ]
        self.assertEqual(self.device.__get_device_ids__(), ["0", "1"])

    def test_train_epoch_on_same_device(self):
        self.device.train_epoch_on("0", "0", 0)
        self.device.client.train_epoch.assert_called_once_with("0", round_no=0)

    def test_train_epoch_on_different_device(self):
        self.device.train_epoch_on("0", "1")
        self.device.server.train.assert_not_called()

    def test_rpc_call_for_train_epoch_on_different_device(self):
        self.device.request_dispatcher.connections["1"] = Mock(spec=["TrainEpoch"])
        self.device.request_dispatcher.connections["1"].TrainEpoch.return_value = (
            TrainEpochResponse(
                weights=Weights(weights=state_dict_to_proto({"weights": Tensor([42])}))
            )
        )

        weights, diagnostic_metrics = self.device.train_epoch_on("1", "0", 0)

        self.assertEqual(weights, {"weights": Tensor([42])})
        self.assertIsNotNone(diagnostic_metrics)
        self.device.request_dispatcher.connections[
            "1"
        ].TrainEpoch.assert_called_once_with(
            connection_pb2.TrainEpochRequest(
                server=DeviceInfo(device_id="0", address=""), round_no=0
            )
        )


class RPCDeviceServicerTest(unittest.TestCase):

    def setUp(self) -> None:
        # instantiating NetworkDevice to mock Device(ABC)'s properties
        # store mock_device reference for later function call assertions
        self.mock_device = Mock(spec=NetworkDevice(42, None, Mock(Battery)))
        self.mock_device.device_id = 42

        my_servicer = RPCDeviceServicer(device=self.mock_device)
        servicers = {connection_pb2.DESCRIPTOR.services_by_name["Device"]: my_servicer}
        self.test_server = server_from_dictionary(servicers, strict_real_time())

        self.metrics = ModelMetricResultContainer(
            [ModelMetricResult("d1", "accuracy", "val", 0.42, 42)]
        )
        self.diagnostic_metrics = DiagnosticMetricResultContainer(
            [DiagnosticMetricResult("d1", "comp_time", "train", 42)]
        )

    def make_call(self, method_name, request):
        method = self.test_server.invoke_unary_unary(
            method_descriptor=connection_pb2.DESCRIPTOR.services_by_name[
                "Device"
            ].methods_by_name[method_name],
            invocation_metadata={},
            request=request,
            timeout=None,
        )
        return method.termination()

    def test_train_global(self):
        self.mock_device.train_global.return_value = (
            {"weights": Tensor([42])},
            {"weights": Tensor([43])},
            self.metrics,
            {"optimizer_state": 44},
            self.diagnostic_metrics,
        )
        request = connection_pb2.TrainGlobalRequest(
            epochs=42,
            round_no=1,
            adaptive_threshold_value=3,
            optimizer_state=state_dict_to_proto({"optimizer_state": 42}),
        )

        response, metadata, code, details = self.make_call("TrainGlobal", request)

        self.assertEqual(
            (
                proto_to_weights(response.client_weights),
                proto_to_weights(response.server_weights),
            ),
            ({"weights": Tensor([42])}, {"weights": Tensor([43])}),
        )
        self.assertEqual(proto_to_metrics(response.metrics), self.metrics)
        self.assertEqual(
            proto_to_state_dict(response.optimizer_state), {"optimizer_state": 44}
        )
        self.assertEqual(code, StatusCode.OK)
        self.mock_device.train_global.assert_called_once_with(
            42, 1, 3, {"optimizer_state": 42}
        )
        self.assertEqual(
            proto_to_metrics(response.diagnostic_metrics), self.diagnostic_metrics
        )

    def test_set_weights(self):
        request = connection_pb2.SetWeightsRequest(
            weights=Weights(weights=state_dict_to_proto({"weights": Tensor([42])})),
            on_client=True,
        )

        response, metadata, code, details = self.make_call("SetWeights", request)

        self.assertEqual(type(response), type(SetWeightsResponse()))
        self.assertEqual(code, StatusCode.OK)
        self.mock_device.set_weights.assert_called_once_with(
            {"weights": Tensor([42])}, True
        )

    def test_train_epoch(self):
        self.mock_device.train_epoch.return_value = {
            "weights": Tensor([42])
        }, self.diagnostic_metrics
        request = connection_pb2.TrainEpochRequest(
            server=datastructures_pb2.DeviceInfo(device_id="42", address="")
        )

        response, metadata, code, details = self.make_call("TrainEpoch", request)

        self.assertEqual(
            proto_to_state_dict(response.weights.weights), {"weights": Tensor([42])}
        )
        self.assertEqual(code, StatusCode.OK)
        self.mock_device.train_epoch.assert_called_once_with("42", 0)
        self.assertEqual(
            proto_to_metrics(response.diagnostic_metrics), self.diagnostic_metrics
        )

    def test_train_batch(self):
        self.mock_device.train_batch.return_value = (
            Tensor([42]),
            42.0,
            self.diagnostic_metrics,
        )
        request = connection_pb2.TrainBatchRequest(
            smashed_data=Activations(activations=tensor_to_proto(Tensor([1.0]))),
            labels=Labels(labels=tensor_to_proto(Tensor([1]))),
        )

        response, metadata, code, details = self.make_call("TrainBatch", request)

        self.assertEqual(code, StatusCode.OK)
        self.mock_device.train_batch.assert_called_once_with(Tensor([1.0]), Tensor([1]))
        self.assertEqual(proto_to_tensor(response.gradients.gradients), Tensor([42]))
        self.assertEqual(
            proto_to_metrics(response.diagnostic_metrics), self.diagnostic_metrics
        )

    def test_evaluate_global(self):
        self.mock_device.evaluate_global.return_value = (
            self.metrics,
            self.diagnostic_metrics,
        )
        request = connection_pb2.EvalGlobalRequest(validation=False, federated=False)

        response, metadata, code, details = self.make_call("EvaluateGlobal", request)

        self.assertEqual(proto_to_metrics(response.metrics), self.metrics)
        self.assertEqual(code, StatusCode.OK)
        self.mock_device.evaluate_global.assert_called_once_with(False, False)
        self.assertEqual(
            proto_to_metrics(response.diagnostic_metrics), self.diagnostic_metrics
        )

    def test_evaluate(self):
        self.mock_device.evaluate.return_value = self.diagnostic_metrics
        request = connection_pb2.EvalRequest(
            server=DeviceInfo(device_id="42", address=""), validation=True
        )

        response, metadata, code, details = self.make_call("Evaluate", request)

        self.assertEqual(type(response), type(EvalResponse()))
        self.assertEqual(code, StatusCode.OK)
        self.mock_device.evaluate.assert_called_once_with("42", True)
        self.assertEqual(
            proto_to_metrics(response.diagnostic_metrics), self.diagnostic_metrics
        )

    def test_evaluate_batch(self):
        self.mock_device.evaluate_batch.return_value = self.diagnostic_metrics
        request = connection_pb2.EvalBatchRequest(
            smashed_data=Activations(activations=tensor_to_proto(Tensor([1.0]))),
            labels=Labels(labels=tensor_to_proto(Tensor([1]))),
        )

        response, metadata, code, details = self.make_call("EvaluateBatch", request)

        self.assertEqual(type(response), type(EvalBatchResponse()))
        self.assertEqual(code, StatusCode.OK)
        self.mock_device.evaluate_batch.assert_called_once_with(
            Tensor([1.0]), Tensor([1])
        )
        self.assertEqual(
            proto_to_metrics(response.diagnostic_metrics), self.diagnostic_metrics
        )

    def test_full_model_training(self):
        self.mock_device.federated_train.return_value = (
            {"weights": Tensor([42])},
            {"weights": Tensor([43])},
            44,
            self.metrics,
            self.diagnostic_metrics,
        )
        request = connection_pb2.FullModelTrainRequest()

        response, metadata, code, details = self.make_call("FullModelTraining", request)

        self.assertEqual(
            proto_to_state_dict(response.client_weights.weights),
            {"weights": Tensor([42])},
        )
        self.assertEqual(
            proto_to_state_dict(response.server_weights.weights),
            {"weights": Tensor([43])},
        )
        self.assertEqual(response.num_samples, 44)
        self.assertEqual(proto_to_metrics(response.metrics), self.metrics)
        self.assertEqual(code, StatusCode.OK)
        self.mock_device.federated_train.assert_called_once()

    def test_start_experiment(self):
        self.mock_device.start_experiment.return_value = None
        request = connection_pb2.StartExperimentRequest()

        response, metadata, code, details = self.make_call("StartExperiment", request)

        self.assertEqual(type(response), type(connection_pb2.StartExperimentResponse()))
        self.assertEqual(code, StatusCode.OK)
        self.mock_device.start_experiment.assert_called_once()

    def test_end_experiment(self):
        self.mock_device.end_experiment.return_value = None
        request = connection_pb2.EndExperimentRequest()

        response, metadata, code, details = self.make_call("EndExperiment", request)

        self.assertEqual(type(response), type(connection_pb2.EndExperimentResponse()))
        self.assertEqual(code, StatusCode.OK)
        self.mock_device.end_experiment.assert_called_once()

    def test_battery_status(self):
        self.mock_device.get_battery_status.return_value = (42, 21)
        request = connection_pb2.BatteryStatusRequest()

        response, metadata, code, details = self.make_call("GetBatteryStatus", request)

        self.assertEqual(
            response.status,
            BatteryStatus(initial_battery_level=42, current_battery_level=21),
        )
        self.assertEqual(code, StatusCode.OK)
        self.mock_device.get_battery_status.assert_called_once()

    def test_dataset_model_info(self):
        self.mock_device.client._train_data.dataset = [1]
        self.mock_device.client._val_data.dataset = [2]
        self.mock_device.client._model_flops = {"FW": 3, "BW": 6}
        self.mock_device.server._model_flops = {"FW": 4, "BW": 8}
        request = connection_pb2.DatasetModelInfoRequest()

        response, metadata, code, details = self.make_call(
            "GetDatasetModelInfo", request
        )

        self.assertEqual(code, StatusCode.OK)
        self.assertEqual(response.train_samples, 1)
        self.assertEqual(response.validation_samples, 1)
        self.assertEqual(response.client_fw_flops, 3)
        self.assertEqual(response.server_fw_flops, 4)
        self.assertEqual(response.client_bw_flops, 6)
        self.assertEqual(response.server_bw_flops, 8)


class RPCDeviceServicerBatteryEmptyTest(unittest.TestCase):

    def setUp(self) -> None:
        # instantiating NetworkDevice to mock Device(ABC)'s properties
        # store mock_device reference for later function call assertions
        self.mock_device = Mock(spec=NetworkDevice(42, None, Mock(Battery)))
        my_servicer = RPCDeviceServicer(device=self.mock_device)
        servicers = {connection_pb2.DESCRIPTOR.services_by_name["Device"]: my_servicer}
        self.test_server = server_from_dictionary(servicers, strict_real_time())

    def make_call(self, method_name, request):
        method = self.test_server.invoke_unary_unary(
            method_descriptor=connection_pb2.DESCRIPTOR.services_by_name[
                "Device"
            ].methods_by_name[method_name],
            invocation_metadata={},
            request=request,
            timeout=None,
        )
        return method.termination()

    def test_stop_at_train_global(self):
        self.mock_device.train_global.side_effect = BatteryEmptyException(
            "Battery empty"
        )
        request = connection_pb2.TrainGlobalRequest(
            optimizer_state=state_dict_to_proto(None)
        )
        self._test_device_out_of_battery_lets_rpc_fail(request, "TrainGlobal")

    def test_stop_at_set_weights(self):
        self.mock_device.set_weights.side_effect = BatteryEmptyException(
            "Battery empty"
        )
        request = connection_pb2.SetWeightsRequest(weights=weights_to_proto({}))
        self._test_device_out_of_battery_lets_rpc_fail(request, "SetWeights")

    def test_stop_at_train_epoch(self):
        self.mock_device.train_epoch.side_effect = BatteryEmptyException(
            "Battery empty"
        )
        request = connection_pb2.TrainEpochRequest(server=None)
        self._test_device_out_of_battery_lets_rpc_fail(request, "TrainEpoch")

    def test_stop_at_train_batch(self):
        self.mock_device.train_batch.side_effect = BatteryEmptyException(
            "Battery empty"
        )
        request = connection_pb2.TrainBatchRequest(
            smashed_data=Activations(activations=tensor_to_proto(Tensor([1]))),
            labels=Labels(labels=tensor_to_proto(Tensor([1]))),
        )
        self._test_device_out_of_battery_lets_rpc_fail(request, "TrainBatch")

    def test_stop_at_evaluate_global(self):
        self.mock_device.evaluate_global.side_effect = BatteryEmptyException(
            "Battery empty"
        )
        request = connection_pb2.EvalGlobalRequest()
        self._test_device_out_of_battery_lets_rpc_fail(request, "EvaluateGlobal")

    def test_stop_at_evaluate(self):
        self.mock_device.evaluate.side_effect = BatteryEmptyException("Battery empty")
        request = connection_pb2.EvalRequest(server=None)
        self._test_device_out_of_battery_lets_rpc_fail(request, "Evaluate")

    def test_stop_at_evaluate_batch(self):
        self.mock_device.evaluate_batch.side_effect = BatteryEmptyException(
            "Battery empty"
        )
        request = connection_pb2.EvalBatchRequest(
            smashed_data=Activations(activations=tensor_to_proto(Tensor([1]))),
            labels=Labels(labels=tensor_to_proto(Tensor([1]))),
        )
        self._test_device_out_of_battery_lets_rpc_fail(request, "EvaluateBatch")

    def test_stop_at_full_model_training(self):
        self.mock_device.federated_train.side_effect = BatteryEmptyException(
            "Battery empty"
        )
        request = connection_pb2.FullModelTrainRequest()
        self._test_device_out_of_battery_lets_rpc_fail(request, "FullModelTraining")

    def test_stop_at_start_experiment(self):
        self.mock_device.start_experiment.side_effect = BatteryEmptyException(
            "Battery empty"
        )
        request = connection_pb2.StartExperimentRequest()
        self._test_device_out_of_battery_lets_rpc_fail(request, "StartExperiment")

    def test_stop_at_end_experiment(self):
        self.mock_device.end_experiment.side_effect = BatteryEmptyException(
            "Battery empty"
        )
        request = connection_pb2.EndExperimentRequest()
        self._test_device_out_of_battery_lets_rpc_fail(request, "EndExperiment")

    def test_stop_at_get_battery_status(self):
        self.mock_device.get_battery_status.side_effect = BatteryEmptyException(
            "Battery empty"
        )
        request = connection_pb2.BatteryStatusRequest()
        self._test_device_out_of_battery_lets_rpc_fail(request, "GetBatteryStatus")

    def _test_device_out_of_battery_lets_rpc_fail(self, request, servicer_method_name):
        response, metadata, code, details = self.make_call(
            servicer_method_name, request
        )
        self.assertIsNone(response)
        self.assertEqual(code, StatusCode.UNKNOWN)
        self.assertEqual(details, "Exception calling application: Battery empty")


class BatteryUpdateTest(unittest.TestCase):

    def setUp(self):
        self.battery = Mock(Battery(1000, 1, 0.1))

    def test_battery_update_on_train_epoch(self):
        device = NetworkDevice("0", None, battery=self.battery)
        device.set_client(Mock(DeviceClient))
        device.train_epoch(None)
        self.battery.update_time.assert_called()


class RequestDispatcherTest(unittest.TestCase):

    def setUp(self) -> None:
        self.dispatcher = DeviceRequestDispatcher(
            []
        )  # pass no devices to avoid grpc calls
        # mock connections instead
        self.dispatcher.connections["1"] = Mock(
            spec=[
                "TrainGlobal",
                "SetWeights",
                "TrainEpoch",
                "TrainBatch",
                "EvaluateGlobal",
                "Evaluate",
                "EvaluateBatch",
                "FullModelTraining",
                "StartExperiment",
                "EndExperiment",
                "GetBatteryStatus",
                "GetDatasetModelInfo",
            ]
        )
        self.dispatcher.devices = [
            DictConfig({"device_id": "0", "address": "42"}),  # inactive device
            DictConfig({"device_id": "1", "address": "43"}),
        ]
        self.mock_stub = self.dispatcher.connections["1"]  # for convenience
        self.weights = {"weights": Tensor([42])}
        self.gradients = Tensor([42])
        self.activations = Tensor([1])
        self.labels = Tensor([1])
        self.metrics = ModelMetricResultContainer(
            [ModelMetricResult("d1", "accuracy", "val", 0.42, 42)]
        )
        self.diagnostic_metrics = DiagnosticMetricResultContainer(
            [DiagnosticMetricResult("d1", "comp_time", "train", 42)]
        )

    def test_train_global_on_without_error(self):
        self.mock_stub.TrainGlobal.return_value = connection_pb2.TrainGlobalResponse(
            client_weights=weights_to_proto(self.weights),
            server_weights=weights_to_proto(self.weights),
            metrics=metrics_to_proto(self.metrics),
            diagnostic_metrics=metrics_to_proto(self.diagnostic_metrics),
            optimizer_state=state_dict_to_proto({"optimizer_state": 42}),
        )

        client_weights, server_weights, metrics, optimizer_state, diagnostic_metrics = (
            self.dispatcher.train_global_on("1", 42, 43, 3, {"optimizer_state": 44})
        )

        self.assertEqual(client_weights, self.weights)
        self.assertEqual(server_weights, self.weights)
        self.assertEqual(metrics, self.metrics)
        self.assertEqual(optimizer_state, {"optimizer_state": 42})

        self._assert_field_size_added_to_diagnostic_metrics(diagnostic_metrics)
        self.mock_stub.TrainGlobal.assert_called_once_with(
            connection_pb2.TrainGlobalRequest(
                epochs=42,
                round_no=43,
                adaptive_threshold_value=3,
                optimizer_state=state_dict_to_proto({"optimizer_state": 44}),
            )
        )

    def test_train_global_on_with_error(self):
        self.mock_stub.TrainGlobal.side_effect = grpc.RpcError()

        response = self.dispatcher.train_global_on(
            "1",
            42,
            round_no=43,
            adaptive_threshold_value=3,
            optimizer_state={"optimizer_state": 44},
        )

        self.assertEqual(response, False)
        self.mock_stub.TrainGlobal.assert_called_once_with(
            connection_pb2.TrainGlobalRequest(
                epochs=42,
                round_no=43,
                adaptive_threshold_value=3,
                optimizer_state=state_dict_to_proto({"optimizer_state": 44}),
            )
        )

    def test_set_weights_on_without_error(self):
        self.mock_stub.SetWeights.return_value = SetWeightsResponse()

        self.dispatcher.set_weights_on("1", self.weights, True)

        self.mock_stub.SetWeights.assert_called_once_with(
            connection_pb2.SetWeightsRequest(
                weights=weights_to_proto(self.weights), on_client=True
            ),
            wait_for_ready=False,
        )

    def test_set_weights_on_with_error(self):
        self.mock_stub.SetWeights.side_effect = grpc.RpcError()

        response = self.dispatcher.set_weights_on("1", self.weights, True)

        self.mock_stub.SetWeights.assert_called_once_with(
            connection_pb2.SetWeightsRequest(
                weights=weights_to_proto(self.weights), on_client=True
            ),
            wait_for_ready=False,
        )
        self.assertEqual(response, False)

    def test_train_epoch_on_without_error(self):
        self.mock_stub.TrainEpoch.return_value = TrainEpochResponse(
            weights=weights_to_proto(self.weights),
            diagnostic_metrics=metrics_to_proto(self.diagnostic_metrics),
        )

        weights, diagnostic_metrics = self.dispatcher.train_epoch_on("1", "0", 42)

        self.assertEqual(weights, self.weights)
        self._assert_field_size_added_to_diagnostic_metrics(diagnostic_metrics)
        self.mock_stub.TrainEpoch.assert_called_once_with(
            connection_pb2.TrainEpochRequest(
                server=DeviceInfo(device_id="0", address="42"), round_no=42
            )
        )

    def test_train_epoch_on_with_error(self):
        self.mock_stub.TrainEpoch.side_effect = grpc.RpcError()

        response = self.dispatcher.train_epoch_on("1", "0", 42)

        self.assertEqual(response, False)
        self.mock_stub.TrainEpoch.assert_called_once_with(
            connection_pb2.TrainEpochRequest(
                server=DeviceInfo(device_id="0", address="42"), round_no=42
            )
        )

    def test_train_batch_on_without_error(self):
        self.mock_stub.TrainBatch.return_value = connection_pb2.TrainBatchResponse(
            gradients=gradients_to_proto(self.gradients),
            diagnostic_metrics=metrics_to_proto(self.diagnostic_metrics),
            loss=42.0,
        )

        gradients, loss, diagnostic_metrics = self.dispatcher.train_batch_on(
            "1", self.activations, self.labels
        )

        self.assertEqual(gradients, self.gradients)
        self.assertEqual(loss, 42.0)
        self._assert_field_size_added_to_diagnostic_metrics(diagnostic_metrics)
        self.mock_stub.TrainBatch.assert_called_once_with(
            connection_pb2.TrainBatchRequest(
                smashed_data=Activations(activations=tensor_to_proto(self.activations)),
                labels=Labels(labels=tensor_to_proto(self.labels)),
            )
        )

    def test_train_batch_on_with_error(self):
        self.mock_stub.TrainBatch.side_effect = grpc.RpcError()

        response = self.dispatcher.train_batch_on("1", self.activations, self.labels)

        self.assertEqual(response, False)
        self.mock_stub.TrainBatch.assert_called_once_with(
            connection_pb2.TrainBatchRequest(
                smashed_data=Activations(activations=tensor_to_proto(self.activations)),
                labels=Labels(labels=tensor_to_proto(self.labels)),
            )
        )

    def test_evaluate_global_on_without_error(self):
        self.mock_stub.EvaluateGlobal.return_value = connection_pb2.EvalGlobalResponse(
            metrics=metrics_to_proto(self.metrics),
            diagnostic_metrics=metrics_to_proto(self.diagnostic_metrics),
        )

        metrics, diagnostic_metrics = self.dispatcher.evaluate_global_on(
            "1", True, True
        )

        self.assertEqual(metrics, self.metrics)
        self._assert_field_size_added_to_diagnostic_metrics(diagnostic_metrics)
        self.mock_stub.EvaluateGlobal.assert_called_once_with(
            connection_pb2.EvalGlobalRequest(validation=True, federated=True)
        )

    def test_evaluate_global_on_with_error(self):
        self.mock_stub.EvaluateGlobal.side_effect = grpc.RpcError()

        response = self.dispatcher.evaluate_global_on("1", True, True)

        self.assertEqual(response, False)
        self.mock_stub.EvaluateGlobal.assert_called_once_with(
            connection_pb2.EvalGlobalRequest(validation=True, federated=True)
        )

    def test_evaluate_on_without_error(self):
        self.mock_stub.Evaluate.return_value = EvalResponse(
            diagnostic_metrics=metrics_to_proto(self.diagnostic_metrics)
        )

        diagnostic_metrics = self.dispatcher.evaluate_on("1", "0", True)

        self.assertEqual(diagnostic_metrics, self.diagnostic_metrics)
        self.mock_stub.Evaluate.assert_called_once_with(
            connection_pb2.EvalRequest(
                server=DeviceInfo(device_id="0", address="42"), validation=True
            )
        )

    def test_evaluate_on_with_error(self):
        self.mock_stub.Evaluate.side_effect = grpc.RpcError()

        response = self.dispatcher.evaluate_on("1", "0", True)

        self.assertEqual(response, False)
        self.mock_stub.Evaluate.assert_called_once_with(
            connection_pb2.EvalRequest(
                server=DeviceInfo(device_id="0", address="42"), validation=True
            )
        )

    def test_evaluate_batch_on_without_error(self):
        self.mock_stub.EvaluateBatch.return_value = EvalBatchResponse(
            diagnostic_metrics=metrics_to_proto(self.diagnostic_metrics)
        )

        diagnostic_metrics = self.dispatcher.evaluate_batch_on(
            "1", self.activations, self.labels
        )

        self._assert_field_size_added_to_diagnostic_metrics(
            diagnostic_metrics
        )  # metric field present in response, but not used in practice. Thus, a field size is added
        self.mock_stub.EvaluateBatch.assert_called_once_with(
            connection_pb2.EvalBatchRequest(
                smashed_data=Activations(activations=tensor_to_proto(self.activations)),
                labels=Labels(labels=tensor_to_proto(self.labels)),
            )
        )

    def test_evaluate_batch_on_with_error(self):
        self.mock_stub.EvaluateBatch.side_effect = grpc.RpcError()

        response = self.dispatcher.evaluate_batch_on("1", self.activations, self.labels)

        self.assertEqual(response, False)
        self.mock_stub.EvaluateBatch.assert_called_once_with(
            connection_pb2.EvalBatchRequest(
                smashed_data=Activations(activations=tensor_to_proto(self.activations)),
                labels=Labels(labels=tensor_to_proto(self.labels)),
            )
        )

    def test_full_model_training_without_error(self):
        self.mock_stub.FullModelTraining.return_value = (
            connection_pb2.FullModelTrainResponse(
                client_weights=weights_to_proto(self.weights),
                server_weights=weights_to_proto(self.weights),
                num_samples=42,
                metrics=metrics_to_proto(self.metrics),
                diagnostic_metrics=metrics_to_proto(self.diagnostic_metrics),
            )
        )

        client_weights, server_weights, num_samples, metrics, diagnostic_metrics = (
            self.dispatcher.federated_train_on("1", 42)
        )

        self.assertEqual(client_weights, self.weights)
        self.assertEqual(server_weights, self.weights)
        self.assertEqual(num_samples, 42)
        self.assertEqual(metrics, self.metrics)
        self._assert_field_size_added_to_diagnostic_metrics(diagnostic_metrics)
        self.mock_stub.FullModelTraining.assert_called_once_with(
            connection_pb2.FullModelTrainRequest(round_no=42)
        )

    def test_full_model_training_with_error(self):
        self.mock_stub.FullModelTraining.side_effect = grpc.RpcError()

        response = self.dispatcher.federated_train_on("1", 42)

        self.assertEqual(response, False)
        self.mock_stub.FullModelTraining.assert_called_once_with(
            connection_pb2.FullModelTrainRequest(round_no=42)
        )

    def test_start_experiment_on_without_error(self):
        self.mock_stub.StartExperiment.return_value = (
            connection_pb2.StartExperimentResponse()
        )

        response = self.dispatcher.start_experiment_on("1", True)

        self.assertEqual(response, True)
        self.mock_stub.StartExperiment.assert_called_once_with(
            connection_pb2.StartExperimentRequest(), wait_for_ready=True
        )

    def test_start_experiment_on_with_error(self):
        self.mock_stub.StartExperiment.side_effect = grpc.RpcError()

        response = self.dispatcher.start_experiment_on("1", True)

        self.assertEqual(response, False)
        self.mock_stub.StartExperiment.assert_called_once_with(
            connection_pb2.StartExperimentRequest(), wait_for_ready=True
        )

    def test_end_experiment_on_without_error(self):
        self.mock_stub.EndExperiment.return_value = (
            connection_pb2.EndExperimentResponse()
        )

        response = self.dispatcher.end_experiment_on("1")

        self.assertEqual(response, True)
        self.mock_stub.EndExperiment.assert_called_once_with(
            connection_pb2.EndExperimentRequest()
        )

    def test_end_experiment_on_with_error(self):
        self.mock_stub.EndExperiment.side_effect = grpc.RpcError()

        response = self.dispatcher.end_experiment_on("1")

        self.assertEqual(response, False)
        self.mock_stub.EndExperiment.assert_called_once_with(
            connection_pb2.EndExperimentRequest()
        )

    def test_get_battery_status_without_error(self):
        self.mock_stub.GetBatteryStatus.return_value = (
            connection_pb2.BatteryStatusResponse(
                status=BatteryStatus(initial_battery_level=42, current_battery_level=21)
            )
        )

        response = self.dispatcher.get_battery_status_on("1")

        self.assertEqual(
            response, DeviceBatteryStatus(initial_capacity=42, current_capacity=21)
        )
        self.mock_stub.GetBatteryStatus.assert_called_once_with(
            connection_pb2.BatteryStatusRequest()
        )

    def test_get_battery_status_with_error(self):
        self.mock_stub.GetBatteryStatus.side_effect = grpc.RpcError()

        response = self.dispatcher.get_battery_status_on("1")

        self.assertEqual(response, False)
        self.mock_stub.GetBatteryStatus.assert_called_once_with(
            connection_pb2.BatteryStatusRequest()
        )

    def test_get_dataset_model_info_without_error(self):
        self.mock_stub.GetDatasetModelInfo.return_value = (
            connection_pb2.DatasetModelInfoResponse(
                train_samples=42,
                validation_samples=21,
                client_fw_flops=1,
                server_fw_flops=2,
                client_bw_flops=3,
                server_bw_flops=4,
            )
        )

        response = self.dispatcher.get_dataset_model_info_on("1")

        self.assertEqual(response, (42, 21, 1, 2, 3, 4))
        self.mock_stub.GetDatasetModelInfo.assert_called_once_with(
            connection_pb2.DatasetModelInfoRequest()
        )

    def test_get_dataset_model_info_with_error(self):
        self.mock_stub.GetDatasetModelInfo.side_effect = grpc.RpcError()

        response = self.dispatcher.get_dataset_model_info_on("1")

        self.assertEqual(response, False)
        self.mock_stub.GetDatasetModelInfo.assert_called_once_with(
            connection_pb2.DatasetModelInfoRequest()
        )

    def test_handle_calls_to_inactive_device(self):
        """Test each method of the dispatcher that does RPC calls to handle calls to inactive devices.
        One test where the device is known, but inactive and one where the device is unknown.
        """
        methods_names = [
            "train_global_on",
            "set_weights_on",
            "train_epoch_on",
            "train_batch_on",
            "evaluate_global_on",
            "evaluate_on",
            "evaluate_batch_on",
            "federated_train_on",
            "start_experiment_on",
            "end_experiment_on",
            "get_battery_status_on",
            "get_dataset_model_info_on",
        ]
        for device in [("0", True), ("2", False)]:  # 0 is inactive, 2 is unknown
            for method_name in methods_names:
                with self.subTest(method_name=f"{method_name}_device{device[0]}"):
                    with patch("builtins.print") as print_patch:
                        method = getattr(self.dispatcher, method_name)
                        params = list(signature(method).parameters)

                        # make method call with device id and all other params as None
                        response = method(
                            device[0],
                            *[None for _ in params if _ not in ["self", "device_id"]],
                        )

                        self.assertEqual(response, False)
                        self.mock_stub.assert_not_called()
                        if device[1]:
                            print_patch.assert_called_once_with(
                                f"Device {device[0]} not active"
                            )
                        else:
                            print_patch.assert_called_once_with(
                                f"Unknown Device ID {device[0]}"
                            )

    def _assert_field_size_added_to_diagnostic_metrics(self, diagnostic_metrics):
        """Not to use when response only includes diagnostic metrics, as these are ignored for the field size"""
        self.assertEqual(
            diagnostic_metrics.get_as_list()[0],
            self.diagnostic_metrics.get_as_list()[0],
        )  # check that previous diagnostic metrics still exist
        self.assertEqual(
            diagnostic_metrics.get_as_list()[1].name, "size"
        )  # check that at least one new diagnostic metric for size was added


class RequestDispatcherThreadingTest(unittest.TestCase):

    def test_handle_rpc_error_thread_safety(self):

        # run the procedure with 2 to 20 threads
        for n in range(2, 21):
            with self.subTest(method_name=f"Test with {n} threads"):
                print(f"Test with {n} threads")
                self.request_dispatcher = DeviceRequestDispatcher([])

                # mock n connections with half of them returning errors and the other half valid responses
                self.request_dispatcher.connections = {
                    f"d{i}": Mock(spec=["GetBatteryStatus"]) for i in range(n)
                }
                response = connection_pb2.BatteryStatusResponse(
                    status=BatteryStatus(
                        initial_battery_level=42, current_battery_level=21
                    )
                )
                for i, mock in enumerate(self.request_dispatcher.connections.values()):
                    mock.GetBatteryStatus.side_effect = get_side_effect(i, response)

                # make n concurrent calls to the dispatcher
                responses = []
                response_lock = threading.Lock()
                with concurrent.futures.ThreadPoolExecutor(max_workers=n) as executor:
                    futures = [
                        executor.submit(
                            self.request_dispatcher.get_battery_status_on, device_id
                        )
                        for device_id in self.request_dispatcher.active_devices()
                    ]
                    for future in concurrent.futures.as_completed(futures):
                        with response_lock:
                            responses.append(future.result())

                # check that only error connections were removed
                self.assertEqual(
                    list(self.request_dispatcher.connections.keys()),
                    [f"d{i}" for i in range(n) if i % 2 == 0],
                )

                response_battery_level = DeviceBatteryStatus(
                    initial_capacity=42, current_capacity=21
                )

                # check that there are n responses in total, half of them valid and half of them None
                self.assertEqual(responses.count(False), math.floor(n / 2))
                self.assertEqual(
                    responses.count(response_battery_level), math.ceil(n / 2)
                )

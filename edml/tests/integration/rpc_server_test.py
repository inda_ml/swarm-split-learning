import threading
import unittest
from concurrent import futures
from unittest.mock import Mock, call

import grpc
from omegaconf import DictConfig

from edml.core.battery import Battery, BatteryEmptyException
from edml.core.device import NetworkDevice, RPCDeviceServicer, DeviceRequestDispatcher
from edml.generated import connection_pb2_grpc
from edml.helpers.interceptors import DeviceServerInterceptor
from edml.helpers.logging import SimpleLogger
from edml.helpers.metrics import DiagnosticMetricResultContainer


class RPCServerTest(unittest.TestCase):

    def setUp(self):
        # set up logger, battery, stop event and device as mocks to assert calls and set behavior
        self.server_logger = Mock(spec=SimpleLogger)
        self.server_battery = Mock(spec=Battery)
        self.server_stop_mock = Mock(threading.Event)

        self.client_battery = Mock(spec=Battery)
        self.client_logger = Mock(spec=SimpleLogger)
        self.client_stop_mock = Mock(threading.Event)
        self.device = Mock(
            NetworkDevice(
                device_id="d0", logger=self.server_logger, battery=self.server_battery
            )
        )
        self.device.device_id = "d0"

        devices = [DictConfig({"device_id": "d0", "address": "localhost:50061"})]

        # start server
        self.grpc_server = grpc.server(
            futures.ThreadPoolExecutor(max_workers=2),
            options=[
                ("grpc.max_send_message_length", -1),
                ("grpc.max_receive_message_length", -1),
            ],
            interceptors=[
                DeviceServerInterceptor(
                    logger=self.server_logger,
                    stop_event=self.server_stop_mock,
                    battery=self.server_battery,
                ),
            ],
        )
        connection_pb2_grpc.add_DeviceServicer_to_server(
            RPCDeviceServicer(device=self.device), self.grpc_server
        )
        self.grpc_server.add_insecure_port("localhost:50061")
        self.grpc_server.start()

        self.dispatcher = DeviceRequestDispatcher(
            devices,
            logger=self.client_logger,
            battery=self.client_battery,
            stop_event=self.client_stop_mock,
        )

    def tearDown(self):
        self.grpc_server.stop(grace=None)

    def test_request_success(self):
        self.device.train_epoch.return_value = {
            "weights": 42
        }, DiagnosticMetricResultContainer()

        active_devices = self.dispatcher.active_devices()
        self.assertEqual(active_devices, ["d0"])

        self.dispatcher.train_epoch_on("d0", "d0")

        self.device.train_epoch.assert_called()
        self.server_stop_mock.set.assert_not_called()
        self.client_stop_mock.set.assert_not_called()
        self.assertEqual(self.dispatcher.active_devices(), ["d0"])

    def test_empty_battery_at_receiving_request(self):
        self.server_battery.update_communication_received.side_effect = (
            BatteryEmptyException
        )
        active_devices = self.dispatcher.active_devices()
        self.assertEqual(active_devices, ["d0"])

        self.dispatcher.train_epoch_on("d0", "d0")

        self.server_logger.log.assert_has_calls(
            [
                call({"/Device/TrainEpoch_request_size": 39}),
                call("Battery empty while receiving request"),
            ]
        )
        self.server_stop_mock.set.assert_called()
        self.server_battery.update_communication_received.assert_called_with(39)
        self.server_battery.update_communication_sent.assert_not_called()
        self.client_logger.log.assert_called_with(
            "RPC server battery empty during request"
        )
        self.client_battery.update_communication_sent.assert_called_with(39)
        self.client_battery.update_communication_received.assert_not_called()
        self.client_stop_mock.set.assert_not_called()
        self.assertEqual(self.dispatcher.active_devices(), [])

    def test_empty_battery_while_processing_request(self):
        self.device.train_epoch.side_effect = BatteryEmptyException

        active_devices = self.dispatcher.active_devices()
        self.assertEqual(active_devices, ["d0"])

        self.dispatcher.train_epoch_on("d0", "d0")

        self.device.train_epoch.assert_called()
        self.server_stop_mock.set.assert_called()
        self.server_battery.update_communication_received.assert_called_with(39)
        self.server_battery.update_communication_sent.assert_not_called()
        self.client_logger.log.assert_called_with(
            "RPC server battery empty during request"
        )
        self.client_battery.update_communication_sent.assert_called_with(39)
        self.client_battery.update_communication_received.assert_not_called()
        self.client_stop_mock.set.assert_not_called()
        self.assertEqual(self.dispatcher.active_devices(), [])

    def test_empty_battery_at_sending_response(self):
        self.server_battery.update_communication_sent.side_effect = (
            BatteryEmptyException
        )
        self.device.train_epoch.return_value = {
            "weights": 42
        }, DiagnosticMetricResultContainer()

        active_devices = self.dispatcher.active_devices()
        self.assertEqual(active_devices, ["d0"])

        self.dispatcher.train_epoch_on("d0", "d0")

        self.server_logger.log.assert_has_calls(
            [
                call({"/Device/TrainEpoch_request_size": 39}),
                call({"/Device/TrainEpoch_response_size": 132}),
                call("Battery empty while handling request or sending response"),
            ]
        )
        self.server_battery.update_communication_sent.assert_called_with(132)
        self.server_battery.update_communication_received.assert_called_with(39)
        self.server_stop_mock.set.assert_called()
        self.client_logger.log.assert_called_with(
            "RPC server battery empty during request"
        )
        self.client_battery.update_communication_sent.assert_called_with(39)
        self.client_battery.update_communication_received.assert_not_called()
        self.client_stop_mock.set.assert_not_called()
        self.assertEqual(self.dispatcher.active_devices(), [])

    def test_empty_client_battery_while_sending_request(self):
        active_devices = self.dispatcher.active_devices()
        self.assertEqual(active_devices, ["d0"])
        self.client_battery.update_communication_sent.side_effect = (
            BatteryEmptyException
        )

        with self.assertRaises(BatteryEmptyException):
            # this should raise an exception because the client battery is empty
            # hence dispatcher is not needed anymore
            self.dispatcher.train_epoch_on("d0", "d0")

        self.device.train_epoch.assert_not_called()
        self.server_stop_mock.set.assert_not_called()
        self.client_stop_mock.set.assert_called()
        self.client_logger.log.assert_called_with(
            "RPC client battery empty during request"
        )

    def test_empty_client_battery_while_receiving_response(self):
        active_devices = self.dispatcher.active_devices()
        self.assertEqual(active_devices, ["d0"])
        self.client_battery.update_communication_received.side_effect = (
            BatteryEmptyException
        )
        self.device.train_epoch.return_value = {
            "weights": 42
        }, DiagnosticMetricResultContainer()

        with self.assertRaises(BatteryEmptyException):
            # this should raise an exception because the client battery is empty
            # hence dispatcher is not needed anymore
            self.dispatcher.train_epoch_on("d0", "d0")

        self.device.train_epoch.assert_called()
        self.server_stop_mock.set.assert_not_called()
        self.client_stop_mock.set.assert_called()
        self.client_logger.log.assert_called_with(
            "RPC client battery empty during request"
        )

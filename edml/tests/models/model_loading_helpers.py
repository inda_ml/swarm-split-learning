import os

from omegaconf import OmegaConf

from edml.core.start_device import _get_models


def _get_model_from_model_provider_config(cfg, config_name):
    cfg.model_provider = OmegaConf.load(
        os.path.join(
            os.path.dirname(__file__),
            f"../../config/model_provider/{config_name}.yaml",
        )
    )
    return _get_models(cfg)

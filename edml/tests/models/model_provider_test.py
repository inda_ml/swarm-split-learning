import os
import unittest

from omegaconf import OmegaConf

from edml.models.provider.base import has_autoencoder
from edml.tests.models.model_loading_helpers import (
    _get_model_from_model_provider_config,
)


class ModelProviderTest(unittest.TestCase):
    def setUp(self):
        os.chdir(os.path.join(os.path.dirname(__file__), "../../../"))
        self.cfg = OmegaConf.create({"some_key": "some_value"})

    def test_has_autoencoder_for_model_without_autoencoder(self):
        client, server = _get_model_from_model_provider_config(self.cfg, "resnet20")
        self.assertFalse(has_autoencoder(client))
        self.assertFalse(has_autoencoder(server))

    def test_has_autoencoder_for_model_with_autoencoder(self):
        client, server = _get_model_from_model_provider_config(
            self.cfg, "resnet20-with-autoencoder"
        )
        self.assertTrue(has_autoencoder(client))
        self.assertTrue(has_autoencoder(server))

    def test_get_optimizer_params_for_model_without_autoencoder(self):
        client, server = _get_model_from_model_provider_config(self.cfg, "resnet20")
        self.assertEqual(list(client.get_optimizer_params()), list(client.parameters()))
        self.assertEqual(list(server.get_optimizer_params()), list(server.parameters()))

    def test_get_optimizer_params_for_model_with_autoencoder(self):
        client, server = _get_model_from_model_provider_config(
            self.cfg, "resnet20-with-autoencoder"
        )
        self.assertEqual(
            list(client.get_optimizer_params()), list(client.model.parameters())
        )
        self.assertEqual(
            list(server.get_optimizer_params()), list(server.model.parameters())
        )

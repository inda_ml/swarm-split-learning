import unittest

from edml.models import resnet_models


class ResnetModelsTest(unittest.TestCase):

    def test_get_resnet(self):
        client, server = resnet_models.resnet20(4, 100)
        self.assertIsNotNone(client.state_dict())
        self.assertIsNotNone(server.state_dict())

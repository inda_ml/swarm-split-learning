import unittest
from unittest.mock import Mock

from edml.controllers.split_controller import SplitController
from edml.core.device import DeviceRequestDispatcher
from edml.helpers.metrics import (
    ModelMetricResultContainer,
    DiagnosticMetricResultContainer,
)
from edml.tests.controllers.test_helper import load_sample_config


class SplitControllerTest(unittest.TestCase):
    def setUp(self) -> None:
        self.cfg = load_sample_config()
        self.split_controller = SplitController(self.cfg)
        self.mock = Mock(spec=DeviceRequestDispatcher)
        self.mock.active_devices.return_value = ["d0", "d1"]
        self.split_controller.request_dispatcher = self.mock

    def test_train(self):
        self.mock.train_global_on.return_value = (
            {"weights": 42},
            {"weights": 43},
            ModelMetricResultContainer(),
            {"optimizer_state": 44},
            DiagnosticMetricResultContainer(),
        )

        self.split_controller._train()

        self.mock.set_weights_on.assert_called_once_with(
            device_id="d0", state_dict=None, on_client=True
        )
        self.mock.train_global_on.assert_called_once_with(
            self.cfg.topology.devices[0].device_id, epochs=1, round_no=0
        )

    def test_train_with_inactive_server_device(self):
        self.mock.train_global_on.return_value = False
        self.mock.active_devices.return_value = ["d1"]

        self.split_controller._train()

        # check that first device in active devices is called with weights
        self.mock.set_weights_on.assert_called_once_with(
            device_id="d1", state_dict=None, on_client=True
        )
        self.mock.train_global_on.assert_called_once_with(
            self.cfg.topology.devices[0].device_id, epochs=1, round_no=0
        )

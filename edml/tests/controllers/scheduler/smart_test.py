import unittest
from unittest.mock import Mock, patch

from edml.controllers.scheduler.max_battery import MaxBatteryNextServerScheduler
from edml.controllers.scheduler.smart import SmartNextServerScheduler
from edml.helpers.metrics import DiagnosticMetricResultContainer
from edml.helpers.types import DeviceBatteryStatus
from edml.tests.controllers.test_helper import load_sample_config


class SmartServerDeviceSelectionTest(unittest.TestCase):

    def setUp(self):
        self.diagnostic_metric_container = Mock(spec=DiagnosticMetricResultContainer)
        self.metrics = {
            "gradient_size": 100000,
            "label_size": 2000,
            "smashed_data_size": 100000,
            "client_weight_size": 300000,
            "server_weight_size": 300000,
            "optimizer_state_size": 300000,
            "client_norm_fw_time": 3,
            "client_norm_bw_time": 3,
            "server_norm_fw_time": 3,
            "server_norm_bw_time": 3,
            "comp_latency_factor": {"d0": 1, "d1": 1.01},
        }
        self.scheduler = SmartNextServerScheduler(
            fallback_scheduler=Mock(spec=MaxBatteryNextServerScheduler),
        )
        self.scheduler.cfg = load_sample_config()
        self.scheduler._update_batteries_cb = lambda: {
            "d0": DeviceBatteryStatus.from_tuple((500, 500)),
            "d1": DeviceBatteryStatus.from_tuple((500, 470)),
        }

        self.scheduler._data_model_cb = lambda: [
            {
                "d0": (2, 1),
                "d1": (4, 1),
            },
            {
                "client": {"FW": 1000000, "BW": 2000000},
                "server": {"FW": 1000000, "BW": 2000000},
            },
        ]

    def test_select_server_device_smart_first_round(self):
        # should select according to max battery
        self.scheduler.next_server(
            [""], diagnostic_metric_container=None, last_server_device_id=None
        )
        self.scheduler.fallback_scheduler.next_server.assert_called_once()

    def test_select_server_device_smart_second_round(self):
        # should select build a schedule and select the first element
        with patch(
            "edml.controllers.scheduler.smart.compute_metrics_for_optimization",
            return_value=self.metrics,
        ):
            server_device = self.scheduler.next_server(
                ["d0", "d1"],
                diagnostic_metric_container=self.diagnostic_metric_container,
                last_server_device_id=None,
            )
            self.assertEqual(server_device, "d0")

    def test_get_selection_schedule(self):
        with patch(
            "edml.controllers.scheduler.smart.compute_metrics_for_optimization",
            return_value=self.metrics,
        ):
            schedule = self.scheduler._get_selection_schedule(
                self.diagnostic_metric_container,
            )
            self.assertEqual(schedule, ["d0", "d1", "d1", "d1"])

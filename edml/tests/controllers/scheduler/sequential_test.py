import unittest

from edml.controllers.scheduler.sequential import SequentialNextServerScheduler


class SequentialServerDeviceSelectionTest(unittest.TestCase):

    def setUp(self) -> None:
        self.active_devices = ["d0", "d1", "d2"]
        self.scheduler = SequentialNextServerScheduler()
        self.scheduler.devices = self.active_devices

    def test_select_server_device_for_only_active_devices(self):
        server_device = self.scheduler.next_server(self.active_devices)
        self.assertEqual(server_device, "d0")
        print("=1="),

        server_device = self.scheduler.next_server(
            self.active_devices, last_server_device_id=server_device
        )
        self.assertEqual(server_device, "d1")
        print("=2=")

        server_device = self.scheduler.next_server(
            self.active_devices, last_server_device_id=server_device
        )
        self.assertEqual(server_device, "d2")
        print("=3=")

        server_device = self.scheduler.next_server(
            self.active_devices, last_server_device_id=server_device
        )
        self.assertEqual(server_device, "d0")
        print("=4=")

    def test_select_server_device_with_last_server_device_inactive(self):
        self.active_devices = ["d1", "d2"]

        server_device = self.scheduler.next_server(
            self.active_devices, last_server_device_id="d0"
        )
        self.assertEqual(server_device, "d1")

        server_device = self.scheduler.next_server(
            self.active_devices, last_server_device_id=server_device
        )
        self.assertEqual(server_device, "d2")

        server_device = self.scheduler.next_server(
            self.active_devices, last_server_device_id=server_device
        )
        self.assertEqual(server_device, "d1")

    def test_select_same_server_device_if_all_other_devices_inactive(self):
        self.active_devices = ["d1"]

        server_device = self.scheduler.next_server(self.active_devices)
        self.assertEqual(server_device, "d1")

        server_device = self.scheduler.next_server(
            self.active_devices, last_server_device_id=server_device
        )
        self.assertEqual(server_device, "d1")

import unittest

from edml.controllers.scheduler.max_battery import MaxBatteryNextServerScheduler
from edml.helpers.types import DeviceBatteryStatus


class MaxBatteryServerDeviceSelectionTest(unittest.TestCase):

    def setUp(self) -> None:
        self.active_devices = ["d0", "d1", "d2"]
        self.scheduler = MaxBatteryNextServerScheduler()

    def test_select_first_device_for_equal_batteries(self):
        self.scheduler._update_batteries_cb = lambda: {
            "d0": DeviceBatteryStatus.from_tuple((100, 50)),
            "d1": DeviceBatteryStatus.from_tuple((100, 50)),
            "d2": DeviceBatteryStatus.from_tuple((100, 50)),
        }

        server_device = self.scheduler.next_server(self.active_devices)
        self.assertEqual(server_device, "d0")

    def test_select_device_with_max_battery(self):
        self.scheduler._update_batteries_cb = lambda: {
            "d0": None,
            "d1": DeviceBatteryStatus.from_tuple((100, 50)),
            "d2": DeviceBatteryStatus.from_tuple((100, 100)),
        }

        server_device = self.scheduler.next_server(self.active_devices)
        self.assertEqual(server_device, "d2")

    def test_no_server_for_no_active_device(self):
        self.scheduler._update_batteries_cb = lambda: {
            "d0": None,
            "d1": None,
            "d2": None,
        }

        server_device = self.scheduler.next_server(self.active_devices)
        self.assertEqual(server_device, None)

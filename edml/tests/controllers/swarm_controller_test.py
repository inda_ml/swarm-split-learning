import unittest
from unittest.mock import Mock, call

from omegaconf import ListConfig

from edml.controllers.scheduler.max_battery import MaxBatteryNextServerScheduler
from edml.controllers.scheduler.random import RandomNextServerScheduler
from edml.controllers.scheduler.sequential import SequentialNextServerScheduler
from edml.controllers.scheduler.smart import SmartNextServerScheduler
from edml.controllers.swarm_controller import SwarmController
from edml.core.device import DeviceRequestDispatcher
from edml.helpers.metrics import (
    ModelMetricResultContainer,
    DiagnosticMetricResultContainer,
)
from edml.tests.controllers.test_helper import load_sample_config


class SwarmControllerTest(unittest.TestCase):

    def setUp(self) -> None:
        self.cfg = load_sample_config()
        self.swarm_controller = SwarmController(
            self.cfg, SequentialNextServerScheduler()
        )
        self.mock = Mock(spec=DeviceRequestDispatcher)
        self.mock.active_devices.return_value = ["d0", "d1"]
        self.swarm_controller.request_dispatcher = self.mock

    def test_split_train_round(self):
        self.mock.train_global_on.return_value = (
            {"weights": 42},
            {"weights": 43},
            ModelMetricResultContainer(),
            {"optimizer_state": 42},
            DiagnosticMetricResultContainer(),
        )

        client_weights, server_weights, metrics, optimizer_state, diagnostic_metrics = (
            self.swarm_controller._swarm_train_round(
                None, None, "d1", 0, optimizer_state={"optimizer_state": 43}
            )
        )

        self.assertEqual(client_weights, {"weights": 42})
        self.assertEqual(server_weights, {"weights": 43})
        self.assertEqual(metrics, ModelMetricResultContainer())
        self.assertEqual(diagnostic_metrics, DiagnosticMetricResultContainer())
        self.assertEqual(optimizer_state, {"optimizer_state": 42})
        self.mock.set_weights_on.assert_has_calls(
            [
                call(device_id="d0", state_dict=None, on_client=True),
                call(device_id="d1", state_dict=None, on_client=False),
            ]
        )
        self.mock.train_global_on.assert_called_once_with(
            "d1",
            epochs=1,
            round_no=0,
            adaptive_threshold_value=0.0,
            optimizer_state={"optimizer_state": 43},
        )

    def test_split_train_round_with_inactive_server_device(self):
        self.mock.train_global_on.return_value = False

        client_weights, server_weights, metrics, optimizer_state, diagnostic_metrics = (
            self.swarm_controller._swarm_train_round(None, None, "d1", 0)
        )

        self.assertEqual(client_weights, None)
        self.assertEqual(server_weights, None)
        self.assertEqual(metrics, None)
        self.assertEqual(diagnostic_metrics, None)
        self.assertEqual(optimizer_state, None)
        self.mock.set_weights_on.assert_has_calls(
            [
                call(device_id="d0", state_dict=None, on_client=True),
                call(device_id="d1", state_dict=None, on_client=False),
            ]
        )
        self.mock.train_global_on.assert_called_once_with(
            "d1",
            epochs=1,
            round_no=0,
            adaptive_threshold_value=0.0,
            optimizer_state=None,
        )


class ServerDeviceSelectionTest(unittest.TestCase):

    def setUp(self) -> None:
        self.swarm_controller = SwarmController(
            load_sample_config(), Mock(SequentialNextServerScheduler)
        )
        self.swarm_controller.request_dispatcher = Mock(spec=DeviceRequestDispatcher)
        self.swarm_controller.devices = ListConfig(
            [{"device_id": "d0"}, {"device_id": "d1"}, {"device_id": "d2"}]
        )  # omitted address etc.

    def test_select_no_server_device_if_no_active_devices(self):
        self.swarm_controller.request_dispatcher.active_devices.return_value = []
        self.swarm_controller._refresh_active_devices()

        server_device = self.swarm_controller._select_server_device(None)
        self.assertEqual(server_device, None)

        server_device = self.swarm_controller._select_server_device("d1")
        self.assertEqual(server_device, None)

    def test_sequential_selection_default(self):
        self.swarm_controller._select_server_device()

        self.swarm_controller._next_server_scheduler.next_server.assert_called()

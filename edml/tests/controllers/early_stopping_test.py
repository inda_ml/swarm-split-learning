import unittest

from edml.controllers.early_stopping import EarlyStopping
from edml.helpers.metrics import ModelMetricResultContainer, ModelMetricResult


def create_metric_result_container(round_results):
    containers = []
    for round_result in round_results:
        containers += [
            ModelMetricResultContainer(
                [ModelMetricResult(*result) for result in round_result]
            )
        ]
    return containers


class EarlyStoppingTest(unittest.TestCase):
    def setUp(self):
        self.early_stopping = EarlyStopping(patience=2, metric="acc")

    def test_no_stop(self):
        round_results = [
            [("d1", "acc", "val", 0.1, 42)],
            [("d1", "acc", "val", 0.2, 42)],
            [("d1", "acc", "val", 0.3, 42)],
        ]
        expected_result = [False, False, False]

        results = self._call_early_stopping(round_results)

        self.assertEqual(results, expected_result)

    def test_no_stop_for_missing_metric(self):
        round_results = [
            [("d1", "acc", "val", 0.2, 42)],
            [("d1", "f1", "val", 0.1, 42)],
            [("d1", "acc", "train", 0.0, 42)],
            [("d1", "acc", "test", 0.0, 42)],
        ]
        expected_result = [False, False, False, False]

        results = self._call_early_stopping(round_results)

        self.assertEqual(results, expected_result)

    def test_no_stop_with_aggregation(self):
        round_results = [
            [("d1", "acc", "val", 0.2, 42), ("d2", "acc", "val", 0.2, 42)],
            [("d1", "acc", "val", 0.21, 42), ("d2", "acc", "val", 0.2, 42)],  # better
            [("d1", "acc", "val", 0.21, 42), ("d2", "acc", "val", 0.2, 42)],  # same
            [("d1", "acc", "val", 0.3, 42), ("d2", "acc", "val", 0.13, 42)],  # better
            [("d1", "acc", "val", 0.29, 42), ("d2", "acc", "val", 0.13, 42)],
        ]  # worse
        expected_result = [False, False, False, False, False]

        results = self._call_early_stopping(round_results)

        self.assertEqual(results, expected_result)

    def test_stop_with_aggregation_no_improvement(self):
        round_results = [
            [("d1", "acc", "val", 0.2, 42), ("d2", "acc", "val", 0.2, 42)],
            [("d1", "acc", "val", 0.2, 42), ("d2", "acc", "val", 0.2, 42)],
            [("d1", "acc", "val", 0.3, 42), ("d2", "acc", "val", 0.1, 42)],
        ]
        expected_result = [False, False, True]

        results = self._call_early_stopping(round_results)

        self.assertEqual(results, expected_result)

    def test_stop_with_aggregation_getting_worse(self):
        round_results = [
            [("d1", "acc", "val", 0.2, 42), ("d2", "acc", "val", 0.2, 42)],
            [("d1", "acc", "val", 0.1, 42), ("d2", "acc", "val", 0.2, 42)],
            [("d1", "acc", "val", 0.1, 42), ("d2", "acc", "val", 0.1, 42)],
        ]
        expected_result = [False, False, True]

        results = self._call_early_stopping(round_results)

        self.assertEqual(results, expected_result)

    def _call_early_stopping(self, round_results):
        metrics = create_metric_result_container(round_results)
        results = []
        for idx, call in enumerate(metrics):
            results.append(self.early_stopping(call, idx))
        return results

import unittest

from edml.controllers.strategy_optimization import (
    DeviceParams,
    GlobalParams,
    ServerChoiceOptimizer,
    EnergySimulator,
)


class StrategyOptimizationTest(unittest.TestCase):

    def setUp(self):
        self.device_params_list = [
            DeviceParams(
                device_id="d0",
                initial_battery=3000,
                current_battery=10000,
                train_samples=2,
                validation_samples=1,
                comp_latency_factor=1,
            ),
            DeviceParams(
                device_id="d1",
                initial_battery=2000,
                current_battery=10000,
                train_samples=3,
                validation_samples=1,
                comp_latency_factor=2,
            ),
            DeviceParams(
                device_id="d2",
                initial_battery=1000,
                current_battery=10000,
                train_samples=4,
                validation_samples=1,
                comp_latency_factor=0.5,
            ),
        ]
        self.global_params = GlobalParams(
            cost_per_sec=1,
            cost_per_byte_sent=1,
            cost_per_byte_received=1,
            cost_per_flop=1,
            client_fw_flops=10,
            server_fw_flops=20,
            smashed_data_size=50,
            label_size=5,
            gradient_size=50,
            batch_size=1,
            client_norm_fw_time=1,
            client_norm_bw_time=2,
            server_norm_fw_time=2,
            server_norm_bw_time=4,
            client_weights_size=10,
            server_weights_size=10,
            optimizer_state_size=10,
        )
        self.optimizer = ServerChoiceOptimizer(
            self.device_params_list, self.global_params
        )

    def test_num_devices(self):
        self.assertEqual(self.optimizer._num_devices(), 3)

    def test_total_battery(self):
        self.assertEqual(self.optimizer._total_battery(), 30000)

    def test_train_dataset_size(self):
        self.assertEqual(self.optimizer._total_train_dataset_size(), 9)

    def test_validation_dataset_size(self):
        self.assertEqual(self.optimizer._total_validation_dataset_size(), 3)

    def test_get_device_params(self):
        self.assertEqual(self.optimizer._get_device_params("d0").device_id, "d0")
        self.assertEqual(self.optimizer._get_device_params("d1").device_id, "d1")
        self.assertEqual(self.optimizer._get_device_params("d2").device_id, "d2")

    def test_transmission_latency_no_values(self):
        self.assertEqual(self.optimizer._transmission_latency(), 0)

    def test_transmission_latency_with_values(self):
        self.global_params.train_global_time = 100
        self.global_params.last_server_device_id = "d0"
        self.assertEqual(self.optimizer._transmission_latency(), 6.5)

    def test_round_runtime_with_server_no_latency(self):
        self.assertEqual(self.optimizer.round_runtime_with_server("d0"), 93.5)
        self.assertEqual(self.optimizer.round_runtime_with_server("d1"), 153.5)
        self.assertEqual(self.optimizer.round_runtime_with_server("d2"), 63.5)

    def test_round_runtime_with_server_with_latency(self):
        self.global_params.train_global_time = 100
        self.global_params.last_server_device_id = "d0"
        self.assertEqual(self.optimizer.round_runtime_with_server("d0"), 100)
        self.assertEqual(self.optimizer.round_runtime_with_server("d1"), 160)
        self.assertEqual(self.optimizer.round_runtime_with_server("d2"), 70)

    def test_num_flops_per_round_on_device(self):
        self.assertEqual(self.optimizer.num_flops_per_round_on_device("d0", "d0"), 670)
        self.assertEqual(self.optimizer.num_flops_per_round_on_device("d1", "d0"), 100)
        self.assertEqual(self.optimizer.num_flops_per_round_on_device("d2", "d0"), 130)

    def test_num_bytes_sent_per_round_on_device(self):
        self.assertEqual(
            self.optimizer.num_bytes_sent_per_round_on_device("d0", "d0"), 390
        )
        self.assertEqual(
            self.optimizer.num_bytes_sent_per_round_on_device("d1", "d0"), 230
        )
        self.assertEqual(
            self.optimizer.num_bytes_sent_per_round_on_device("d2", "d0"), 285
        )

    def test_num_bytes_received_per_round_on_device(self):
        self.assertEqual(
            self.optimizer.num_bytes_received_per_round_on_device("d0", "d0"), 535
        )
        self.assertEqual(
            self.optimizer.num_bytes_received_per_round_on_device("d1", "d0"), 160
        )
        self.assertEqual(
            self.optimizer.num_bytes_received_per_round_on_device("d2", "d0"), 210
        )

    def test_energy_per_round_on_device(self):
        self.assertEqual(self.optimizer.energy_per_round_on_device("d0", "d0"), 1688.5)
        self.assertEqual(self.optimizer.energy_per_round_on_device("d1", "d0"), 583.5)
        self.assertEqual(self.optimizer.energy_per_round_on_device("d2", "d0"), 718.5)

    def test_optimize(self):
        solution, status = self.optimizer.optimize()
        self.assertEqual(solution, {"d0": 4.0, "d1": 3.0, "d2": 3.0})
        self.assertEqual(status, 0)


class EnergySimulatorTest(unittest.TestCase):
    def setUp(self):
        self.device_params_list = [
            DeviceParams(
                device_id="d0",
                initial_battery=3000,
                current_battery=10000,
                train_samples=2,
                validation_samples=1,
                comp_latency_factor=1,
            ),
            DeviceParams(
                device_id="d1",
                initial_battery=2000,
                current_battery=10000,
                train_samples=3,
                validation_samples=1,
                comp_latency_factor=2,
            ),
            DeviceParams(
                device_id="d2",
                initial_battery=1000,
                current_battery=10000,
                train_samples=4,
                validation_samples=1,
                comp_latency_factor=0.5,
            ),
        ]
        self.global_params = GlobalParams(
            cost_per_sec=1,
            cost_per_byte_sent=1,
            cost_per_byte_received=1,
            cost_per_flop=1,
            client_fw_flops=10,
            server_fw_flops=20,
            smashed_data_size=50,
            label_size=5,
            gradient_size=50,
            batch_size=1,
            client_norm_fw_time=1,
            client_norm_bw_time=2,
            server_norm_fw_time=2,
            server_norm_bw_time=4,
            client_weights_size=10,
            server_weights_size=10,
            optimizer_state_size=10,
        )
        self.simulator = EnergySimulator(self.device_params_list, self.global_params)

    def test_simulate_greedy_selection(self):
        num_rounds, schedule, remaining_batteries = (
            self.simulator.simulate_greedy_selection()
        )
        self.assertEqual(num_rounds, 10)
        self.assertEqual(
            schedule, ["d0", "d1", "d2", "d0", "d1", "d2", "d0", "d1", "d0", "d2"]
        )
        self.assertEqual(remaining_batteries, [465.0, 985.0, 265.0])

    def test_simulate_smart_selection(self):
        num_rounds, solution, remaining_batteries = (
            self.simulator.simulate_smart_selection()
        )
        self.assertEqual(num_rounds, 10)
        self.assertEqual(solution, {"d0": 4.0, "d1": 3.0, "d2": 3.0})
        self.assertEqual(remaining_batteries, [465.0, 985.0, 265.0])

    def test_fl_round_time(self):
        self.assertEqual(self.simulator._fl_round_time(), 60.0)

    def test_fl_flops_on_device(self):
        self.assertEqual(self.simulator._fl_flops_on_device("d0"), 210)

    def test_fl_data_sent_per_device(self):
        self.assertEqual(self.simulator._fl_data_sent_per_device(), 20)

    def test_fl_data_received_per_device(self):
        self.assertEqual(self.simulator._fl_data_received_per_device(), 20)

    def test_fl_energy_per_round_on_device(self):
        self.assertEqual(self.simulator._fl_energy_per_round_on_device("d0"), 310)

    def test_simulate_federated_learning(self):
        num_rounds, remaining_batteries = self.simulator.simulate_federated_learning()
        self.assertEqual(num_rounds, 20)
        self.assertEqual(remaining_batteries, [3800, 2000, 200])


class TestWithRealData(unittest.TestCase):
    """Case studies for estimating the number of rounds of each strategy with given energy constratins."""

    def setUp(self):
        self.train_samples = [9600, 9600, 9600, 9600, 9600]
        self.validation_samples = [2400, 2400, 2400, 2400, 2400]
        self.current_batteries = [3750, 3750, 3750, 3750, 3750]
        self.comp_latency = [1, 1, 1, 1, 1]
        self.cost_per_sec = 1
        self.cost_per_mbyte_sent = 1
        self.cost_per_mbyte_received = 1
        self.cost_per_mflop = 1

    def _init_params_and_optimizer(self):
        self.device_params_list = [
            DeviceParams(
                device_id="d0",
                initial_battery=3750,
                current_battery=self.current_batteries[0],
                train_samples=self.train_samples[0],
                validation_samples=self.validation_samples[0],
                comp_latency_factor=self.comp_latency[0],
            ),
            DeviceParams(
                device_id="d1",
                initial_battery=3750,
                current_battery=self.current_batteries[1],
                train_samples=self.train_samples[1],
                validation_samples=self.validation_samples[1],
                comp_latency_factor=self.comp_latency[1],
            ),
            DeviceParams(
                device_id="d2",
                initial_battery=3750,
                current_battery=self.current_batteries[2],
                train_samples=self.train_samples[2],
                validation_samples=self.validation_samples[2],
                comp_latency_factor=self.comp_latency[2],
            ),
            DeviceParams(
                device_id="d3",
                initial_battery=3750,
                current_battery=self.current_batteries[3],
                train_samples=self.train_samples[3],
                validation_samples=self.validation_samples[3],
                comp_latency_factor=self.comp_latency[3],
            ),
            DeviceParams(
                device_id="d4",
                initial_battery=3750,
                current_battery=self.current_batteries[4],
                train_samples=self.train_samples[4],
                validation_samples=self.validation_samples[4],
                comp_latency_factor=self.comp_latency[4],
            ),
        ]
        self.global_params = GlobalParams(
            cost_per_sec=self.cost_per_sec,
            cost_per_byte_sent=self.cost_per_mbyte_sent / 1000000,
            cost_per_byte_received=self.cost_per_mbyte_received / 1000000,
            cost_per_flop=self.cost_per_mflop / 1000000,
            client_fw_flops=5405760,
            server_fw_flops=11215800,
            smashed_data_size=36871,
            label_size=14,
            gradient_size=36871,
            batch_size=64,
            client_norm_fw_time=0.0001318198063394479,
            client_norm_bw_time=1.503657614975644e-05,
            server_norm_fw_time=2.1353501238321005e-05,
            server_norm_bw_time=3.1509113154913254e-05,
            client_weights_size=71678,
            # train global response size 15878758
            server_weights_size=15807080,
            optimizer_state_size=0,  # was not recorded then
        )
        self.optimizer = ServerChoiceOptimizer(
            self.device_params_list, self.global_params
        )
        self.simulator = EnergySimulator(self.device_params_list, self.global_params)

    def test_with_actual_data(self):
        self.current_batteries = [3719.654, 3717.608, 3711.923, 3708.051, 3704.294]
        self.cost_per_sec = 1  # 0.1
        self.cost_per_mbyte_sent = 0.05  # 0.002
        self.cost_per_mbyte_received = 0.05  # 0.0005
        self.cost_per_mflop = 0.00025  # 0.0005
        self._init_params_and_optimizer()
        solution, status = self.optimizer.optimize()
        self.assertEqual(
            solution, {"d0": 4.0, "d1": 4.0, "d2": 4.0, "d3": 3.0, "d4": 0.0}
        )

    def test_simulate_greedy_selection_with_actual_data(self):
        self.current_batteries = [3719.654, 3717.608, 3711.923, 3708.051, 3704.294]
        self.cost_per_sec = 1  # 0.1
        self.cost_per_mbyte_sent = 0.05  # 0.002
        self.cost_per_mbyte_received = 0.05  # 0.0005
        self.cost_per_mflop = 0.00025  # 0.0005
        self._init_params_and_optimizer()
        num_rounds, schedule, remaining_batteries = (
            self.simulator.simulate_greedy_selection()
        )
        self.assertEqual(num_rounds, 15)
        self.assertEqual(
            schedule,
            [
                "d0",
                "d1",
                "d2",
                "d3",
                "d4",
                "d0",
                "d1",
                "d2",
                "d3",
                "d4",
                "d0",
                "d1",
                "d2",
                "d3",
                "d4",
            ],
        )

    def test_unequal_split(self):
        self.current_batteries = [5750, 4750, 3750, 2750, 1750]
        self.train_samples = [4800, 9600, 19200, 7200, 7200]
        self.validation_samples = [1200, 2400, 4800, 1800, 1800]
        self.cost_per_sec = 1
        self.cost_per_mbyte_sent = 0.05
        self.cost_per_mbyte_received = 0.05
        self.cost_per_mflop = 0.00025
        self._init_params_and_optimizer()
        solution, status = self.optimizer.optimize()
        self.assertEqual(
            solution, {"d0": 8.0, "d1": 5.0, "d2": 1.0, "d3": 2.0, "d4": 0.0}
        )

    def test_unequal_split_and_batteries_with_high_communication_cost(self):
        self.current_batteries = [3750, 3750, 3750, 3750, 3750]
        data_partitions = [0.1, 0.1, 0.6, 0.1, 0.1]
        self.train_samples = [partition * 48000 for partition in data_partitions]
        self.validation_samples = [partition * 12000 for partition in data_partitions]
        self.cost_per_sec = 0.1
        self.cost_per_mbyte_sent = 0.1
        self.cost_per_mbyte_received = 0.1
        self.cost_per_mflop = 0.00001
        self._init_params_and_optimizer()
        solution, status = self.optimizer.optimize()
        num_rounds, schedule, remaining_batteries = (
            self.simulator.simulate_greedy_selection()
        )
        print(solution, sum(solution.values()), num_rounds, remaining_batteries)
        self.assertGreater(sum(solution.values()), num_rounds)

    def test_unequal_battery_unequal_processing_high_time_cost(self):
        self.current_batteries = [5750, 4750, 3750, 2750, 1750]
        data_partitions = [0.2, 0.1, 0.1, 0.1, 0.5]
        self.comp_latency = [5, 5, 5, 5, 1]
        self.train_samples = [partition * 48000 for partition in data_partitions]
        self.validation_samples = [partition * 12000 for partition in data_partitions]
        self.cost_per_sec = 10
        self.cost_per_mbyte_sent = 0.0  # 1
        self.cost_per_mbyte_received = 0.0  # 1
        self.cost_per_mflop = 0.0000  # 5
        self._init_params_and_optimizer()
        solution, status = self.optimizer.optimize()
        num_rounds, schedule, remaining_batteries = (
            self.simulator.simulate_greedy_selection()
        )
        print(solution, sum(solution.values()), num_rounds)
        self.assertGreater(sum(solution.values()), num_rounds)

    def test_unequal_battery_unequal_processing_high_time_cost2(self):
        self.current_batteries = [3750, 3750, 3750, 3750, 3750]
        data_partitions = [0.05, 0.1, 0.1, 0.1, 0.65]
        self.comp_latency = [10, 10, 10, 10, 1]
        self.train_samples = [partition * 48000 for partition in data_partitions]
        self.validation_samples = [partition * 12000 for partition in data_partitions]
        self.cost_per_sec = 3
        self.cost_per_mbyte_sent = 0.05
        self.cost_per_mbyte_received = 0.05
        self.cost_per_mflop = 0.000005
        self._init_params_and_optimizer()
        solution, status = self.optimizer.optimize()
        num_rounds, schedule, remaining_batteries = (
            self.simulator.simulate_greedy_selection()
        )
        print(solution, sum(solution.values()), num_rounds, remaining_batteries)
        self.assertGreater(sum(solution.values()), num_rounds)

    def test_unequal_battery_unequal_processing_high_time_cost3(self):
        self.current_batteries = [3750, 3750, 3750, 3750, 3750]
        data_partitions = [0.01, 0.01, 0.01, 0.01, 0.96]
        self.comp_latency = [100, 100, 100, 100, 1]
        self.train_samples = [partition * 48000 for partition in data_partitions]
        self.validation_samples = [partition * 12000 for partition in data_partitions]
        self.cost_per_sec = 3
        self.cost_per_mbyte_sent = 0.05  # 375
        self.cost_per_mbyte_received = 0.05
        self.cost_per_mflop = 0.000005
        self._init_params_and_optimizer()
        solution, status = self.optimizer.optimize()
        num_rounds, schedule, remaining_batteries = (
            self.simulator.simulate_greedy_selection()
        )
        print(solution, sum(solution.values()), num_rounds, remaining_batteries)
        self.assertGreater(sum(solution.values()), num_rounds)

    def test_unequal_battery_unequal_processing_high_time_cost4(self):
        self.current_batteries = [3750, 3750, 3750, 3750, 3750]
        data_partitions = [0.1, 0.1, 0.1, 0.1, 0.6]
        self.comp_latency = [10, 10, 10, 10, 1]
        self.train_samples = [partition * 48000 for partition in data_partitions]
        self.validation_samples = [partition * 12000 for partition in data_partitions]
        self.cost_per_sec = 4
        self.cost_per_mbyte_sent = 0.01
        self.cost_per_mbyte_received = 0.01
        self.cost_per_mflop = 0.00001
        self._init_params_and_optimizer()
        solution, status = self.optimizer.optimize()
        num_rounds, schedule, remaining_batteries = (
            self.simulator.simulate_greedy_selection()
        )
        print(solution, sum(solution.values()), num_rounds, remaining_batteries)
        self.assertGreater(sum(solution.values()), num_rounds)


class TestBug(unittest.TestCase):
    """Case studies for estimating the number of rounds of each strategy with given energy constratins."""

    def setUp(self):
        self.global_params = GlobalParams(
            cost_per_sec=0.02,
            cost_per_byte_sent=2e-10,
            cost_per_byte_received=2e-10,
            cost_per_flop=4.9999999999999995e-14,
            client_fw_flops=88408064,
            server_fw_flops=169728256,
            smashed_data_size=65542.875,
            batch_size=64,
            client_weights_size=416469,
            server_weights_size=6769181,
            optimizer_state_size=6664371,
            train_global_time=204.80956506729126,
            last_server_device_id="d0",
            label_size=14.359375,
            gradient_size=65542.875,
            client_norm_fw_time=0.0002889558474222819,
            client_norm_bw_time=0.00033905270364549425,
            server_norm_fw_time=0.0035084471996721703,
            server_norm_bw_time=0.005231082973148723,
        )
        self.device_params_list = [
            DeviceParams(
                device_id="d0",
                initial_battery=400.0,
                current_battery=393.09099611222206,
                train_samples=4500,
                validation_samples=500,
                comp_latency_factor=3.322075197090219,
            ),
            DeviceParams(
                device_id="d1",
                initial_battery=400.0,
                current_battery=398.2543529099223,
                train_samples=4500,
                validation_samples=500,
                comp_latency_factor=3.5023548154181494,
            ),
            DeviceParams(
                device_id="d2",
                initial_battery=300.0,
                current_battery=297.6957505401916,
                train_samples=4500,
                validation_samples=500,
                comp_latency_factor=3.2899405054194144,
            ),
            DeviceParams(
                device_id="d3",
                initial_battery=200.0,
                current_battery=197.12318101733192,
                train_samples=4500,
                validation_samples=500,
                comp_latency_factor=3.4622951339692114,
            ),
            DeviceParams(
                device_id="d4",
                initial_battery=200.0,
                current_battery=194.35766488685508,
                train_samples=27000,
                validation_samples=3000,
                comp_latency_factor=1.0,
            ),
        ]

        self.optimizer = ServerChoiceOptimizer(
            self.device_params_list, self.global_params
        )
        self.simulator = EnergySimulator(self.device_params_list, self.global_params)

    def test_bug(self):
        solution, status = self.optimizer.optimize()
        num_rounds, schedule, remaining_batteries = (
            self.simulator.simulate_smart_selection()
        )
        print(solution, sum(solution.values()), num_rounds, remaining_batteries)
        self.assertEqual(sum(solution.values()), num_rounds)

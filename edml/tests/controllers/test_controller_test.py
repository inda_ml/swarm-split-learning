import unittest
from unittest.mock import patch, Mock, call

from edml.controllers.test_controller import TestController
from edml.core.device import DeviceRequestDispatcher
from edml.tests.controllers.test_helper import load_sample_config


class TestControllerTest(unittest.TestCase):

    def test_run_with_test_data(self):
        cfg = load_sample_config()
        cfg.best_round = 42
        test_controller = TestController(cfg)
        mock = Mock(spec=DeviceRequestDispatcher)
        mock.active_devices.return_value = ["d0", "d1"]
        test_controller.request_dispatcher = mock

        with patch(
            "edml.controllers.base_controller.BaseController._load_weights"
        ) as mock_load_weights:
            mock_load_weights.return_value = {"weights": 42}, {"weights": 43}

            test_controller._train()

            mock_load_weights.assert_called_once_with(42)
            mock.evaluate_global_on.assert_has_calls(
                [call("d0", False, True), call("d1", False, True)]
            )

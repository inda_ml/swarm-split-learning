import math
import unittest
from unittest.mock import Mock, patch, call

from omegaconf import DictConfig
from torch import Tensor

from edml.controllers.fed_controller import FedController, fed_average
from edml.core.device import DeviceRequestDispatcher
from edml.generated.connection_pb2 import FullModelTrainResponse
from edml.helpers.metrics import (
    ModelMetricResultContainer,
    ModelMetricResult,
    DiagnosticMetricResultContainer,
)
from edml.helpers.proto_helpers import weights_to_proto, metrics_to_proto
from edml.tests.controllers.test_helper import load_sample_config, get_side_effect


class FederatedLearningTest(unittest.TestCase):

    def test_fed_average_unweighted(self):
        avg = fed_average([{"weights": Tensor([42])}, {"weights": Tensor([43])}])
        self.assertEqual(avg, {"weights": Tensor([42.5])})

    def test_fed_average_weighted_by_examples(self):
        avg = fed_average(
            [{"weights": Tensor([42])}, {"weights": Tensor([43])}], [10, 30]
        )
        self.assertEqual(avg, {"weights": Tensor([42.75])})

    def test_fed_average_weighted_by_percentage(self):
        avg = fed_average(
            [{"weights": Tensor([42])}, {"weights": Tensor([43])}], [0.25, 0.75]
        )
        self.assertEqual(avg, {"weights": Tensor([42.75])})


class FedControllerTest(unittest.TestCase):
    def setUp(self) -> None:
        self.fed_controller = FedController(load_sample_config())
        self.mock = Mock(spec=DeviceRequestDispatcher)
        self.mock.active_devices.return_value = ["d0", "d1"]
        self.fed_controller.request_dispatcher = self.mock

    def test_fed_train_round(self):
        def fed_train_side_effect(*args, **kwargs):
            if args[0] == "d0":
                return (
                    {"weights": Tensor([42])},
                    {"weights": Tensor([43])},
                    100,
                    ModelMetricResultContainer(
                        [ModelMetricResult("d0", "acc", "train", Tensor([42]), 100)]
                    ),
                    DiagnosticMetricResultContainer(),
                )
            elif args[0] == "d1":
                return (
                    {"weights": Tensor([44])},
                    {"weights": Tensor([45])},
                    100,
                    ModelMetricResultContainer(
                        [ModelMetricResult("d1", "acc", "train", Tensor([44]), 100)]
                    ),
                    DiagnosticMetricResultContainer(),
                )

        self.mock.federated_train_on.side_effect = fed_train_side_effect

        client_weights, server_weights, metrics = self.fed_controller._fed_train_round()

        self.assertEqual(client_weights, {"weights": Tensor([43])})
        self.assertEqual(server_weights, {"weights": Tensor([44])})
        self.assertEqual(
            metrics,
            ModelMetricResultContainer(
                [
                    ModelMetricResult("d0", "acc", "train", Tensor([42]), 100),
                    ModelMetricResult("d1", "acc", "train", Tensor([44]), 100),
                ]
            ),
        )

    def test_train(self):
        self.mock.federated_train_on.return_value = (
            {"weights": Tensor([42])},
            {"weights": Tensor([43])},
            100,
            ModelMetricResultContainer(
                [ModelMetricResult("d0", "acc", "train", Tensor([42]), 100)]
            ),
            DiagnosticMetricResultContainer(),
        )

        self.fed_controller._train()

        self.mock.federated_train_on.assert_has_calls(
            [
                (("d0", 0), {}),
                (("d1", 0), {}),
            ]
        )
        self.mock.active_devices.assert_called()
        self.mock.set_weights_on.assert_has_calls(
            [
                call("d0", {"weights": Tensor([42])}, True, wait_for_ready=False),
                call("d1", {"weights": Tensor([42])}, True, wait_for_ready=False),
                call("d0", {"weights": Tensor([43])}, False, wait_for_ready=False),
                call("d1", {"weights": Tensor([43])}, False, wait_for_ready=False),
            ]
        )

    def test_train_with_empty_batteries_after_train_round(self):
        def side_effect(*args, **kwargs):
            if self.mock.federated_train_on.call_count > 1:
                self.mock.active_devices.return_value = []
            return (
                {"weights": Tensor([42])},
                {"weights": Tensor([43])},
                100,
                ModelMetricResultContainer(
                    [ModelMetricResult("d0", "acc", "train", Tensor([42]), 42)]
                ),
                DiagnosticMetricResultContainer(),
            )

        self.mock.federated_train_on.side_effect = side_effect

        self.fed_controller._train()

        self.mock.federated_train_on.assert_has_calls(
            [
                (("d0", 0), {}),
                (("d1", 0), {}),
            ]
        )
        self.mock.active_devices.assert_called()
        self.mock.set_weights_on.assert_not_called()


class FedTrainRoundThreadingTest(unittest.TestCase):

    def setUp(self):
        self.fed_controller = FedController(
            DictConfig(
                {
                    "topology": {"devices": []},
                    "num_devices": 0,
                    "wandb": {"enabled": False},
                    "experiment": {"early_stopping": False},
                    "simulate_parallelism": False,
                }
            )
        )
        self.request_dispatcher = DeviceRequestDispatcher([])

    def test_fed_train_round(self):
        """Test _fed_train_round with an increasing number of threads. Mocking the behavior of the FedControllers
        request_dispatcher to let every second request fail.
        Afterward, the assertions make sure that the correct weights and metrics are returned.
        Furthermore, the calls of fed_average are checked to be as expected."""
        for n in range(2, 20):
            with self.subTest(method_name=f"Test fed train with {n} threads"):
                with patch(
                    "edml.controllers.fed_controller.fed_average"
                ) as fed_average_mock:
                    print(f"Test with {n} threads")
                    self.request_dispatcher.connections = {
                        f"d{i}": Mock(spec=["FullModelTraining"]) for i in range(n)
                    }
                    self.response = FullModelTrainResponse(
                        client_weights=weights_to_proto({"weights": Tensor([42])}),
                        server_weights=weights_to_proto({"weights": Tensor([43])}),
                        num_samples=100,
                        metrics=metrics_to_proto(
                            ModelMetricResultContainer(
                                [
                                    ModelMetricResult(
                                        "d0", "acc", "train", Tensor([42]), 100
                                    )
                                ]
                            )
                        ),
                        diagnostic_metrics=metrics_to_proto(
                            DiagnosticMetricResultContainer()
                        ),
                    )
                    for i, mock in enumerate(
                        self.request_dispatcher.connections.values()
                    ):
                        mock.FullModelTraining.side_effect = get_side_effect(
                            i, self.response
                        )  # let every second fail
                    self.fed_controller.request_dispatcher = self.request_dispatcher
                    self.fed_controller._refresh_active_devices()

                    def mock_with_actual_behavior(*args, **kwargs):
                        """Mock with actual behavior to check if fed_average is called."""
                        return fed_average(*args, **kwargs)

                    fed_average_mock.side_effect = mock_with_actual_behavior

                    client_weights, server_weights, metrics = (
                        self.fed_controller._fed_train_round()
                    )

                    expected_valid_responses = math.ceil(n / 2)
                    self.assertEqual(client_weights, {"weights": Tensor([42])})
                    self.assertEqual(server_weights, {"weights": Tensor([43])})
                    self.assertEqual(
                        metrics.get_aggregated_metrics(),
                        ModelMetricResultContainer(
                            [
                                ModelMetricResult(
                                    "d0",
                                    "acc",
                                    "train",
                                    Tensor([42]),
                                    100 * expected_valid_responses,
                                )
                            ]
                        ).get_aggregated_metrics(),
                    )
                    # Check that fed_average is called with the correct number of arguments
                    fed_average_mock.assert_has_calls(
                        [
                            call(
                                model_weights=[{"weights": Tensor([42])}]
                                * expected_valid_responses,
                                weighting_scheme=[100] * expected_valid_responses,
                            ),
                            call(
                                model_weights=[{"weights": Tensor([43])}]
                                * expected_valid_responses,
                                weighting_scheme=[100] * expected_valid_responses,
                            ),
                        ]
                    )

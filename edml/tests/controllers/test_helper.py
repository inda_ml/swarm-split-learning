import os.path
import time
from unittest.mock import Mock

import grpc
from omegaconf import OmegaConf
from torch import Tensor

from edml.generated import connection_pb2
from edml.generated.connection_pb2_grpc import DeviceStub
from edml.helpers import proto_helpers


def mock_establish_device_connections():
    """Mocks device connections for testing.
    Sets return values for some methods offered by the DeviceStub class.
    """
    connections = {
        "d0": Mock(spec=DeviceStub(Mock(grpc.Channel))),
        "d1": Mock(spec=DeviceStub(Mock(grpc.Channel))),
    }
    client0_weights = proto_helpers.weights_to_proto({"weights": Tensor([42])})
    server0_weights = proto_helpers.weights_to_proto({"weights": Tensor([43])})
    client1_weights = proto_helpers.weights_to_proto({"weights": Tensor([44])})
    server1_weights = proto_helpers.weights_to_proto({"weights": Tensor([45])})

    connections["d0"].FullModelTraining.return_value = (
        connection_pb2.FullModelTrainResponse(
            client_weights=client0_weights,
            server_weights=server0_weights,
        )
    )
    connections["d1"].FullModelTraining.return_value = (
        connection_pb2.FullModelTrainResponse(
            client_weights=client1_weights,
            server_weights=server1_weights,
        )
    )

    connections["d0"].Evaluate.return_value = connection_pb2.EvalResponse()
    connections["d1"].Evaluate.return_value = connection_pb2.EvalResponse()

    connections["d0"].TrainGlobal.return_value = connection_pb2.TrainGlobalResponse(
        client_weights=client0_weights,
        server_weights=server0_weights,
    )
    connections["d1"].TrainGlobal.return_value = connection_pb2.TrainGlobalResponse(
        client_weights=client1_weights,
        server_weights=server1_weights,
    )

    connections["d0"].SetWeights.return_value = connection_pb2.SetWeightsResponse()
    connections["d1"].SetWeights.return_value = connection_pb2.SetWeightsResponse()

    return connections


def load_sample_config():
    return OmegaConf.load(os.path.join(os.path.dirname(__file__), "sample_config.yaml"))


def assert_experiment_started_and_ended(test_case):
    test_case.connections["d0"].StartExperiment.assert_called_once()
    test_case.connections["d1"].StartExperiment.assert_called_once()
    test_case.connections["d0"].EndExperiment.assert_called_once()
    test_case.connections["d1"].EndExperiment.assert_called_once()


def get_side_effect(i, valid_response):
    """Constructs a side effect based on integer input.
    If even, the side effect returns a valid response, else raises a grpc error"""
    if i % 2 == 0:

        def side_effect(*args, **kwargs):
            time.sleep(0.2)
            return valid_response

    else:

        def side_effect(*args, **kwargs):
            time.sleep(0.2)
            raise grpc.RpcError()

    return side_effect

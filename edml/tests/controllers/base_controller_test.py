import unittest
from unittest.mock import Mock

from edml.controllers.base_controller import BaseController
from edml.core.device import DeviceRequestDispatcher
from edml.tests.controllers.test_helper import load_sample_config


class TestBaseController(BaseController):
    """Subclass of BaseController for testing purposes."""

    def _train(self):
        pass


class BaseControllerTest(unittest.TestCase):

    def setUp(self):
        self.base_controller = TestBaseController(load_sample_config())
        self.mock = Mock(spec=DeviceRequestDispatcher)
        self.base_controller.request_dispatcher = self.mock

    def test_devices_empty_or_only_server_left_with_server_only(self):
        self.mock.active_devices.return_value = ["d0"]
        self.base_controller._refresh_active_devices()
        self.assertTrue(self.base_controller._devices_empty_or_only_server_left("d0"))

    def test_devices_empty_or_only_server_left_with_no_devices(self):
        self.mock.active_devices.return_value = []
        self.base_controller._refresh_active_devices()
        self.assertTrue(self.base_controller._devices_empty_or_only_server_left("d0"))

    def test_devices_empty_or_only_server_left_with_two_devices(self):
        self.mock.active_devices.return_value = ["d0", "d1"]
        self.base_controller._refresh_active_devices()
        self.assertFalse(self.base_controller._devices_empty_or_only_server_left("d0"))
        self.assertFalse(self.base_controller._devices_empty_or_only_server_left("d1"))

    def test_battery_status(self):
        self.mock.get_battery_status_on.return_value = 42
        self.assertEqual(
            self.base_controller._get_battery_status(), {"d0": 42, "d1": 42}
        )

    def test_battery_status_with_empty_device(self):
        self.mock.get_battery_status_on.return_value = 42
        self.assertEqual(
            self.base_controller._get_battery_status(), {"d0": 42, "d1": 42}
        )

        def battery_status_side_effect(*args, **kwargs):
            if args[0] == "d0":
                return 42
            elif args[0] == "d1":
                return False

        self.mock.get_battery_status_on.side_effect = battery_status_side_effect
        self.mock.active_devices.return_value = [
            "d0",
            "d1",
        ]  # d1 is inactive, but still in active_devices
        self.assertEqual(
            self.base_controller._get_battery_status(), {"d0": 42, "d1": None}
        )

    def test_update_device_battery_status(self):
        self.mock.get_battery_status_on.return_value = 42
        self.base_controller._update_devices_battery_status()
        self.assertEqual(
            self.base_controller.device_batteries_status, {"d0": 42, "d1": 42}
        )

        self.mock.active_devices.return_value = ["d0"]
        self.base_controller._refresh_active_devices()
        self.base_controller._update_devices_battery_status()
        self.assertEqual(
            self.base_controller.device_batteries_status, {"d0": 42, "d1": None}
        )

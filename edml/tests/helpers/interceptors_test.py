import threading
import unittest
from unittest.mock import Mock, call

from grpc_interceptor.testing import dummy_client, DummyRequest
from torch import Tensor

from edml.core.battery import Battery, BatteryEmptyException
from edml.generated.connection_pb2 import TrainGlobalRequest, TrainGlobalResponse
from edml.helpers.interceptors import (
    DeviceServerInterceptor,
    _proto_serialized_size,
    DeviceClientInterceptor,
)
from edml.helpers.logging import SimpleLogger
from edml.helpers.proto_helpers import weights_to_proto


class ProtoByteSizeTest(unittest.TestCase):
    def setUp(self):
        self.context = Mock()
        self.context.invocation_metadata.return_value = []
        self.context.trailing_metadata.return_value = []

    def test_TrainGlobalRequest_byte_size(self):
        proto_object = TrainGlobalRequest(epochs=42)
        self.assertEqual(_proto_serialized_size(proto_object), 7)

    def test_TrainGlobalResponse_byte_size(self):
        proto_object = TrainGlobalResponse(
            server_weights=weights_to_proto({"weights": Tensor([42])}),
            client_weights=weights_to_proto({"weights": Tensor([42])}),
        )
        self.assertEqual(_proto_serialized_size(proto_object), 845)


class DeviceServerInterceptorTest(unittest.TestCase):

    def setUp(self):
        self.logger = Mock(spec=SimpleLogger)
        self.battery = Mock(spec=Battery)

    def test_measure_byte_size(self):
        interceptors = [
            DeviceServerInterceptor(
                logger=self.logger, stop_event=None, battery=self.battery
            )
        ]
        with dummy_client(special_cases={}, interceptors=interceptors) as client:
            request_input = "request"
            request = DummyRequest(input=request_input)

            self.assertTrue(client.Execute(request).output == request_input)

            self.logger.log.assert_has_calls(
                [
                    call({"/DummyService/Execute_request_size": 14}),
                    call({"/DummyService/Execute_response_size": 14}),
                ]
            )
            self.battery.update_communication_received.assert_called_with(14)
            self.battery.update_communication_sent.assert_called_with(14)

    def test_without_battery(self):
        interceptors = [DeviceServerInterceptor(logger=self.logger, stop_event=None)]
        with dummy_client(special_cases={}, interceptors=interceptors) as client:
            request_input = "request"
            request = DummyRequest(input=request_input)
            self.assertTrue(client.Execute(request).output == request_input)
            self.logger.log.assert_has_calls(
                [
                    call({"/DummyService/Execute_request_size": 14}),
                    call({"/DummyService/Execute_response_size": 14}),
                ]
            )
            self.battery.update_communication_received.assert_not_called()
            self.battery.update_communication_sent.assert_not_called()

    def test_with_empty_battery_at_receiving(self):
        stop_mock = Mock(threading.Event)
        interceptors = [
            DeviceServerInterceptor(
                logger=self.logger, stop_event=stop_mock, battery=self.battery
            )
        ]
        with dummy_client(special_cases={}, interceptors=interceptors) as client:
            request_input = "request"
            request = DummyRequest(input=request_input)
            self.battery.is_empty.return_value = False
            self.battery.update_communication_received.side_effect = (
                BatteryEmptyException
            )

            with self.assertRaises(Exception):
                client.Execute(request)
            stop_mock.set.assert_called()

    def test_with_empty_battery_at_responding(self):
        stop_mock = Mock(threading.Event)
        interceptors = [
            DeviceServerInterceptor(
                logger=self.logger, stop_event=stop_mock, battery=self.battery
            )
        ]
        with dummy_client(special_cases={}, interceptors=interceptors) as client:
            request_input = "request"
            request = DummyRequest(input=request_input)
            self.battery.is_empty.return_value = False
            self.battery.update_communication_sent.side_effect = BatteryEmptyException

            with self.assertRaises(Exception):
                client.Execute(request)
            stop_mock.set.assert_called()


class DeviceClientInterceptorTest(unittest.TestCase):
    def setUp(self):
        self.logger = Mock(spec=SimpleLogger)
        self.battery = Mock(spec=Battery)
        self.stop_event = Mock(threading.Event)

    def test_measure_byte_size(self):
        interceptors = [
            DeviceClientInterceptor(
                logger=self.logger, stop_event=self.stop_event, battery=self.battery
            )
        ]
        with dummy_client(special_cases={}, client_interceptors=interceptors) as client:
            request_input = "request"
            request = DummyRequest(input=request_input)

            self.assertTrue(client.Execute(request).output == request_input)

            self.battery.update_communication_sent.assert_called_with(14)
            self.battery.update_communication_received.assert_called_with(14)

    def test_with_empty_battery_at_sending(self):
        interceptors = [
            DeviceClientInterceptor(
                logger=self.logger, stop_event=self.stop_event, battery=self.battery
            )
        ]
        with dummy_client(special_cases={}, client_interceptors=interceptors) as client:
            request_input = "request"
            request = DummyRequest(input=request_input)
            self.battery.update_communication_sent.side_effect = BatteryEmptyException

            with self.assertRaises(BatteryEmptyException):
                client.Execute(request)
            self.stop_event.set.assert_called()

    def test_with_empty_battery_at_receiving(self):
        interceptors = [
            DeviceClientInterceptor(
                logger=self.logger, stop_event=self.stop_event, battery=self.battery
            )
        ]
        with dummy_client(special_cases={}, client_interceptors=interceptors) as client:
            request_input = "request"
            request = DummyRequest(input=request_input)
            self.battery.update_communication_received.side_effect = (
                BatteryEmptyException
            )

            with self.assertRaises(BatteryEmptyException):
                client.Execute(request)
            self.stop_event.set.assert_called()

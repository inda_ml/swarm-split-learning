import unittest

import torch.utils.data
import torchmetrics

from edml.helpers import metrics
from edml.helpers.metrics import (
    DiagnosticMetricResult,
    compute_metrics_for_optimization,
    DiagnosticMetricResultContainer,
)


def _all_metrics_num_samples_equals(
    container: metrics.ModelMetricContainer, num_samples: int
):
    """Helper method to test if all metric objects have the given empty flag."""
    for metric in container.metrics.values():
        if metric.num_samples != num_samples:
            return False
    return True


class CreateMetricsTest(unittest.TestCase):
    def setUp(self):
        self.cfg = {
            "average_setting": "micro",
            "classes": 5,
            "metrics": ["accuracy", "f1", "auc"],
        }
        self.metric_container = metrics.create_metrics(
            self.cfg["metrics"], self.cfg["classes"], self.cfg["average_setting"]
        )

    def test_create_metrics(self):
        self.assertEqual(len(self.metric_container.metrics), 3)
        self.assertTrue(_all_metrics_num_samples_equals(self.metric_container, 0))


class MetricContainerTest(unittest.TestCase):
    def setUp(self) -> None:
        self.cfg = {
            "average_setting": "micro",
            "classes": 5,
            "metrics": ["accuracy", "f1", "auc"],
        }
        self.metric_container = metrics.create_metrics(
            self.cfg["metrics"], self.cfg["classes"], self.cfg["average_setting"]
        )

    def test_add_metric(self):
        self.assertEqual(len(self.metric_container.metrics.values()), 3)
        self.metric_container.add_metric(torchmetrics.classification.Recall(), "recall")
        self.assertEqual(len(self.metric_container.metrics.values()), 4)

    def test_metric_on_batch(self):
        predictions = torch.Tensor([[0.3351, 0.0562, 0.7052, 0.4191, 0.2356]])
        labels = torch.Tensor([[0, 0, 0, 1, 0]]).int()
        self.assertTrue(_all_metrics_num_samples_equals(self.metric_container, 0))

        res = self.metric_container.metrics_on_batch(predictions, labels)

        self.assertTrue(_all_metrics_num_samples_equals(self.metric_container, 1))
        self.assertEqual(
            res, [torch.Tensor([0.6000]), torch.Tensor([0.0]), torch.Tensor([0.7500])]
        )

    def test_compute_metrics(self):
        predictions_1 = torch.Tensor([[0.3351, 0.0562, 0.7052, 0.4191, 0.2356]])
        labels_1 = torch.Tensor([[0, 0, 0, 1, 0]]).int()
        predictions_2 = torch.Tensor([[0.7075, 0.4229, 0.9530, 0.1434, 0.4220]])
        labels_2 = torch.Tensor([[0, 0, 0, 1, 0]]).int()
        self.metric_container.metrics_on_batch(predictions_1, labels_1)
        self.metric_container.metrics_on_batch(predictions_2, labels_2)
        self.assertTrue(_all_metrics_num_samples_equals(self.metric_container, 2))

        res = self.metric_container.compute_metrics("test", "d1")
        self.assertEqual(type(res), list)
        self.assertEqual(res[0].value, torch.Tensor([0.5000]))  # acc
        self.assertEqual(res[1].value, torch.Tensor([0.0]))  # f1
        self.assertEqual(res[2].value, torch.Tensor([0.2500]))  # auc
        for metric_result in res:
            self.assertEqual(metric_result.num_samples, 2)

    def test_with_empty_metrics(self):
        self.assertTrue(_all_metrics_num_samples_equals(self.metric_container, 0))

        res = self.metric_container.compute_metrics("test", "d1")

        self.assertEqual(res, [])

    def test_reset_metrics(self):
        predictions = torch.Tensor([[0.3351, 0.0562, 0.7052, 0.4191, 0.2356]])
        labels = torch.Tensor([[0, 0, 0, 1, 0]]).int()
        self.metric_container.metrics_on_batch(predictions, labels)
        self.assertTrue(_all_metrics_num_samples_equals(self.metric_container, 1))

        self.metric_container.reset_metrics()

        self.assertTrue(_all_metrics_num_samples_equals(self.metric_container, 0))


class MetricResultContainerTest(unittest.TestCase):
    def setUp(self):
        self.metric_result_container = metrics.ModelMetricResultContainer()

    def test_add_result(self):
        self.assertEqual(len(self.metric_result_container.results), 0)
        result = metrics.ModelMetricResult("d1", "acc", "test", torch.Tensor([42]), 42)
        self.metric_result_container.add_result(result)
        self.assertDictEqual(
            self.metric_result_container.get_raw_metrics(), {("acc", "test"): [result]}
        )

    def test_add_results_with_different_phases(self):
        self.assertEqual(len(self.metric_result_container.results), 0)
        result1 = metrics.ModelMetricResult("d1", "acc", "test", torch.Tensor([42]), 42)
        result2 = metrics.ModelMetricResult("d1", "acc", "val", torch.Tensor([43]), 42)
        self.metric_result_container.add_results([result1, result2])
        self.assertDictEqual(
            self.metric_result_container.get_raw_metrics(),
            {("acc", "test"): [result1], ("acc", "val"): [result2]},
        )

    def test_added_result_with_same_phase(self):
        self.assertEqual(len(self.metric_result_container.results), 0)
        result1 = metrics.ModelMetricResult("d1", "acc", "test", torch.Tensor([42]), 42)
        result2 = metrics.ModelMetricResult("d1", "acc", "test", torch.Tensor([43]), 42)
        self.metric_result_container.add_result(result1)
        self.metric_result_container.add_result(result2)
        self.assertDictEqual(
            self.metric_result_container.get_raw_metrics(),
            {("acc", "test"): [result1, result2]},
        )

    def test_merge_results_into_container(self):
        self.assertEqual(len(self.metric_result_container.results), 0)
        result1 = metrics.ModelMetricResult("d1", "acc", "test", torch.Tensor([42]), 42)
        result2 = metrics.ModelMetricResult("d1", "acc", "test", torch.Tensor([43]), 42)
        result3 = metrics.ModelMetricResult("d1", "acc", "val", torch.Tensor([44]), 42)
        result4 = metrics.ModelMetricResult("d1", "acc", "val", torch.Tensor([45]), 42)
        self.metric_result_container.add_results([result1, result2])
        other = metrics.ModelMetricResultContainer([result3, result4])

        self.metric_result_container.merge(other)

        self.assertDictEqual(
            self.metric_result_container.get_raw_metrics(),
            {("acc", "test"): [result1, result2], ("acc", "val"): [result3, result4]},
        )

    def test_get_aggregated_metrics(self):
        result1 = metrics.ModelMetricResult("d0", "acc", "test", torch.Tensor([42]), 1)
        result2 = metrics.ModelMetricResult("d1", "acc", "test", torch.Tensor([43]), 3)
        result3 = metrics.ModelMetricResult("d0", "acc", "val", torch.Tensor([44]), 1)
        result4 = metrics.ModelMetricResult("d1", "acc", "val", torch.Tensor([45]), 3)
        self.metric_result_container.add_results([result1, result2, result3, result4])

        aggregated_results_container = (
            self.metric_result_container.get_aggregated_metrics()
        )

        self.assertDictEqual(
            aggregated_results_container.get_raw_metrics(),
            {
                ("acc", "test"): [
                    metrics.ModelMetricResult(
                        "aggregated", "acc", "test", torch.Tensor([42.75]), 4
                    )
                ],
                ("acc", "val"): [
                    metrics.ModelMetricResult(
                        "aggregated", "acc", "val", torch.Tensor([44.75]), 4
                    )
                ],
            },
        )

    def test_get_as_list(self):
        result1 = metrics.ModelMetricResult("d1", "acc", "test", torch.Tensor([42]), 42)
        result2 = metrics.ModelMetricResult("d1", "acc", "test", torch.Tensor([43]), 42)
        result3 = metrics.ModelMetricResult("d1", "acc", "val", torch.Tensor([44]), 42)
        result4 = metrics.ModelMetricResult("d1", "acc", "val", torch.Tensor([45]), 42)
        self.metric_result_container.add_results([result1, result2, result3, result4])

        result_list = self.metric_result_container.get_as_list()

        self.assertEqual(result_list, [result1, result2, result3, result4])


class DiagnosticMetricResultContainerTest(unittest.TestCase):

    def setUp(self):
        self.diagnostic_metric_result_container = (
            metrics.DiagnosticMetricResultContainer()
        )

    def test_add_result(self):
        self.assertEqual(len(self.diagnostic_metric_result_container.results), 0)
        result = metrics.DiagnosticMetricResult("d1", "acc", "test", 0.42)
        self.diagnostic_metric_result_container.add_result(result)
        self.assertDictEqual(
            self.diagnostic_metric_result_container.get_raw_metrics(),
            {("acc", "test"): [result]},
        )


class MetricsForOptimizationTest(unittest.TestCase):
    def test_get_metrics_for_optimization(self):
        results = [
            # comp_time
            # 2 + 4 samples & batch_size = 1
            DiagnosticMetricResult(
                device_id="d0", method="train_batch", name="comp_time", value=0.6
            ),
            DiagnosticMetricResult(
                device_id="d0", method="train_batch", name="comp_time", value=0.6
            ),
            DiagnosticMetricResult(
                device_id="d0", method="train_batch", name="comp_time", value=0.6
            ),
            DiagnosticMetricResult(
                device_id="d0", method="train_batch", name="comp_time", value=0.6
            ),
            DiagnosticMetricResult(
                device_id="d0", method="train_batch", name="comp_time", value=0.6
            ),
            DiagnosticMetricResult(
                device_id="d0", method="train_batch", name="comp_time", value=0.6
            ),
            DiagnosticMetricResult(
                device_id="d0", method="train_global", name="comp_time", value=10
            ),
            # train epoch time = train batch time + client model train time
            DiagnosticMetricResult(
                device_id="d0",
                method="client_train_epoch_time",
                name="comp_time",
                value=0.6,
            ),
            DiagnosticMetricResult(
                device_id="d1",
                method="client_train_epoch_time",
                name="comp_time",
                value=2.4,
            ),
            # one eval batch each
            DiagnosticMetricResult(
                device_id="d0", method="evaluate_batch", name="comp_time", value=0.1
            ),
            DiagnosticMetricResult(
                device_id="d0", method="evaluate_batch", name="comp_time", value=0.1
            ),
            DiagnosticMetricResult(
                device_id="d0",
                method="client_eval_epoch_time",
                name="comp_time",
                value=0.1,
            ),
            DiagnosticMetricResult(
                device_id="d1",
                method="client_eval_epoch_time",
                name="comp_time",
                value=0.2,
            ),
            # size
            DiagnosticMetricResult(
                device_id="d0", method="gradients", name="size", value=50
            ),
            DiagnosticMetricResult(
                device_id="d1", method="gradients", name="size", value=50
            ),
            DiagnosticMetricResult(
                device_id="d0", method="labels", name="size", value=6
            ),
            DiagnosticMetricResult(
                device_id="d1", method="labels", name="size", value=6
            ),
            DiagnosticMetricResult(
                device_id="d0", method="smashed_data", name="size", value=42
            ),
            DiagnosticMetricResult(
                device_id="d1", method="smashed_data", name="size", value=42
            ),
            DiagnosticMetricResult(
                device_id="d0", method="client_weights", name="size", value=500
            ),
            DiagnosticMetricResult(
                device_id="d1", method="server_weights", name="size", value=500
            ),
            DiagnosticMetricResult(
                device_id="d1", method="optimizer_state", name="size", value=500
            ),
        ]
        diagnostic_metric_result_container = DiagnosticMetricResultContainer(results)
        batch_size = 1

        num_samples_per_device = {"d0": (2, 1), "d1": (4, 1)}  # (train, eval)

        normalized_metrics = compute_metrics_for_optimization(
            diagnostic_metric_result_container, num_samples_per_device, batch_size
        )

        self.assertEqual(normalized_metrics["smashed_data_size"], 42)
        self.assertEqual(normalized_metrics["label_size"], 6)
        self.assertEqual(normalized_metrics["gradient_size"], 50)
        self.assertEqual(normalized_metrics["client_weight_size"], 500)
        self.assertEqual(normalized_metrics["server_weight_size"], 500)
        self.assertEqual(normalized_metrics["optimizer_state_size"], 500)
        self.assertEqual(normalized_metrics["train_global_time"], 10)
        self.assertAlmostEqual(normalized_metrics["client_norm_fw_time"], 0.1)
        self.assertAlmostEqual(normalized_metrics["client_norm_bw_time"], 0.2)
        self.assertAlmostEqual(normalized_metrics["server_norm_fw_time"], 0.1)
        self.assertAlmostEqual(normalized_metrics["server_norm_bw_time"], 0.5)
        self.assertDictEqual(
            normalized_metrics["comp_latency_factor"], {"d0": 1.0, "d1": 2.0}
        )

import unittest

import torch

from edml.helpers.model_splitting import split_network_at_layer
from edml.models.resnet_models import BasicBlock, ResNet


class ModelSplittingTest(unittest.TestCase):
    def test_split_network_at_layer_with_resnet(self):
        # test that the original model and the concatenation of client and server model produce the same output
        model = ResNet(BasicBlock, [3, 3, 3], num_classes=100)
        client, server = split_network_at_layer(model, 3)
        random_input = torch.rand(1, 3, 32, 32)
        model_output = model(random_input)
        client_output = client(random_input)
        server_output = server(client_output)
        self.assertTrue(torch.equal(model_output, server_output))

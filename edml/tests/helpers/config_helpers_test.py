import os
import unittest
from unittest.mock import patch

from omegaconf import DictConfig, OmegaConf

from edml.controllers.parallel_split_controller import ParallelSplitController
from edml.helpers.config_helpers import (
    get_device_id_by_index,
    get_device_address_by_id,
    preprocess_config,
    get_device_index_by_id,
    instantiate_controller,
    __drop_irrelevant_keys__,
    get_torch_device_id,
)


class ConfigHelpersTest(unittest.TestCase):

    def setUp(self) -> None:
        self.cfg = DictConfig(
            {
                "own_device_id": "d0",
                "topology": {
                    "devices": [
                        {"device_id": "d0", "address": "localhost:50051"},
                        {"device_id": "d1", "address": "localhost:50052"},
                    ]
                },
                "num_devices": "${len:${topology.devices}}",
                "controller": {
                    "name": "swarm",
                    "scheduler": {"name": "max_battery"},
                    "adaptive_threshold_fn": {
                        "name": "sta.tic"
                    },  # check that points are removed
                },
                "group_by": {
                    "controller": [
                        "name",
                        {"scheduler": "name", "adaptive_threshold_fn": "name"},
                    ],
                },
                "group": "${group_name:${group_by}}",
            }
        )

    def test_get_device_id_by_index(self):
        device_id = get_device_id_by_index(self.cfg, 0)
        self.assertEqual(self.cfg.own_device_id, device_id)

    def test_get_device_index_by_id(self):
        device_idx = get_device_index_by_id(self.cfg, "d1")
        self.assertEqual(1, device_idx)

    def test_get_device_address_by_id(self):
        device_address = get_device_address_by_id(device_id="d1", cfg=self.cfg)
        self.assertEqual("localhost:50052", device_address)

    def test_preprocess_config_resolving_num_devices(self):
        preprocess_config(self.cfg)
        self.assertEqual(2, self.cfg.num_devices)

    def test_preprocess_config_set_device_id_by_index(self):
        self.cfg.own_device_id = 1
        preprocess_config(self.cfg)
        self.assertEqual("d1", self.cfg.own_device_id)

    def test_get_default_torch_device_if_cuda_available(self):
        with patch("torch.cuda.is_available", return_value=True):
            self.assertEqual(get_torch_device_id(self.cfg), "cuda:0")

    def test_get_default_torch_device_if_cuda_not_available(self):
        with patch("torch.cuda.is_available", return_value=False):
            self.assertEqual(get_torch_device_id(self.cfg), "cpu")

    def test_preprocess_config_group_name(self):
        preprocess_config(self.cfg)
        self.assertEqual(self.cfg.group, "swarm_max_battery_static")


class ControllerInstantiationTest(unittest.TestCase):
    def setUp(self) -> None:
        self.cfg = OmegaConf.create({"some_key": "some_value"})
        self.cfg.controller = OmegaConf.load(
            os.path.join(
                os.path.dirname(__file__),
                "../../../edml/config/controller/parallel_swarm.yaml",
            )
        )
        self.cfg.controller.scheduler = OmegaConf.load(
            os.path.join(
                os.path.dirname(__file__),
                "../../../edml/config/controller/scheduler/max_battery.yaml",
            )
        )

    def test_parallel_split_controller_with_max_battery_instantiation(self):
        with patch(
            "edml.controllers.base_controller.BaseController.__init__"
        ):  # Avoid initializing the base_controller for brevity
            with patch(
                "edml.controllers.base_controller.BaseController._get_device_ids"
            ) as _get_device_ids:  # needed by scheduler
                _get_device_ids.return_value = ["d0"]
                controller = instantiate_controller(self.cfg)
                self.assertIsInstance(controller, ParallelSplitController)

    def test_drop_irrelevant_keys(self):
        self.cfg.controller["irrelevant_key"] = "some value"
        reduced_cfg = __drop_irrelevant_keys__(self.cfg.controller)
        self.assertListEqual(
            list(reduced_cfg.keys()), ["_target_", "_partial_", "scheduler"]
        )


class GetTorchDeviceIdTest(unittest.TestCase):

    def setUp(self) -> None:
        self.cfg = DictConfig(
            {
                "own_device_id": "d0",
                "topology": {
                    "devices": [
                        {
                            "device_id": "d0",
                            "address": "localhost:50051",
                            "torch_device": "my_torch_device1",
                        },
                        {
                            "device_id": "d1",
                            "address": "localhost:50052",
                            "torch_device": "my_torch_device2",
                        },
                    ]
                },
                "num_devices": "${len:${topology.devices}}",
            }
        )

    def test_get_torch_device1(self):
        self.assertEqual(get_torch_device_id(self.cfg), "my_torch_device1")

    def test_get_torch_device2(self):
        self.cfg.own_device_id = "d1"
        self.assertEqual(get_torch_device_id(self.cfg), "my_torch_device2")

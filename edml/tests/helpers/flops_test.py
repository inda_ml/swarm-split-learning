import os
import unittest

import torch
import torch.nn as nn
from omegaconf import OmegaConf

from edml.helpers.flops import estimate_model_flops
from edml.models.mnist_models import ClientNet, ServerNet
from edml.tests.models.model_loading_helpers import (
    _get_model_from_model_provider_config,
)


class FullTestModel(nn.Module):
    def __init__(self):
        super().__init__()
        self.fc1 = nn.Linear(in_features=1000, out_features=10)
        self.fc2 = nn.Linear(in_features=10, out_features=10)
        self.conv = nn.Conv2d(in_channels=3, out_channels=10, kernel_size=1)
        self.act = nn.ReLU()

    def forward(self, x):
        return self.fc2(self.act(self.fc1(self.act(self.conv(x)).flatten(1))))


class ClientTestModel(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv = nn.Conv2d(in_channels=3, out_channels=10, kernel_size=1)
        self.act = nn.ReLU()

    def forward(self, x):
        return self.act(self.conv(x)).flatten(1)


class ServerTestModel(nn.Module):
    def __init__(self):
        super().__init__()
        self.fc1 = nn.Linear(in_features=1000, out_features=10)
        self.fc2 = nn.Linear(in_features=10, out_features=10)
        self.act = nn.ReLU()

    def forward(self, x):
        return self.fc2(self.act(self.fc1(x)))


class FlopsTest(unittest.TestCase):

    def test_split_count(self):
        """Check that a split model yields the same flop count as if the model was not split."""
        client_model = ClientTestModel()
        server_model = ServerTestModel()
        full_model = FullTestModel()

        inputs = (torch.randn((10, 3, 10, 10)),)
        server_inputs = client_model(inputs[0])

        full_flops = estimate_model_flops(full_model, inputs)
        client_flops = estimate_model_flops(client_model, inputs)
        server_flops = estimate_model_flops(server_model, server_inputs)

        self.assertEqual(client_flops, {"BW": 60000, "FW": 30000})
        self.assertEqual(server_flops, {"BW": 202000, "FW": 101000})
        self.assertEqual(
            full_flops,
            {
                "BW": client_flops["BW"] + server_flops["BW"],
                "FW": client_flops["FW"] + server_flops["FW"],
            },
        )

    def test_mnist_split_count(self):
        client_model = ClientNet()
        server_model = ServerNet()

        inputs = torch.randn((1, 1, 28, 28))
        server_inputs = client_model(inputs)

        client_flops = estimate_model_flops(client_model, inputs)
        server_flops = estimate_model_flops(server_model, server_inputs)

        self.assertEqual(client_flops, {"BW": 10811520, "FW": 5405760})
        self.assertEqual(server_flops, {"BW": 22431600, "FW": 11215800})

    def test_autoencoder_does_not_affect_backward_flops(self):
        os.chdir(os.path.join(os.path.dirname(__file__), "../../../"))
        client, server = _get_model_from_model_provider_config(
            OmegaConf.create({}), "resnet20"
        )
        client_with_encoder, server_with_decoder = (
            _get_model_from_model_provider_config(
                OmegaConf.create({}), "resnet20-with-autoencoder"
            )
        )
        input = torch.randn(1, 3, 32, 32)

        ae_smashed_data = client_with_encoder(input)
        smashed_data = client(input)

        client_flops = estimate_model_flops(client, input)
        server_flops = estimate_model_flops(server, smashed_data)

        client_with_encoder_flops = estimate_model_flops(client_with_encoder, input)
        server_with_decoder_flops = estimate_model_flops(
            server_with_decoder, ae_smashed_data
        )

        self.assertEqual(client_flops["BW"], client_with_encoder_flops["BW"])
        self.assertEqual(server_flops["BW"], server_with_decoder_flops["BW"])

        self.assertGreater(client_with_encoder_flops["FW"], client_flops["FW"])
        self.assertGreater(server_with_decoder_flops["FW"], server_flops["FW"])

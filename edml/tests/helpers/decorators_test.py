import time
import unittest
from unittest.mock import Mock, patch

import torch

from edml.core.battery import Battery
from edml.core.client import DeviceClient
from edml.core.device import NetworkDevice
from edml.helpers.decorators import (
    check_device_set,
    log_execution_time,
    update_battery,
    battery_updater,
    LatencySimulator,
    simulate_latency_decorator,
    add_time_to_diagnostic_metrics,
)
from edml.helpers.logging import SimpleLogger
from edml.helpers.metrics import DiagnosticMetricResult, DiagnosticMetricResultContainer


class CheckDeviceDecoratorTest(unittest.TestCase):

    def test_client_with_device_set(self):
        # check that decorator raises no exception if client has device set
        client = Mock(spec=DeviceClient)
        client.node_device = Mock(spec=NetworkDevice)
        decorator = check_device_set()
        decorated_function = decorator(lambda x: x)
        exception_raised = False
        try:
            decorated_function(client)
        except ValueError:
            exception_raised = True
        self.assertFalse(exception_raised)

    def test_client_without_device_set(self):
        # check that decorator raises exception if client has no device set
        client = Mock(spec=DeviceClient)
        client.node_device = None
        decorator = check_device_set()
        decorated_function = decorator(lambda x: x)

        self.assertRaises(ValueError, decorated_function, client)


class LogExecutionTimeDecoratorTest(unittest.TestCase):

    @log_execution_time("logger", "sleep_time")
    def function_to_decorate(self, sleep_time):
        time.sleep(sleep_time)

    def test_log_execution_time(self):
        # check that decorator returns execution time
        # slight probability that this test fails since time.sleep is not exact
        self.logger = Mock(spec=SimpleLogger)

        self.function_to_decorate(1)

        self.logger.log.assert_called_once()
        execution_time = self.logger.log.call_args[0][0]["sleep_time"]
        self.assertEqual(["start", "end", "duration"], list(execution_time.keys()))
        self.assertAlmostEqual(execution_time["duration"], 1, delta=0.1)


class UpdateBatteryMethodDecoratorTest(unittest.TestCase):

    @update_battery
    def function_to_decorate(self, sleep_time):
        time.sleep(sleep_time)

    def test_update_battery(self):
        self.battery = Battery(1000, deduction_per_second=1)
        self.battery.start_experiment()

        self.function_to_decorate(1)

        self.assertAlmostEqual(self.battery.remaining_capacity(), 999, delta=0.1)

    def test_update_battery_without_time(self):
        self.battery = Battery(1000, deduction_per_second=0)
        self.battery.start_experiment()

        self.function_to_decorate(1)

        self.assertEqual(self.battery.remaining_capacity(), 1000)


class BatteryUpdaterClassDecoratorTest(unittest.TestCase):
    # Deprecated
    @battery_updater
    class ClassToDecorate(object):
        def __init__(self, battery):
            self.battery = battery

        def function_to_decorate(self, sleep_time):
            time.sleep(sleep_time)

    def test_update_battery(self):
        battery = Battery(1000, deduction_per_second=1)
        battery.start_experiment()
        decorated_class = self.ClassToDecorate(battery)
        decorated_class.function_to_decorate(1)

        self.assertAlmostEqual(battery.remaining_capacity(), 999, delta=0.1)

    def test_update_battery_without_time(self):
        battery = Battery(1000, deduction_per_second=0)
        battery.start_experiment()

        decorated_class = self.ClassToDecorate(battery)
        decorated_class.function_to_decorate(1)

        self.assertEqual(battery.remaining_capacity(), 1000)


class LatencySimulatorTest(unittest.TestCase):

    def test_no_latency(self):
        self._test_latency(0, 0, 0)
        self._test_latency(0.5, 0, 0.5)
        self._test_latency(1, 0, 1)

    def test_latency(self):
        self._test_latency(0, 1, 0)
        self._test_latency(0.5, 1, 1)
        self._test_latency(1, 2, 3)

    def _test_latency(self, computation_time, latency_factor, resulting_time):
        start_time = time.time()
        with LatencySimulator(latency_factor=latency_factor) as l:
            time.sleep(computation_time)
        end_time = time.time()
        self.assertAlmostEqual(end_time - start_time, resulting_time, delta=0.1)


class SimulateLatencyDecoratorTest(unittest.TestCase):

    def test_no_latency(self):
        self._test_latency(0, 0, 0)
        self._test_latency(0.5, 0, 0.5)
        self._test_latency(1, 0, 1)

    def test_latency(self):
        self._test_latency(0, 1, 0)
        self._test_latency(0.5, 1, 1)
        self._test_latency(1, 2, 3)

    @simulate_latency_decorator(latency_factor_attr="latency_factor")
    def method_to_slow(self, sleep_time):
        time.sleep(sleep_time)

    def _test_latency(self, computation_time, latency_factor, resulting_time):
        start_time = time.time()
        self.latency_factor = latency_factor

        self.method_to_slow(computation_time)

        end_time = time.time()
        self.assertAlmostEqual(end_time - start_time, resulting_time, delta=0.1)


class AddTimeToDiagnosticMetricsTest(unittest.TestCase):

    def setUp(self):
        self.device_id = "d0"  # assume class to be device

    @add_time_to_diagnostic_metrics("echo_method_to_decorate")
    def echo_method_to_decorate(self, input):
        return input

    def test_append_diagnostic_metrics_to_no_output(self):
        with patch("time.time", side_effect=[0, 42]):
            response = self.echo_method_to_decorate(None)
            self.assertEqual(
                response.get_as_list(),
                [
                    DiagnosticMetricResult(
                        device_id="d0",
                        name="comp_time",
                        value=42,
                        method="echo_method_to_decorate",
                    )
                ],
            )

    def test_append_diagnostic_metrics_to_single_output(self):
        with patch("time.time", side_effect=[0, 42]):
            weights = {"weights": torch.tensor(42)}
            response = self.echo_method_to_decorate(weights)
            self.assertEqual(
                response,
                (
                    weights,
                    DiagnosticMetricResultContainer(
                        [
                            DiagnosticMetricResult(
                                device_id="d0",
                                name="comp_time",
                                value=42,
                                method="echo_method_to_decorate",
                            )
                        ]
                    ),
                ),
            )

    def test_append_diagnostic_metrics_to_multiple_outputs(self):
        with patch("time.time", side_effect=[0, 42]):
            weights = {"weights": torch.tensor(42)}
            metrics = {"acc": torch.tensor(42)}
            response = self.echo_method_to_decorate((weights, metrics))
            self.assertEqual(
                response,
                (
                    weights,
                    metrics,
                    DiagnosticMetricResultContainer(
                        [
                            DiagnosticMetricResult(
                                device_id="d0",
                                name="comp_time",
                                value=42,
                                method="echo_method_to_decorate",
                            )
                        ]
                    ),
                ),
            )

    def test_append_to_existing_diagnostic_metrics(self):
        with patch("time.time", side_effect=[0, 42]):
            previous_diagnostic_metrics = DiagnosticMetricResultContainer(
                [
                    DiagnosticMetricResult(
                        device_id="d0", name="comp_time", value=3, method="TrainBatch"
                    )
                ]
            )
            response = self.echo_method_to_decorate(previous_diagnostic_metrics)
            self.assertEqual(
                response,
                DiagnosticMetricResultContainer(
                    [
                        DiagnosticMetricResult(
                            device_id="d0",
                            name="comp_time",
                            value=3,
                            method="TrainBatch",
                        ),
                        DiagnosticMetricResult(
                            device_id="d0",
                            name="comp_time",
                            value=42,
                            method="echo_method_to_decorate",
                        ),
                    ]
                ),
            )

    def test_multiple_outputs_append_to_existing_diagnostic_metrics(self):
        with patch("time.time", side_effect=[0, 42]):
            previous_diagnostic_metrics = DiagnosticMetricResultContainer(
                [
                    DiagnosticMetricResult(
                        device_id="d0", name="comp_time", value=3, method="TrainBatch"
                    )
                ]
            )
            response = self.echo_method_to_decorate((42, previous_diagnostic_metrics))
            self.assertEqual(
                response,
                (
                    42,
                    DiagnosticMetricResultContainer(
                        [
                            DiagnosticMetricResult(
                                device_id="d0",
                                name="comp_time",
                                value=3,
                                method="TrainBatch",
                            ),
                            DiagnosticMetricResult(
                                device_id="d0",
                                name="comp_time",
                                value=42,
                                method="echo_method_to_decorate",
                            ),
                        ]
                    ),
                ),
            )

import unittest
from unittest.mock import patch

import edml.helpers.logging as logging


class WandbLoggerTest(unittest.TestCase):

    def setUp(self) -> None:
        self.logger = logging.WandbLogger(None, 42)
        self.logger.wandb_enabled = (
            True  # skip logger.start_experiment to avoid unnecessary patching
        )

    @patch("wandb.finish", return_value=None)
    def test_end_experiment(self, wandb_finish_mock):
        self.logger.end_experiment()
        self.assertFalse(self.logger.wandb_enabled)
        wandb_finish_mock.assert_called_once()

    @patch("wandb.log")
    def test_log_string(self, wandb_log_mock):
        self.logger.log("test")
        wandb_log_mock.log.assert_not_called()

    @patch("wandb.log")
    def test_log_metrics(self, wandb_log_mock):
        self.logger.log({"test": 42})
        wandb_log_mock.assert_called_once_with({"test": 42})

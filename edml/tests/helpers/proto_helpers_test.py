import unittest

from torch import Tensor

import edml.generated.datastructures_pb2
import edml.helpers.proto_helpers as proto_helpers
from edml.generated.connection_pb2 import FullModelTrainResponse
from edml.helpers.metrics import (
    ModelMetricResultContainer,
    ModelMetricResult,
    DiagnosticMetricResultContainer,
    DiagnosticMetricResult,
)


class ProtoHelpersTest(unittest.TestCase):

    def setUp(self) -> None:
        self.tensor = Tensor([42])
        self.state_dict = {"weights": self.tensor}
        self.metrics = ModelMetricResultContainer()
        self.metrics.add_result(ModelMetricResult("d1", "acc", "test", self.tensor, 42))

    def test_tensor_to_proto(self):
        proto = proto_helpers.tensor_to_proto(self.tensor)
        self.assertEqual(proto_helpers.proto_to_tensor(proto), self.tensor)

    def test_state_dict_to_proto(self):
        proto = proto_helpers.state_dict_to_proto(self.state_dict)
        self.assertEqual(proto_helpers.proto_to_state_dict(proto), self.state_dict)

    def test_proto_to_tensor(self):
        proto = edml.generated.datastructures_pb2.Tensor(
            serialized=proto_helpers._tensor_to_bytes(self.tensor)
        )
        self.assertEqual(proto_helpers.proto_to_tensor(proto), self.tensor)

    def test_proto_to_state_dict(self):
        proto = edml.generated.datastructures_pb2.StateDict(
            serialized=proto_helpers._state_dict_to_bytes(self.state_dict)
        )
        self.assertEqual(proto_helpers.proto_to_state_dict(proto), self.state_dict)

    def test_weights_to_proto(self):
        proto = proto_helpers.weights_to_proto(self.state_dict)
        self.assertEqual(proto_helpers.proto_to_weights(proto), self.state_dict)

    def test_none_weights_to_proto(self):
        proto = proto_helpers.weights_to_proto(None)
        self.assertEqual(proto_helpers.proto_to_weights(proto), None)

    def test_proto_to_weights(self):
        proto = edml.generated.datastructures_pb2.Weights(
            weights=proto_helpers.state_dict_to_proto(self.state_dict)
        )
        self.assertEqual(proto_helpers.proto_to_weights(proto), self.state_dict)

    def test_activations_to_proto(self):
        proto = proto_helpers.activations_to_proto(self.tensor)
        self.assertEqual(proto_helpers.proto_to_activations(proto), self.tensor)

    def test_proto_to_activations(self):
        proto = edml.generated.datastructures_pb2.Activations(
            activations=proto_helpers.tensor_to_proto(self.tensor)
        )
        self.assertEqual(proto_helpers.proto_to_activations(proto), self.tensor)

    def test_labels_to_proto(self):
        proto = proto_helpers.labels_to_proto(self.tensor)
        self.assertEqual(proto_helpers.proto_to_labels(proto), self.tensor)

    def test_proto_to_labels(self):
        proto = edml.generated.datastructures_pb2.Labels(
            labels=proto_helpers.tensor_to_proto(self.tensor)
        )
        self.assertEqual(proto_helpers.proto_to_labels(proto), self.tensor)

    def test_proto_to_device_info(self):
        proto = edml.generated.datastructures_pb2.DeviceInfo(
            device_id="42", address="localhost"
        )
        self.assertEqual(proto_helpers.proto_to_device_info(proto), ("42", "localhost"))

    def test_device_info_to_proto(self):
        proto = proto_helpers.device_info_to_proto("42", "localhost")
        self.assertEqual(proto_helpers.proto_to_device_info(proto), ("42", "localhost"))

    def test_proto_to_gradients(self):
        proto = edml.generated.datastructures_pb2.Gradients(
            gradients=proto_helpers.tensor_to_proto(self.tensor)
        )
        self.assertEqual(proto_helpers.proto_to_gradients(proto), self.tensor)

    def test_gradients_to_proto(self):
        proto = proto_helpers.gradients_to_proto(self.tensor)
        self.assertEqual(proto_helpers.proto_to_gradients(proto), self.tensor)

    def test_proto_to_metrics(self):
        proto = edml.generated.datastructures_pb2.Metrics(
            metrics=proto_helpers._metrics_to_bytes(self.metrics)
        )
        self.assertEqual(proto_helpers.proto_to_metrics(proto), self.metrics)

    def test_metrics_to_proto(self):
        proto = proto_helpers.metrics_to_proto(self.metrics)
        self.assertEqual(proto_helpers.proto_to_metrics(proto), self.metrics)

    def test_proto_size_per_attribute(self):
        proto_object = FullModelTrainResponse(
            client_weights=proto_helpers.weights_to_proto(self.state_dict),
            server_weights=proto_helpers.weights_to_proto(self.state_dict),
            num_samples=42,  # to be ignored by the size computation
            metrics=proto_helpers.metrics_to_proto(self.metrics),
        )
        result = proto_helpers._proto_size_per_field(proto_object, "d0")
        self.assertEqual(
            result,
            DiagnosticMetricResultContainer(
                [
                    DiagnosticMetricResult(
                        device_id="d0", name="size", value=417, method="client_weights"
                    ),
                    DiagnosticMetricResult(
                        device_id="d0", name="size", value=417, method="server_weights"
                    ),
                    DiagnosticMetricResult(
                        device_id="d0", name="size", value=585, method="metrics"
                    ),
                ]
            ),
        )

    def test_proto_size_per_attribute_ignore_diagnostic_metrics(self):
        proto_object = FullModelTrainResponse(
            client_weights=proto_helpers.weights_to_proto(self.state_dict),
            server_weights=proto_helpers.weights_to_proto(self.state_dict),
            num_samples=42,  # to be ignored by the size computation
            metrics=proto_helpers.metrics_to_proto(self.metrics),
            diagnostic_metrics=proto_helpers.metrics_to_proto(
                DiagnosticMetricResultContainer(
                    [
                        DiagnosticMetricResult(
                            device_id="d0",
                            name="comp_time",
                            value=42,
                            method="TrainEpoch",
                        )
                    ]
                )
            ),
        )
        self.assertEqual(
            proto_helpers._proto_size_per_field(proto_object, "d0"),
            DiagnosticMetricResultContainer(
                [
                    DiagnosticMetricResult(
                        device_id="d0", name="size", value=417, method="client_weights"
                    ),
                    DiagnosticMetricResult(
                        device_id="d0", name="size", value=417, method="server_weights"
                    ),
                    DiagnosticMetricResult(
                        device_id="d0", name="size", value=585, method="metrics"
                    ),
                ]
            ),
        )

import unittest

import torch.utils.data
from torch import Tensor
from torch.utils.data import TensorDataset

from edml.helpers.data_partitioning import (
    __get_partitioned_data_for_device__,
    DataPartitioner,
)


class DataPartitioningTest(unittest.TestCase):

    def setUp(self) -> None:
        self.data = TensorDataset(
            Tensor([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
            Tensor([1, 0, 1, 0, 1, 0, 1, 0, 1, 0]),
        )

    def test_deterministic_partitioning(self):
        partition = __get_partitioned_data_for_device__(self.data, 0, 2, seed=42)
        self.assertTrue(
            torch.equal(partition[0:5][0], Tensor([3.0, 7.0, 2.0, 9.0, 5.0]))
        )
        self.assertTrue(
            torch.equal(partition[0:5][1], Tensor([1.0, 1.0, 0.0, 1.0, 1.0]))
        )

    def test_partition_distinct_data(self):
        partition0_indices = __get_partitioned_data_for_device__(
            self.data, 0, 2
        ).indices
        partition1_indices = __get_partitioned_data_for_device__(
            self.data, 1, 2
        ).indices
        for i in partition0_indices:
            self.assertFalse(i in partition1_indices)
        self.assertEqual(
            len(partition0_indices) + len(partition1_indices), len(self.data)
        )

    def test_partitioned_data_unequal_size(self):
        partition0 = __get_partitioned_data_for_device__(self.data, 0, 3)
        self.assertEqual(len(partition0), 4)

        partition1 = __get_partitioned_data_for_device__(self.data, 1, 3)
        self.assertEqual(len(partition1), 3)

        partition2 = __get_partitioned_data_for_device__(self.data, 2, 3)
        self.assertEqual(len(partition2), 3)

    def test_partition_with_lengths(self):
        partition0 = __get_partitioned_data_for_device__(
            self.data, 0, 3, fractions=[0.55, 0.27, 0.18]
        )
        partition1 = __get_partitioned_data_for_device__(
            self.data, 1, 3, fractions=[0.55, 0.27, 0.18]
        )
        partition2 = __get_partitioned_data_for_device__(
            self.data, 2, 3, fractions=[0.55, 0.27, 0.18]
        )
        self.assertEqual(len(partition0), 6)
        self.assertEqual(len(partition1), 3)
        self.assertEqual(len(partition2), 1)

    def test_subset_only_with_lengths(self):
        partition0 = __get_partitioned_data_for_device__(
            self.data, 0, 3, fractions=[0.1, 0.2, 0.3]
        )
        partition1 = __get_partitioned_data_for_device__(
            self.data, 1, 3, fractions=[0.1, 0.2, 0.3]
        )
        partition2 = __get_partitioned_data_for_device__(
            self.data, 2, 3, fractions=[0.1, 0.2, 0.3]
        )
        self.assertEqual(len(partition0), 1)
        self.assertEqual(len(partition1), 2)
        self.assertEqual(len(partition2), 3)

    def test_non_iid(self):
        data = TensorDataset(
            Tensor([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]).unsqueeze(1),
            Tensor([1, 1, 2, 2, 3, 3, 4, 4, 5, 5]).unsqueeze(1),
        )
        partition0 = __get_partitioned_data_for_device__(
            data, 0, 3, fractions=[0.4, 0.3, 0.3], distribution="non-iid"
        )
        partition1 = __get_partitioned_data_for_device__(
            data, 1, 3, fractions=[0.4, 0.3, 0.3], distribution="non-iid"
        )
        partition2 = __get_partitioned_data_for_device__(
            data, 2, 3, fractions=[0.4, 0.3, 0.3], distribution="non-iid"
        )
        self.assertEqual(len(partition0), 4)
        self.assertEqual(len(partition1), 3)
        self.assertEqual(partition2[0], (Tensor([8.0]), Tensor([4.0])))
        self.assertEqual(partition2[1], (Tensor([9.0]), Tensor([5.0])))
        self.assertEqual(partition2[2], (Tensor([10.0]), Tensor([5.0])))

    def test_floating_point_arithmetic(self):
        data = TensorDataset(
            Tensor(range(45000)).unsqueeze(1), Tensor(range(45000)).unsqueeze(1)
        )
        for num_devices in range(1, 100):
            partition = __get_partitioned_data_for_device__(
                data, num_devices - 1, num_devices
            )
            self.assertEqual(len(partition), int(45000 / num_devices))


class TestDataPartitioner(unittest.TestCase):

    def setUp(self) -> None:
        self.data = TensorDataset(
            Tensor([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
            Tensor([1, 0, 1, 0, 1, 0, 1, 0, 1, 0]),
        )

    def test_partition(self):
        partitioner = DataPartitioner(0, 2, seed=42)
        partition = partitioner.partition(self.data)
        self.assertTrue(
            torch.equal(partition[0:5][0], Tensor([3.0, 7.0, 2.0, 9.0, 5.0]))
        )
        self.assertTrue(
            torch.equal(partition[0:5][1], Tensor([1.0, 1.0, 0.0, 1.0, 1.0]))
        )

    def test_subset(self):
        partitioner = DataPartitioner(0, 2, seed=42, fractions=[0.2, 0.2])
        partition = partitioner.partition(self.data)
        self.assertTrue(torch.equal(partition[0:2][0], Tensor([3.0, 7.0])))
        self.assertTrue(torch.equal(partition[0:2][1], Tensor([1.0, 1.0])))

import os
from typing import Optional, Tuple

from torch.utils.data import random_split, DataLoader
from torchvision import transforms, datasets

from edml.helpers.data_partitioning import DataPartitioner


def _load_cifar100(train_transform, test_transform):
    train_data = datasets.CIFAR100(
        os.path.join(os.path.dirname(__file__), "../../../data"),
        train=True,
        download=True,
        transform=train_transform,
    )
    test_data = datasets.CIFAR100(
        os.path.join(os.path.dirname(__file__), "../../../data"),
        train=False,
        download=True,
        transform=test_transform,
    )
    return train_data, test_data


def _load_cifar10(train_transform, test_transform):
    train_data = datasets.CIFAR10(
        os.path.join(os.path.dirname(__file__), "../../../data"),
        train=True,
        download=True,
        transform=train_transform,
    )
    test_data = datasets.CIFAR10(
        os.path.join(os.path.dirname(__file__), "../../../data"),
        train=False,
        download=True,
        transform=test_transform,
    )
    return train_data, test_data


def _get_transforms():
    # transformations from https://github.com/akamaster/pytorch_resnet_cifar10
    # However, in this repository the test data is used for validation
    # Here, we use the test data for testing only and split the training data into train and validation data (90%/10%) as in the original resnet paper
    train_transform = transforms.Compose(
        [
            transforms.RandomHorizontalFlip(),
            transforms.RandomCrop(32, 4),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
        ]
    )
    test_transform = transforms.Compose(
        [
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
        ]
    )
    return train_transform, test_transform


def _cifar_dataloaders(batch_size, data_partitioner, split, dataset=100):
    """Returns the train, validation and test dataloaders for the CIFAR100 dataset"""
    train_transform, test_transform = _get_transforms()
    if dataset == 100:
        train_data, test_data = _load_cifar100(train_transform, test_transform)
    else:
        train_data, test_data = _load_cifar10(train_transform, test_transform)
    # partition data for device
    if data_partitioner is not None:
        train_data = data_partitioner.partition(train_data)
        test_data = data_partitioner.partition(test_data)
    train, val = random_split(train_data, split)
    return (
        DataLoader(train, batch_size=batch_size),
        DataLoader(val, batch_size=batch_size),
        DataLoader(test_data, batch_size=batch_size),
    )


def cifar100_dataloaders(
    batch_size: int,
    split: Tuple[float, float] = (0.9, 0.1),
    data_partitioner: Optional[DataPartitioner] = None,
):
    return _cifar_dataloaders(batch_size, data_partitioner, split, dataset=100)


def cifar10_dataloaders(
    batch_size: int,
    split: Tuple[float, float] = (0.9, 0.1),
    data_partitioner: Optional[DataPartitioner] = None,
):
    return _cifar_dataloaders(batch_size, data_partitioner, split, dataset=10)

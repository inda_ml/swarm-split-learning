import os
from typing import Tuple, Optional

from torchvision import transforms, datasets
from torch.utils.data import random_split, DataLoader, Subset

from edml.helpers.data_partitioning import DataPartitioner


def _load_transformed_data():
    transform = transforms.Compose(
        [  # preprocessing from https://github.com/pytorch/examples/blob/main/mnist/main.py
            transforms.ToTensor(),
            transforms.Normalize((0.1307,), (0.3081,)),
        ]
    )
    train_data = datasets.MNIST(
        os.path.join(os.path.dirname(__file__), "../../../data"),
        train=True,
        download=True,
        transform=transform,
    )
    test_data = datasets.MNIST(
        os.path.join(os.path.dirname(__file__), "../../../data"),
        train=False,
        download=True,
        transform=transform,
    )
    return train_data, test_data


def mnist_dataloaders(
    batch_size: int,
    split: Tuple[float, float] = (0.8, 0.2),
    data_partitioner: Optional[DataPartitioner] = None,
):
    """Returns the train, validation and test dataloaders for the MNIST dataset"""
    train_data, test_data = _load_transformed_data()
    # partition data for device
    if data_partitioner is not None:
        train_data = data_partitioner.partition(train_data)
        test_data = data_partitioner.partition(test_data)
    train, val = random_split(train_data, split)
    return (
        DataLoader(train, batch_size=batch_size),
        DataLoader(val, batch_size=batch_size),
        DataLoader(test_data, batch_size=batch_size),
    )


def single_batch_dataloaders(batch_size: int):
    """Returns the train, validation and test dataloaders of the MNIST dataset with only a single batch each for testing and debugging."""
    train_data, _ = _load_transformed_data()
    return (
        DataLoader(Subset(train_data, range(batch_size)), batch_size=batch_size),
        DataLoader(
            Subset(train_data, range(batch_size, 2 * batch_size)), batch_size=batch_size
        ),
        DataLoader(
            Subset(train_data, range(2 * batch_size, 3 * batch_size)),
            batch_size=batch_size,
        ),
    )

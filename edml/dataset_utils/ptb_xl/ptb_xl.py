import os
import pickle
from typing import Optional

from torch.utils.data import Dataset, DataLoader

from edml.dataset_utils.ptb_xl import utils
from edml.helpers.data_partitioning import DataPartitioner
from edml.helpers.types import DatasetDataLoaders

"""
Taken from https://github.com/a-ayad/Split_ECG_Classification/
"""


def load_dataset():
    sampling_frequency = 100
    cwd = os.path.dirname(os.path.abspath(__file__))
    mlb_path = os.path.join(cwd, "mlb.pkl")
    dataset_path = (
        os.path.normpath(
            os.path.join(
                cwd,
                "../../../data/ptb-xl-a-large-publicly-available-electrocardiography-dataset-1.0.1/",
            )
        )
        + "/"
    )

    task = "superdiagnostic"

    # Load PTB-XL data
    data, raw_labels = utils.load_dataset(dataset_path, sampling_frequency)
    # Preprocess label data
    labels = utils.compute_label_aggregations(raw_labels, dataset_path, task)
    # Select relevant data and convert to one-hot
    data, labels, Y, _ = utils.select_data(
        data, labels, task, min_samples=0, outputfolder=mlb_path
    )

    # 1-8 for training
    X_train = data[labels.strat_fold < 9]
    y_train = Y[labels.strat_fold < 9].astype("float64")
    # 9 for validation
    X_val = data[labels.strat_fold == 9]
    y_val = Y[labels.strat_fold == 9].astype("float64")
    # 10 for testing
    X_test = data[labels.strat_fold == 10]
    y_test = Y[labels.strat_fold == 10].astype("float64")

    standard_scaler = pickle.load(open(cwd + "/standard_scaler.pkl", "rb"))

    X_train = utils.apply_standardizer(X_train, standard_scaler)
    X_val = utils.apply_standardizer(X_val, standard_scaler)
    X_test = utils.apply_standardizer(X_test, standard_scaler)

    return X_train, y_train, X_val, y_val, X_test, y_test


class PTB_XL(Dataset):
    """
    Class to load sample-sets (train, val, test)
    """

    def __init__(self, data, labels, stage=None):
        self.stage = stage
        self.data = data
        self.labels = labels

    def __len__(self):
        return len(self.labels)

    def __getitem__(self, idx):
        return self.data[idx].transpose((1, 0)), self.labels[idx]


def ptb_xl_train_val_test(
    batch_size: int, data_partitioner: Optional[DataPartitioner] = None
) -> DatasetDataLoaders:
    """Returns train, val, test dataloaders for PTB-XL dataset"""
    X_train, y_train, X_val, y_val, X_test, y_test = load_dataset()
    train_data = PTB_XL(X_train, y_train, "train")
    val_data = PTB_XL(X_val, y_val, "train")
    test_data = PTB_XL(X_test, y_test, "test")

    if data_partitioner is not None:
        if data_partitioner.distribution == "non-iid":
            train_data = data_partitioner.partition(train_data)
            # stage is lost during non-iid partitioning, set it again
            train_data.stage = "train"
        else:
            train_data = data_partitioner.partition(train_data)
        # set distribution to iid for validation and test data
        data_partitioner.distribution = "iid"
        val_data = data_partitioner.partition(val_data)
        test_data = data_partitioner.partition(test_data)

    return (
        DataLoader(train_data, batch_size=batch_size),
        DataLoader(val_data, batch_size=batch_size),
        DataLoader(test_data, batch_size=batch_size),
    )

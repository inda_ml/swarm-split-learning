from typing import Any, Tuple, Optional

from hydra.utils import instantiate
from omegaconf import DictConfig
from torch.optim import Optimizer


def get_optimizer_and_scheduler(
    cfg: DictConfig, model_params: Any
) -> Tuple[Optimizer, Optional[Any]]:
    """
    Returns the optimizer for the given configuration. Optionally, a learning rate scheduler is returned as well.

    Args:
        cfg (DictConfig): The experiment's configuration.
        model_params (Any): Model parameters passed to the optimizer.

    Returns:
        An optimizer instance with the given parameters.

    Notes:
        Assumes the all optimizer-related config parameters to be present.
        If scheduler should be used, milestones and gamma must be present in the config.
    """

    # Instantiate the optimizer with the given parameters.
    optimizer = instantiate(cfg.optimizer, model_params)

    # Optionally, instantiate the scheduler with the given parameters.
    scheduler = None
    if "scheduler" in cfg:
        scheduler = instantiate(cfg.scheduler, optimizer)

    return optimizer, scheduler

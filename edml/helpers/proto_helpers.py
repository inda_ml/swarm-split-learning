import io
import pickle
from typing import Union, Any, Tuple

import torch

from edml.generated import datastructures_pb2
from edml.helpers.metrics import (
    ModelMetricResultContainer,
    DiagnosticMetricResultContainer,
    DiagnosticMetricResult,
)
from edml.helpers.types import StateDict


class CpuUnpickler(pickle.Unpickler):
    # quickfix from https://github.com/pytorch/pytorch/issues/16797
    def find_class(self, module, name):
        if module == "torch.storage" and name == "_load_from_bytes":
            return lambda b: torch.load(io.BytesIO(b), map_location="cpu")
        else:
            return super().find_class(module, name)


def _tensor_to_bytes(tensor: torch.Tensor) -> bytes:
    return pickle.dumps(tensor)


def _bytes_to_tensor(raw_bytes: bytes) -> torch.Tensor:
    bs = io.BytesIO(raw_bytes)
    return CpuUnpickler(bs).load()


def _state_dict_to_bytes(state_dict: StateDict) -> bytes:
    return pickle.dumps(state_dict)


def _bytes_to_state_dict(raw_bytes: bytes) -> StateDict:
    bs = io.BytesIO(raw_bytes)
    return CpuUnpickler(bs).load()


def _metrics_to_bytes(
    metrics: Union[ModelMetricResultContainer, DiagnosticMetricResultContainer]
) -> bytes:
    return pickle.dumps(metrics)


def _bytes_to_metrics(raw_bytes: bytes):
    bs = io.BytesIO(raw_bytes)
    return CpuUnpickler(bs).load()


def tensor_to_proto(tensor: torch.Tensor) -> datastructures_pb2.Tensor:
    return datastructures_pb2.Tensor(serialized=_tensor_to_bytes(tensor))


def proto_to_tensor(proto: datastructures_pb2.Tensor) -> torch.Tensor:
    return _bytes_to_tensor(proto.serialized)


def state_dict_to_proto(state_dict: StateDict) -> datastructures_pb2.StateDict:
    return datastructures_pb2.StateDict(serialized=_state_dict_to_bytes(state_dict))


def proto_to_state_dict(proto: datastructures_pb2.StateDict) -> StateDict:
    return _bytes_to_state_dict(proto.serialized)


def weights_to_proto(weights: dict) -> datastructures_pb2.Weights:
    return datastructures_pb2.Weights(weights=state_dict_to_proto(weights))


def proto_to_weights(proto: datastructures_pb2.Weights):
    return proto_to_state_dict(proto.weights)


def activations_to_proto(activations: torch.Tensor) -> datastructures_pb2.Activations:
    return datastructures_pb2.Activations(activations=tensor_to_proto(activations))


def proto_to_activations(proto: datastructures_pb2.Activations) -> torch.Tensor:
    return proto_to_tensor(proto.activations)


def labels_to_proto(labels: torch.Tensor) -> datastructures_pb2.Labels:
    return datastructures_pb2.Labels(labels=tensor_to_proto(labels))


def proto_to_labels(proto: datastructures_pb2.Labels) -> torch.Tensor:
    return proto_to_tensor(proto.labels)


def proto_to_device_info(proto: datastructures_pb2.DeviceInfo) -> Tuple[str, str]:
    return proto.device_id, proto.address


def device_info_to_proto(device_id: str, address: str) -> datastructures_pb2.DeviceInfo:
    return datastructures_pb2.DeviceInfo(device_id=device_id, address=address)


def proto_to_gradients(proto: datastructures_pb2.Gradients) -> torch.Tensor:
    return proto_to_tensor(proto.gradients)


def gradients_to_proto(gradients: torch.Tensor) -> datastructures_pb2.Gradients:
    return datastructures_pb2.Gradients(gradients=tensor_to_proto(gradients))


def proto_to_metrics(proto: datastructures_pb2.Metrics):
    return _bytes_to_metrics(proto.metrics)


def metrics_to_proto(
    metrics: Union[ModelMetricResultContainer, DiagnosticMetricResultContainer]
) -> datastructures_pb2.Metrics:
    return datastructures_pb2.Metrics(metrics=_metrics_to_bytes(metrics))


def _proto_size_per_field(
    proto_object: Any, device_id: str
) -> DiagnosticMetricResultContainer:
    metrics = DiagnosticMetricResultContainer()
    for attribute in proto_object.DESCRIPTOR.fields:
        if attribute.name != "diagnostic_metrics":
            try:
                byte_size = getattr(proto_object, attribute.name).ByteSize()
                metrics.add_result(
                    DiagnosticMetricResult(
                        device_id=device_id,
                        name="size",
                        method=attribute.name,
                        value=byte_size,
                    )
                )
            except AttributeError:
                # ignore primitive types without ByteSize() method
                pass
    return metrics

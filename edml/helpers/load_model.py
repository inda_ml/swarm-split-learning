from os.path import exists
from typing import Tuple

import torch
from omegaconf import DictConfig
from torch import nn

from edml.models import mnist_models, tcn_models, resnet_models


def _load_weights(model: nn.Module, path: str):
    """Loads the weights for the given model if the file at `path` exists"""
    if exists(path):
        model.load_state_dict(torch.load(path))


def get_models(cfg: DictConfig) -> Tuple[nn.Module, nn.Module]:
    """
    Returns the client and server models for the configured name. If `load_weights` has been set to `True`, the weights
    are loaded from the provided file paths.

    Args:
        cfg (DictConfig): The experiment's configuration.

    Returns:
         A tuple with the client model at position 0 and the server model at position 1.

    Raises:
        ValueError: If the configured model name is unknown.
    """
    name = cfg.model.name
    load_weights = cfg.experiment.load_weights
    client_model_load_path = cfg.experiment.client_model_load_path
    server_model_load_path = cfg.experiment.server_model_load_path

    if name == "simple_conv":
        client_model = mnist_models.ClientNet()
        server_model = mnist_models.ServerNet()
    elif name == "tcn":
        client_model = tcn_models.Small_TCN_5_Client(
            classes=cfg.dataset.num_classes, n_inputs=cfg.dataset.n_inputs
        )
        server_model = tcn_models.Small_TCN_5_Server(
            classes=cfg.dataset.num_classes, n_inputs=cfg.dataset.n_inputs
        )
    elif name == "resnet20":
        client_model, server_model = resnet_models.resnet20(
            cfg.model.cut_layer, num_classes=cfg.dataset.num_classes
        )
    elif name == "resnet32":
        client_model, server_model = resnet_models.resnet32(
            cfg.model.cut_layer, num_classes=cfg.dataset.num_classes
        )
    elif name == "resnet44":
        client_model, server_model = resnet_models.resnet44(
            cfg.model.cut_layer, num_classes=cfg.dataset.num_classes
        )
    elif name == "resnet56":
        client_model, server_model = resnet_models.resnet56(
            cfg.model.cut_layer, num_classes=cfg.dataset.num_classes
        )
    elif name == "resnet110":
        client_model, server_model = resnet_models.resnet110(
            cfg.model.cut_layer, num_classes=cfg.dataset.num_classes
        )
    elif name == "resnet1202":
        client_model, server_model = resnet_models.resnet1202(
            cfg.model.cut_layer, num_classes=cfg.dataset.num_classes
        )
    else:
        raise ValueError(f"Unknown model name {name}")

    if load_weights:
        if exists(client_model_load_path):
            _load_weights(client_model, client_model_load_path)
        if exists(server_model_load_path):
            _load_weights(server_model, server_model_load_path)

    return client_model, server_model

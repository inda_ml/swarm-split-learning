import time
import types
from functools import wraps

from edml.helpers.metrics import DiagnosticMetricResult, DiagnosticMetricResultContainer


def check_device_set():
    """
    The decorator checks if the `node_device` attribute has already been initialized.

    The decorator is used on functions inside the py:class:`edml.core.device.Device` class.

    Raises:
        ValueError: If `node_device` is `None`.
    """

    def decorator(func):
        @wraps(func)
        def wrapper(self, *args, **kwargs):
            if self.node_device is None:
                raise ValueError("Device not set")
            return func(self, *args, **kwargs)

        return wrapper

    return decorator


def log_execution_time(logger: str, log_key: str = "execution_time"):
    """
    A decorator factory for methods that returns a decorator that measures the wrapped function's execution time.

    The execution time is saved in a dictionary with key `log_key` and a dictionary as value. The value dictionary holds
    the start time, end time and duration of the function execution.

    Args:
        logger (str): The name of the attribute that provides the `log` method used to log the execution time.
        log_key (str): The key under which the execution time data is stored. Defaults to `execution_time`.
    """

    def decorator(func):
        @wraps(func)
        def wrapper(self, *args, **kwargs):
            start = time.time()
            result = func(self, *args, **kwargs)
            end = time.time()
            if getattr(self, logger) is not None:
                getattr(self, logger).log(
                    {
                        f"{log_key}": {
                            "start": start,
                            "end": end,
                            "duration": end - start,
                        }
                    }
                )
            else:
                print(f"{log_key}: {end - start}")
            return result

        return wrapper

    return decorator


def battery_updater(cls):
    """
    A decorator for classes that should update the battery of the device. Attaches the
    py:meth:`edml.helpers.decorators.update_battery` decorator to all methods of the class.

    Args:
        cls (class): Class to attach the py:meth:`edml.helpers.decorators.update_battery` decorators to.
    """
    for key in dir(cls):
        value = getattr(cls, key)
        if callable(value) and isinstance(value, types.FunctionType):
            setattr(cls, key, update_battery(value))
    return cls


def update_battery(func, battery_attr: str = "battery"):
    """
    Decorator for methods that should update the battery of the device.

    Args:
        func (function): The function to decorate.
        battery_attr (str): The name of the battery attribute. Defaults to `battery`.

    Notes:
        Requires an attribute of type py:class:`edml.core.battery.Battery` to be set on the decorated object.
    """

    @wraps(func)
    def wrapper(self, *args, **kwargs):
        if hasattr(self, battery_attr):
            battery = getattr(self, battery_attr)
            battery.update_time()
        result = func(self, *args, **kwargs)
        if hasattr(self, battery_attr):
            battery = getattr(self, battery_attr)
            battery.update_time()
        return result

    return wrapper


def simulate_latency_decorator(latency_factor_attr):
    """
    Simulates latency by sleeping for the given number of seconds.

    Args:
        latency_factor_attr (str): the class attribute to determine the computational latency

    Returns:
        A decorator that sleeps for the time of its wrapped method execution times the latency_factor.

    Raises:
        None

    Notes:
        Hence the resulting execution time is [MethodExecutionTime] * (1 + latency_factor).
        Use only for methods that do not call other functions using this decorator. Otherwise, the latency is increases by more than the given factor.
    """

    def decorator(func):
        @wraps(func)
        def wrapper(self, *args, **kwargs):
            latency = getattr(self, latency_factor_attr)
            with LatencySimulator(latency_factor=latency):
                res = func(self, *args, **kwargs)
            return res

        return wrapper

    return decorator


class LatencySimulator:
    def __init__(self, latency_factor: float = 0.0):
        """
        Simulates latency by sleeping for the given number of seconds.

        Args:
            latency_factor (float): the class attribute to determine the computational latency

        Returns:
            A decorator that sleeps for the time of its wrapped method execution times the latency_factor.

        Notes:
            Hence the resulting execution time is [MethodExecutionTime] * (1 + latency_factor)
            Use only for methods that do not call other functions using this decorator. Otherwise, the latency is increases by more than the given factor.
        """
        self.latency_factor = latency_factor

    def __enter__(self):
        self.start_time = time.time()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.latency_factor is None or self.latency_factor <= 0:
            return
        self.end_time = time.time()
        self.execution_time = self.end_time - self.start_time
        time.sleep(self.execution_time * self.latency_factor)


class Timer:
    """
    Context Manager to measure execution time.

    Notes:
        Access execution time via Timer.execution_time.
    """

    def __enter__(self):
        self.start_time = time.perf_counter()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.execution_time = time.perf_counter() - self.start_time


def add_time_to_diagnostic_metrics(method_name: str):
    """
    A decorator factory that measures the execution time of the wrapped method. It then creates a diagnostic metric
    result container to store the execution time.

    The decorator is smart enough to discover if functions already return an instance of type
    `DiagnosticMetricResultContainer`. If that is the case, the current metric result originating from `func` and the
    existing ones are merged together.

    More specifically, the decorator does the following:

        - If `func` has a return value of `None`, then the wrapped function returns an instance of
            `DiagnosticMetricResultContainer`.
        - if `func` returns an instance of `DiagnosticMetricResultContainer`, then the results are merged together and
            the wrapped function returns an instance of `DiagnosticMetricResultContainer`.
        - If `func` returns a tuple `t`, we analyze its value types:
            - If its last value is an instance of `DiagnosticMetricResultContainer`, we merge them together and return
                the same tuple, but its last value is changed to the new `DiagnosticMetricResultContainer` instance.
            - Else we return a new tuple of length `len(t) + 1`, where its last value will be the current
                `DiagnosticMetricResultContainer` instance.
        - Else the return value of `func` is a single value. We create a tuple that holds the original return value at
            position 0 and the new `DiagnosticMetricResultContainer` at position 1.

    Args:
        method_name (str): The name of the method to add the computation time to. Will be included in the metric.

    Returns:
        A decorator that adds the computation time to the diagnostic metrics of the wrapped method.

    Notes:
        The returned decorator can only be used for class functions. It expects that `func` is a class method with
        `self` parameter. It also expects that an attribute named `device_id` exists. This is required because the
        diagnostic metrics are bound to specific devices.
    """

    def decorator(func):
        @wraps(func)
        def wrapper(self, *args, **kwargs):
            start_time = time.time()
            res = func(self, *args, **kwargs)
            end_time = time.time()

            diagnostic_metric_result = DiagnosticMetricResult(
                device_id=self.device_id,
                name="comp_time",
                value=end_time - start_time,
                method=method_name,
            )

            # The wrapped function does not have a return value. In that case we return the metrics.
            if res is None:
                return DiagnosticMetricResultContainer([diagnostic_metric_result])

            # The wrapped function returns a metrics container. In that case we merge the current metrics instance with
            # the existing container and return the result.
            if isinstance(res, DiagnosticMetricResultContainer):
                res.add_result(diagnostic_metric_result)
                return res

            # The wrapped function returns a tuple. In that case we check if the last tuple value is a metrics
            # container and add the current metrics instance to it if that is the case. If not, we simply create a new
            # container and append it to the tuple.
            if isinstance(res, tuple):
                potential_metrics_container = res[-1]
                if isinstance(
                    potential_metrics_container, DiagnosticMetricResultContainer
                ):
                    potential_metrics_container.add_result(diagnostic_metric_result)
                else:
                    res = (
                        *res,
                        DiagnosticMetricResultContainer([diagnostic_metric_result]),
                    )
                return res

            # The wrapped function returns some kind of value. But none of the special cases above. We create a new
            # tuple that holds the wrapped function's return value at position 0 and the metrics container at position
            # 1.

            return res, DiagnosticMetricResultContainer([diagnostic_metric_result])

        return wrapper

    return decorator

import threading
from typing import Callable, Any

import grpc
from grpc_interceptor import ServerInterceptor, ClientInterceptor

from edml.core.battery import BatteryEmptyException, Battery
from edml.helpers.logging import SimpleLogger


class DeviceServerInterceptor(ServerInterceptor):

    def __init__(
        self,
        logger: SimpleLogger,
        battery: Battery = None,
        stop_event: threading.Event = None,
    ):
        """
        Intercepts all calls made to its gRPC server. Handles the battery and logs the request and response sizes.

        Args:
            logger (SimpleLogger): The logger to use.
            battery (Battery): The battery to monitor.
            stop_event (threading.Event): The stop event to set if the battery is empty to stop the gRPC server.
        Returns:
            None
        Raises:
            grpc.StatusCode.RESOURCE_EXHAUSTED when the battery is empty.
        Notes:
        """
        self.logger = logger
        self.battery = battery
        self.stop_event = stop_event

    def intercept(
        self,
        method: Callable,
        request_or_iterator: Any,
        context: grpc.ServicerContext,
        method_name: str,
    ) -> Any:
        request_size = _proto_serialized_size(request_or_iterator)
        self.logger.log({f"{method_name}_request_size": request_size})
        # check battery before handling request
        if self.battery is not None:
            try:
                self.battery.update_communication_received(request_size)
            except BatteryEmptyException as e:
                self.logger.log("Battery empty while receiving request")
                self.stop_event.set()
                context.abort(grpc.StatusCode.RESOURCE_EXHAUSTED, "Battery empty")
        # handle request
        try:
            response = method(request_or_iterator, context)
            response_size = _proto_serialized_size(response)
            self.logger.log({f"{method_name}_response_size": response_size})
            if self.battery is not None:
                self.battery.update_communication_sent(response_size)
            return response
        except BatteryEmptyException as e:
            self.logger.log("Battery empty while handling request or sending response")
            self.stop_event.set()
            context.abort(grpc.StatusCode.RESOURCE_EXHAUSTED, "Battery empty")


class DeviceClientInterceptor(ClientInterceptor):

    def __init__(self, logger, battery=None, stop_event=None):
        self.logger = logger
        self.battery = battery
        self.stop_event = stop_event

    def intercept(
        self,
        method: Callable,
        request_or_iterator: Any,
        call_details: grpc.ClientCallDetails,
    ):
        request_size = _proto_serialized_size(request_or_iterator)
        if self.battery is not None:
            try:
                self.battery.update_communication_sent(request_size)
                future = method(request_or_iterator, call_details)
                result = future.result()
                response_size = _proto_serialized_size(result)
                self.battery.update_communication_received(response_size)
                return future
            except BatteryEmptyException as e:
                # client exception, i.e. local device
                # catch to stop the server by setting the stop event
                self.logger.log("RPC client battery empty during request")
                self.stop_event.set()
                # now reraise exception to abort the RPC
                # any action of the device is triggered by some RPC, thus this exception will be handled by the next server interceptor in the cascade
                # as this device does not have any battery left for any other action, let it fail as fast as possible
                # this exception is not meant to be handled by the request dispatcher that started the RPC
                raise e
            except grpc.RpcError as e:
                # server exception, i.e. remote device
                # exceptions do not have an associated byte size and are hence not counted towards the battery
                self.logger.log("RPC server battery empty during request")
                raise e  # forward exception to dispatcher to handle


def _proto_serialized_size(proto_object):
    """Returns the size of the length-prefixed message in bytes.
    Includes the grpc header in the calculation since messages without payload are allowed (content size 0)
    """
    byte_size = proto_object.ByteSize()
    length_prefix = 4  # length prefix is 4 bytes to determine the message payload size
    compressed_flag = 1
    return byte_size + length_prefix + compressed_flag

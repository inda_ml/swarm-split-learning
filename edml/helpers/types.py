from dataclasses import dataclass
from typing import Mapping, Any, Tuple, Protocol, Union, Optional, Sequence, Callable

from torch import Tensor
from torch.utils.data import DataLoader

from edml.controllers.scheduler.base import NextServerScheduler
from edml.generated import datastructures_pb2

# TODO: use `type` keyword once Python>3.9 approved.

# Return type of the `get_dataloaders` function.
DatasetDataLoaders = Tuple[DataLoader, DataLoader, DataLoader]

# pytorch type for working with state dictionaries.
StateDict = Mapping[str, Any]

LossFn = Callable[[Tensor, Tensor], Tensor]


@dataclass
class HasMetrics(Protocol):
    """
    A subtype that has access to a pickled diagnostic metrics structure.

    Attributes:
        diagnostic_metrics (datastructures_pb2.DiagnosticMetrics): The pickled metrics.
    """

    diagnostic_metrics: datastructures_pb2.Metrics


class KeyedNextServerScheduler(Protocol):
    KEY: str

    # From the `NextServerScheduler` interface.
    def next_server(self, active_devices: Sequence[str]) -> Optional[str]: ...


@dataclass
class DeviceBatteryStatus:
    current_capacity: float
    initial_capacity: float

    @staticmethod
    def from_tuple(t: tuple[float, float]) -> "DeviceBatteryStatus":
        return DeviceBatteryStatus(initial_capacity=t[0], current_capacity=t[1])


DeviceBatteryStatusReport = Union[DeviceBatteryStatus, bool]


@dataclass
class SLTrainBatchResult:
    smashed_data: Any
    labels: Any

import concurrent.futures


def create_executor_with_threads(threads: int, min_threads: int = 1):
    return concurrent.futures.ThreadPoolExecutor(max_workers=max(threads, min_threads))

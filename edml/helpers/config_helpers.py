from copy import deepcopy
from inspect import signature
from typing import TYPE_CHECKING

import torch
from hydra.utils import get_class, instantiate
from omegaconf import OmegaConf, DictConfig, ListConfig
from omegaconf.errors import ConfigAttributeError

if TYPE_CHECKING:
    from edml.controllers.base_controller import BaseController


def get_device_address_by_id(device_id: str, cfg: DictConfig) -> str:
    """
    Returns the binding address of the device with the given id.

    Args:
        device_id (str): The device id.
        cfg (DictConfig): The config loaded from YAML files.

    Returns:
        The device's binding address.

    Raises:
        StopIteration: If the device with the given ID cannot be found.
    """
    return next(
        device.address
        for device in cfg.topology.devices
        if device.device_id == device_id
    )


def get_device_id_by_index(cfg: DictConfig, index: int) -> str:
    """
    Returns the device info of the device with the given index in the network topology.

    Args:
        cfg (DictConfig): The config loaded from YAML files.
        index (int): The index of the device inside the configuration file.

    Returns:
        The device's ID.

    Raises:
        StopIteration: If the device with the given ID cannot be found.
    """
    return cfg.topology.devices[index].device_id


def get_device_index_by_id(cfg: DictConfig, device_id: str) -> int:
    """
    Returns the index of the device with the given id in the network topology.

    Args:
        cfg (DictConfig): The config loaded from YAML files.
        device_id (str): The device's ID.

    Returns:
        The index of the device inside the configuration file.

    Raises:
        StopIteration: If the device with the given ID cannot be found.
    """
    return next(
        i
        for i, device in enumerate(cfg.topology.devices)
        if device.device_id == device_id
    )


def _group_resolver(cfg: DictConfig, group_by: DictConfig):
    """
    Resolver for the group_by attribute in the config. This attribute specifies which values to include in the group name.
    Therefore, the values of group_by are parsed to retrieve the key paths to the desired values.
    E.g. grouping by controller and scheduler name:
      group_by:
        - controller: [ name, scheduler: name]
    yields the paths: ["controller", "name"] and ["controller", "scheduler", "name"] which are read from the cfg then.

    Args:
        cfg (DictConfig): The full dict config.
        group_by (DictConfig): The part of the config specifying which attributes to use for experiment grouping.

    Returns:
        The name of the config group with underscores in between each value.
    """

    def __recurse__(group_by, attr_path: list):
        """Retrieves the key paths to the desired attributes."""
        attr_paths = []
        if isinstance(group_by, DictConfig):
            for k, v in group_by.items():
                if isinstance(v, str):
                    attr_paths.append(attr_path + [k] + [v])
                else:
                    attr_paths.extend(__recurse__(group_by[k], attr_path + [k]))
        elif isinstance(group_by, ListConfig):
            for idx, item in enumerate(group_by):
                if isinstance(item, str):
                    attr_paths.append(attr_path + [item])
                else:
                    attr_paths.extend(__recurse__(group_by[idx], attr_path))

        return attr_paths

    attr_paths = __recurse__(group_by, [])
    # resolve each attribute
    values = []
    for path in attr_paths:
        value = cfg
        for key in path:
            if isinstance(
                value, str
            ):  # if previous key was not found, value is the empty string
                break
            value = value.get(key, "")
        values.append(value)
    # concatenate and return
    return "_".join(values).replace(".", "")


def preprocess_config(cfg: DictConfig):
    """
    Configures `OmegaConf` and registers custom resolvers. Additionally, normalizes the configuration file for command
    line usage:

        - If `own_device_id` is an integer, the value is treated as an index into the list of available devices; it is
          treated as the i-th device inside the configured topology. This functions then looks up the device_id by index
          and sets `own_device_id`.
        - resolves the group_name attribute specifying the composition of the experiment group name.
    """
    OmegaConf.register_new_resolver("len", lambda x: len(x), replace=True)
    OmegaConf.register_new_resolver(
        "group_name", lambda group_by: _group_resolver(cfg, group_by), replace=True
    )
    OmegaConf.resolve(cfg)

    # In case someone specified an integer instead of a proper device_id (str), we look up the proper device by indexing
    # the list of all available devices using said integer.
    if isinstance(cfg.own_device_id, int):
        cfg.own_device_id = get_device_id_by_index(cfg, cfg.own_device_id)


def __drop_irrelevant_keys__(cfg: DictConfig) -> DictConfig:
    """
    Removes keys from config not needed to instantiate the specified _target_ class.
    Assumes that cfg has a key _target_. Hydra keys  _recursive_ and _partial_ are not removed.

    Args:
        cfg: The controller configuration.

    Returns:
        A DictConfig without unnecessary keys.
    """
    controller_class = get_class(cfg._target_)
    controller_signature = signature(controller_class.__init__)
    controller_args = controller_signature.parameters.keys()

    # These are special hydra keywords that we do not want to filter out.
    special_keys = ["_target_", "_recursive_", "_partial_"]
    cfg = {k: v for k, v in cfg.items() if k in controller_args or k in special_keys}
    return cfg


def drop_irrelevant_keys_recursively(cfg: DictConfig) -> DictConfig:
    """
    Removes parameters that are not necessary to instantiate the specified classes.
    This is done for the controller class as well as for scheduler and adaptive threshold if present.
    This is needed because hydra's instantiation mechanism expects that all given parameters are actually needed.

    Args:
        cfg: The controller configuration.

    Returns:
        A DictConfig that contains only the parameters actually needed to instantiate the specified classes.
    """
    cfg.controller = __drop_irrelevant_keys__(cfg.controller)
    if cfg.controller.get("scheduler", False):
        cfg.controller.scheduler = __drop_irrelevant_keys__(cfg.controller.scheduler)
    if cfg.controller.get("adaptive_threshold_fn", False):
        cfg.controller.adaptive_threshold_fn = __drop_irrelevant_keys__(
            cfg.controller.adaptive_threshold_fn
        )
    return cfg


def instantiate_controller(cfg: DictConfig):  # -> BaseController:
    """
    Instantiates a controller based on the configuration. This method filters out extra parameters defined through hydra
    but not required by the controller's init method. This allows for hydra's multirun feature to work even if
    controllers have different parameters (like next server schedulers).

    Args:
        cfg: The controller configuration.

    Returns:
        An instance of `BaseController`.
    """
    original_cfg = deepcopy(cfg)
    # Filter out any arguments not present in the controller constructor. This is a hack required to make multirun work.
    # We want to be able to use different scheduling strategies combined with different controllers. But hydra's
    # `instantiate` method is strict and fails if it receives any extra arguments.
    cfg = drop_irrelevant_keys_recursively(cfg)

    # Update the device ID and set it to controller.
    cfg.own_device_id = "controller"

    # Instantiate the controller.
    controller: BaseController = instantiate(cfg.controller)(cfg=original_cfg)
    return controller


def get_torch_device_id(cfg: DictConfig) -> str:
    """
    Returns the configured torch_device for the current device.
    Resorts to default if no torch_device is configured.

    Args:
        cfg (DictConfig): The config loaded from YAML files.

    Returns:
        The id of the configured torch_device for the current device.

    Raises:
        StopIteration: If the device with the given ID cannot be found.
        ConfigAttributeError: If no device id is present in the config.
    """
    own_device_id = cfg.own_device_id
    try:
        return next(
            device_cfg.torch_device
            for device_cfg in cfg.topology.devices
            if device_cfg.device_id == own_device_id
        )
    except ConfigAttributeError:
        return _default_torch_device()


def _default_torch_device():
    """
    Returns the default torch devices, depending on whether cuda is available.
    """
    return "cuda:0" if torch.cuda.is_available() else "cpu"

from __future__ import annotations

import abc
from typing import List, Optional, Dict, Tuple, Any

import torchmetrics
from torch import Tensor
from torchmetrics import Accuracy, F1Score, AUROC


def create_metrics(
    metric_names: List[str], num_classes: int, average_setting: str
) -> ModelMetricContainer:
    """
    Returns the torchmetrics metrics instance based on the name and settings.

    Args:
         metric_names (List[str]): list of metric names to instantiate.
         num_classes (int): number of classes.
         average_setting (str): defines the reduction to be applied.

    Returns:
        ModelMetricContainer: A container holding an instance for each passed metric name.
    """
    result = ModelMetricContainer()
    if "accuracy" in metric_names:
        acc = Accuracy(num_classes=num_classes, average=average_setting)
        acc.custom_name = "accuracy"
        acc.num_samples = 0
        result.add_metric(acc, "accuracy")
    if "f1" in metric_names:
        f1 = F1Score(num_classes=num_classes, average=average_setting)
        f1.custom_name = "f1"
        f1.num_samples = 0
        result.add_metric(f1, "f1")
    if "auc" in metric_names:
        auc = AUROC(num_classes=num_classes, average=average_setting)
        auc.custom_name = "auc"
        auc.num_samples = 0  # doesn't seem to be an official attribute.
        result.add_metric(auc, "auc")
    return result


class MetricResult(abc.ABC):
    """
    Base class for metric results. Defines shared attributes and required methods.

    Args:
        device_id (str): The device's ID the metric has been measured on.
        name (str): The name of the metric.
    """

    def __init__(self, device_id: str, name: str):
        self.device_id = device_id
        self.name = name

    @abc.abstractmethod
    def as_loggable_dict(self):
        """
        Returns the result in a convenient dictionary format. Used for structural logging.
        """
        pass

    @abc.abstractmethod
    def __eq__(self, other: Any) -> bool:
        """For unit testing purposes."""
        pass


class ModelMetricResult(MetricResult):
    """
    A class representing a single metric result for a given device and experiment phase.

    Attributes:
        device_id (str): The device's ID the metric has been measured on.
        name (str): The name of the metric.
        phase (str): The phase during which the metric has been measured. Can be 'train' or 'test' or similar values.
        num_samples (int): The number of samples that were used to compute the metric.
    """

    def __init__(self, device_id: str, name: str, phase: str, value, num_samples: int):
        super().__init__(device_id, name)
        self.phase = phase
        self.value = value
        self.num_samples = num_samples

    def as_loggable_dict(self, round_no: Optional[int] = None):
        """
        Returns the result in a convenient dictionary format for logging.

        Args:
            round_no (int, optional): The number of the current round. If given, the round number is added to the
                dictionary.

        Returns:
            A dictionary with the metric name as key and a dictionary with the value and the number of samples as value.

        Notes:
            Does not include the device id, as this information is added by the logger.
        """
        if round_no is None:
            return {
                f"{self.phase}_{self.name}": {
                    "value": self.value,
                    "num_samples": self.num_samples,
                }
            }
        return {
            f"{self.phase}_{self.name}": {
                "value": self.value,
                "num_samples": self.num_samples,
                "round": round_no,
            }
        }

    def __eq__(self, other):
        """For unit testing purposes."""
        return (
            self.device_id == other.device_id
            and self.name == other.name
            and self.phase == other.phase
            and self.value == other.value
            and self.num_samples == other.num_samples
        )


class DiagnosticMetricResult(MetricResult):

    def __init__(self, device_id: str, name: str, method: str, value: float):
        """
        Initializes the metric result with the given values.

        Attributes:
            device_id (str): The id of the device.
            name (str): The name of the metric such as time, flops or bytes.
            method (str): The method of the metric such as TrainEpoch.
            value (float): The value of the metric.

        Notes:
            Diagnostic metrics are not aggregated over multiple devices.
            Furthermore, there is no specific container, since the metrics are already finalized upon initialization.
        """
        super().__init__(device_id, name)
        self.value = value
        self.method = method

    def as_loggable_dict(self):
        return {f"{self.method}_{self.name}": {"value": self.value}}

    def __eq__(self, other) -> bool:
        """For unit testing purposes."""
        return (
            self.device_id == other.device_id
            and self.name == other.name
            and self.method == other.method
            and self.value == other.value
        )


class ModelMetricContainer:
    """
    A container class for multiple `ModelMetricResult`s.

    Attributes:
        metrics (Dict[str, Metric]): A dictionary of metric name and instance pairs.
    """

    def __init__(self):
        self.metrics: Dict[str, torchmetrics.Metric] = {}

    def add_metric(self, metric: torchmetrics.Metric, name: str):
        """
        Adds a metric to the metric container.

        Args:
            metric (torchmetrics.Metric): The metric to add.
            name (str): The name of the metric.
        """
        self.metrics[name] = metric

    def metrics_on_batch(self, prediction: Tensor, labels: Tensor) -> list:
        """
        Appends the predictions and labels to the metric objects for the current batch.

        Args:
            prediction (torch.Tensor): The prediction.
            labels (torch.Tensor): The labels.

        Returns:
            The results of the batch a list of the outputs of the torchmetrics.

        Notes:
            Assumes that the first dimension of the predictions is the batch dimension.
        """
        result = []
        for metric in self.metrics.values():
            result.append(metric(prediction, labels))
            metric.num_samples += prediction.shape[
                0
            ]  # add size of batch dimension to number of samples
        return result

    def compute_metrics(self, phase: str, device_id: str) -> List[ModelMetricResult]:
        """
        Computes the overall metrics based on all accumulated values so far.

        Args:
            phase (str): The phase to compute the metrics for.
            device_id (str): The device ID to filter the collected metrics by.

        Returns:
            List[ModelMetricResult]: A list of metrics inside the container, filtered. The list is empty if no metrics
                are available or no predictions have been made so far.
        """
        result = []
        for name, metric in self.metrics.items():
            if metric.num_samples > 0:
                if hasattr(metric, "custom_name"):
                    result.append(
                        ModelMetricResult(
                            device_id,
                            metric.custom_name,
                            phase,
                            metric.compute(),
                            metric.num_samples,
                        )
                    )
                else:
                    result.append(
                        ModelMetricResult(
                            device_id,
                            type(metric),  # FIXME: should probably be str(type(metric))
                            phase,
                            metric.compute(),
                            metric.num_samples,
                        )
                    )
        return result

    def reset_metrics(self):
        """
        Resets the given metric objects by calling their `reset` method. Sets the `num_samples` attribute to 0.
        """
        for metric in self.metrics.values():
            metric.reset()
            metric.num_samples = 0


class ModelMetricResultContainer:
    """
    A container class for holding multiple `ModelMetricResult` instances.

    Attributes:
        results (Dict[Tuple[str, str], List[ModelMetricResult]]): A dictionary holding the `ModelMetricResult`
            instances. The key is a tuple that holds the name and the phase of the model metric result. The value is the
            result itself.
    """

    def __init__(self, results: Optional[List[ModelMetricResult]] = None):
        self.results: Dict[Tuple[str, str], List[ModelMetricResult]] = {}
        if results is not None:
            self.add_results(results)

    def get(self, key: Tuple[str, str]):
        return self.results[key]

    def add_results(self, results: List[ModelMetricResult]):
        """
        Adds the `ModelMetricResult`s to the container.
        """
        for result in results:
            self.add_result(result)

    def add_result(self, result: ModelMetricResult):
        """
        Adds a single `ModelMetricResult` to the container.

        Args:
            result (ModelMetricResult): The result to add.
        """
        name = result.name
        phase = result.phase
        if (name, phase) in self.results:
            self.results[(name, phase)] += [result]
        else:
            self.results[(name, phase)] = [result]

    def merge(self, other):
        """Merges the results of the other MetricResultContainer into this one."""
        for key, value in other.results.items():
            if key in self.results:
                self.results[key] += value
            else:
                self.results[key] = value

    def get_aggregated_metrics(self) -> ModelMetricResultContainer:
        """Returns a new MetricResultContainer with the aggregated metrics."""
        aggregates = ModelMetricResultContainer()
        for key, result_list in self.results.items():
            aggregates.add_result(
                ModelMetricResult(
                    device_id="aggregated",
                    name=key[0],
                    phase=key[1],
                    value=sum(
                        [result.value * result.num_samples for result in result_list]
                    )
                    / sum([result.num_samples for result in result_list]),
                    num_samples=sum([result.num_samples for result in result_list]),
                )
            )
        return aggregates

    def get_as_list(self) -> List[ModelMetricResult]:
        """Returns the results as a list."""
        result = []
        for key, result_list in self.results.items():
            result += result_list
        return result

    def get_raw_metrics(self):
        return self.results

    def __eq__(self, other) -> bool:
        """For unit testing purposes."""
        for key, value in self.results.items():
            if key not in other.results:
                return False
            if len(value) != len(other.results[key]):
                return False
            for i in range(len(value)):
                if value[i] != other.results[key][i]:
                    return False
        return True


class DiagnosticMetricResultContainer:

    def __init__(self, results: Optional[List[DiagnosticMetricResult]] = None):
        self.results: Dict[(str, str), List[DiagnosticMetricResult]] = {}
        if results is not None:
            self.add_results(results)

    def add_results(self, results: List[DiagnosticMetricResult]):
        for result in results:
            self.add_result(result)

    def add_result(self, result: DiagnosticMetricResult):
        name = result.name
        method = result.method
        if (name, method) in self.results:
            self.results[(name, method)] += [result]
        else:
            self.results[(name, method)] = [result]

    def merge(self, other):
        """Merges the results of the other MetricResultContainer into this one."""
        if other is not None and type(other) == DiagnosticMetricResultContainer:
            for key, value in other.results.items():
                if key in self.results:
                    self.results[key] += value
                else:
                    self.results[key] = value

    def get_as_list(self) -> List[DiagnosticMetricResult]:
        """Returns the results as a list."""
        result = []
        for key, result_list in self.results.items():
            result += result_list
        return result

    def get_raw_metrics(self):
        return self.results

    def __eq__(self, other):
        """For unit testing purposes."""
        for key, value in self.results.items():
            if key not in other.results:
                return False
            if len(value) != len(other.results[key]):
                return False
            for i in range(len(value)):
                if value[i] != other.results[key][i]:
                    return False
        return True


def compute_metrics_for_optimization(
    metrics: DiagnosticMetricResultContainer,
    samples_per_device: Dict[str, Tuple[int, int]],
    batch_size: int,
):
    """
    Computes the metrics for the optimization.

    Args:
        metrics (DiagnosticMetricResultContainer): The diagnostic metrics of one round.
        samples_per_device (dict[str, (int, int)]): The number of samples per device. The key is the device id, the value is the number of samples (train, validation).
        batch_size (int): The batch size.

    Returns:
        A dictionary with the metrics.

    Raises:
        KeyError: if keys are missing which may be the case if not all diagnostic metrics were reported e.g. because a device ran out of battery.

    Notes:
        Assumes that the metrics result from exactly one round. I.e. we assume that there is only one server device and e.g. all train_batch metrics are from this device.
        Also, we assume that the client train time per sample is larger than the client eval time per sample.
        This should be given, due the additional time for the backpropagation during training. Otherwise, the results are not meaningful, for example if there is a huge lag during evaluation.
    """
    raw_metrics = metrics.get_raw_metrics()
    result = {}
    num_devices = len(samples_per_device.keys())

    # byte size per sample
    result["gradient_size"] = max(
        [metric.value / batch_size for metric in raw_metrics[("size", "gradients")]]
    )
    result["label_size"] = max(
        [metric.value / batch_size for metric in raw_metrics[("size", "labels")]]
    )
    result["smashed_data_size"] = max(
        [metric.value / batch_size for metric in raw_metrics[("size", "smashed_data")]]
    )

    # total byte size
    result["client_weight_size"] = max(
        [metric.value for metric in raw_metrics[("size", "client_weights")]]
    )  # all values should be equal anyway
    result["server_weight_size"] = max(
        [metric.value for metric in raw_metrics[("size", "server_weights")]]
    )  # all values should be equal anyway
    result["optimizer_state_size"] = max(
        [metric.value for metric in raw_metrics[("size", "optimizer_state")]]
    )  # all values should be equal anyway

    # time
    result["train_global_time"] = max(
        [metric.value for metric in raw_metrics[("comp_time", "train_global")]]
    )  # should be only one value anyway
    avg_server_model_train_time_per_sample = (
        sum([metric.value for metric in raw_metrics[("comp_time", "train_batch")]])
        / len(raw_metrics[("comp_time", "train_batch")])
        / batch_size
    )
    avg_server_model_evaluate_time_per_sample = (
        sum([metric.value for metric in raw_metrics[("comp_time", "evaluate_batch")]])
        / len(raw_metrics[("comp_time", "evaluate_batch")])
        / batch_size
    )
    client_train_time_per_sample = {}
    client_eval_time_per_sample = {}
    for device_id in samples_per_device.keys():
        # each device: compute the average training time on the client model per sample
        train_epoch_time_list = [
            metric.value
            for metric in raw_metrics[("comp_time", "client_train_epoch_time")]
            if metric.device_id == device_id
        ]  # in case of multiple epochs on one device per round
        client_train_time_per_sample[device_id] = sum(train_epoch_time_list) / (
            samples_per_device[device_id][0] * len(train_epoch_time_list)
        )
        # analogously for the client eval time
        eval_time_list = [
            metric.value
            for metric in raw_metrics[("comp_time", "client_eval_epoch_time")]
            if metric.device_id == device_id
        ]
        client_eval_time_per_sample[device_id] = sum(eval_time_list) / (
            samples_per_device[device_id][1] * len(eval_time_list)
        )
    # min train time per sample on client
    # use eval time as estimate for the forward pass
    result["client_norm_fw_time"] = min(
        [
            client_eval_time_per_sample[device_id]
            for device_id in client_train_time_per_sample.keys()
        ]
    )
    # backward pass = train time - forward pass
    bw_estimate = [
        client_train_time_per_sample[device_id] - client_eval_time_per_sample[device_id]
        for device_id in client_train_time_per_sample.keys()
        if client_train_time_per_sample[device_id]
        - client_eval_time_per_sample[device_id]
        > 0
    ]
    # make sure that training time is larger than eval time
    if len(bw_estimate) > 0:
        result["client_norm_bw_time"] = min(bw_estimate)
    # otherwise estimate the bw time with the training time per sample (which is still smaller than the eval time then)
    else:
        result["client_norm_bw_time"] = min(
            [
                client_train_time_per_sample[device_id]
                for device_id in client_train_time_per_sample.keys()
            ]
        )

    # comp speed, normalized to the fastest device
    min_client_train_time = min(client_train_time_per_sample.values())
    result["comp_latency_factor"] = {
        device_id: client_train_time_per_sample[device_id] / min_client_train_time
        for device_id in client_train_time_per_sample.keys()
    }

    # weight train time per sample on server with the server device's comp speed
    server_device_id = raw_metrics[("comp_time", "train_batch")][0].device_id
    result["server_norm_fw_time"] = (
        avg_server_model_evaluate_time_per_sample
        * result["comp_latency_factor"][server_device_id]
    )
    # again make sure that training time is larger than eval time
    server_bw_estimate = (
        avg_server_model_train_time_per_sample
        - avg_server_model_evaluate_time_per_sample
    ) * result["comp_latency_factor"][server_device_id]
    if server_bw_estimate > 0:
        result["server_norm_bw_time"] = server_bw_estimate
    else:
        result["server_norm_bw_time"] = (
            avg_server_model_train_time_per_sample
            * result["comp_latency_factor"][server_device_id]
        )

    return result

import itertools
import math
from collections import defaultdict
from typing import Optional, List, Tuple, Any

import torch
from torch.utils.data import random_split, Subset
from torch.utils.data.dataset import Dataset


def __get_partitioned_data_for_device__(
    dataset: Dataset,
    device_index: int,
    num_devices: int,
    fractions: Optional[List[float]] = None,
    seed: int = 42,
    distribution: Optional[str] = None,
) -> Subset:
    """
    Returns the data partition for the given device index.

    Args:
        dataset(Dataset): The dataset to partition.
        device_index(int): The index of the device to get the partition.
        num_devices(int): The total number of devices.
        fractions (List[float], optional): The fractions of the dataset to assign to each device.
        seed (int, optional): The seed for the random number generator.
        distribution (str, optional): The distribution of the data. Set to "non-iid" for non-iid, otherwise defaults to
            iid.

    Returns:
        The partition of the dataset for the given device index.

    Raises:
        ValueError: If the sum of the fractions is greater than 1.

    Notes:
        If fractions is not given or too short, it will be set to 1/num_devices for each device.
        If the sum of the fractions is less than 1, a dummy subset is added to sum up to 1.
        If the distribution is non-iid, the data is grouped by label and split in half. The first half is shuffled
        while the second half remains ordered. Then the halfs are merged and the dataset is split according to the
        fractions. Not supported by every dataset since it has to be reinitialized, so test beforehand with new
        datasets.
    """
    if fractions is None or len(fractions) < num_devices:
        quotient, remainder = divmod(len(dataset), num_devices)
        fractions = [quotient] * num_devices
        if remainder > 0:
            for i in range(remainder):
                fractions[i] += 1
    elif sum(fractions) != 1:
        if sum(fractions) > 1:
            if not math.isclose(sum(fractions), 1):
                raise ValueError("Sum of lengths must be <= 1.")
        else:
            # to make sampling subsets possible, add a dummy subset to sum lengths up to 1
            # use round to avoid floating point errors leading to remainders that are added to the other subsets
            fractions += [1 - round(sum(fractions), 4)]
    if distribution == "non-iid":
        split_position = 0.6  # split at 60% of the data, i.e. shuffle the first 60% and keep the rest ordered
        # group by label
        partitions_by_label = defaultdict(list)
        dataset_class = type(dataset)
        for sample, label in dataset:
            partitions_by_label[str(label)].append((sample, label))
        # create lists sorted by label
        data_list = []
        label_list = []
        for key, partition in partitions_by_label.items():
            for sample, label in partition:
                data_list.append(sample)
                label_list.append(label)
        # split both lists at the split position
        split_idx = int(len(data_list) // (1 / split_position))
        data_list1, data_list2 = data_list[:split_idx], data_list[split_idx:]
        label_list1, label_list2 = label_list[:split_idx], label_list[split_idx:]
        # shuffle first half
        generator = torch.Generator().manual_seed(seed)
        indices = torch.randperm(len(data_list1), generator=generator)
        data_list1 = [data_list1[i] for i in indices]
        label_list1 = [label_list1[i] for i in indices]
        # merge lists again
        data_list = data_list1 + data_list2
        label_list = label_list1 + label_list2
        reordered_dataset = SimpleDataset(data_list, label_list)
        # split in order according to the fractions
        splits = []
        for i, f in enumerate(fractions):
            start = int(sum(fractions[:i]) * len(data_list))
            end = int(start + len(data_list) * f)
            splits.append(Subset(reordered_dataset, range(start, end)))
        # transform to dataset
        return splits[device_index]
    else:
        generator = torch.Generator().manual_seed(seed)
        splits = random_split(dataset, fractions, generator=generator)
    return splits[device_index]


class DataPartitioner:
    """
    Deterministically splits datasets into chunks of specified size based on seed and device index.

    Instead of sending training and test data to each device over the network, this helper class is used to split the
    dataset in non-overlapping chunks. Thus, an instance of this class is instantiated by each device.

    Given the same parameters, the class will always split the data in the same way.

    Attributes:
        device_index (int): The device the data partitioner will split data for.
        num_devices (int): The total number of devices that data needs to be split for.
        fractions (List[float], optional): List of fractions that represent the amount of data to split for a device.
            The fraction at position `i` is used to partition the data for a device at index `i`. Defaults to `None`,
            meaning that all data is partitioned into same-sized chunks.
        seed (int): The seed of the PRNG. Defaults to 42.
        distribution (str, optional): The distribution to use for partitioning the data. Defaults to `None`, meaning
            that the data is split randomly. Possible values are `iid` and `non-iid`.
    """

    def __init__(
        self,
        device_index: int,
        num_devices: int,
        fractions: Optional[List[float]] = None,
        seed: int = 42,
        distribution: Optional[str] = None,
    ):
        self.device_index = device_index
        self.num_devices = num_devices
        self.fractions = fractions
        self.seed = seed
        self.distribution = distribution

    def partition(self, data: Dataset):
        return __get_partitioned_data_for_device__(
            data,
            self.device_index,
            self.num_devices,
            self.fractions,
            self.seed,
            self.distribution,
        )


class SimpleDataset(Dataset):
    """
    Dataset for wrapping data and labels after non-iid partitioning. Possible transformations are already applied at the
    partitioning, so no further transformations are needed.
    """

    def __init__(self, data, labels):
        self.data = data
        self.labels = labels

    def __len__(self) -> int:
        return len(self.labels)

    def __getitem__(self, idx: int) -> Tuple[Any, Any]:
        return self.data[idx], self.labels[idx]

from typing import Union, Tuple

from fvcore.nn import FlopCountAnalysis
from torch import Tensor, nn

from edml.models.autoencoder import ClientWithAutoencoder, ServerWithAutoencoder
from edml.models.provider.base import has_autoencoder


def estimate_model_flops(
    model: nn.Module, sample: Union[Tensor, Tuple[Tensor, ...]]
) -> dict[str, int]:
    """
    Estimates the FLOPs of one forward pass and one backward pass of the model using the sample data provided.
    If forward and backward pass affect the same parameters, the number of FLOPs of one backward pass is assumed to be
    two times the number of FLOPs for the backward pass.
    In case of non-trainable parameters as an autoencoder in between, the number of backward FLOPs is assumed to be
    twice the number of the forward FLOPs of the trainable part, while the number of actual forward FLOPs is estimated
    using the full model.
    Args:
        model (nn.Module): the neural network model to calculate the FLOPs for.
        sample: The data used to calculate the FLOPs. The first dimension should be the batch dimension.

    Returns:
        dict(str, int): {"FW": #ForwardFLOPs, "BW": #BackwardFLOPs} the number of estimated forward and backward FLOPs.
    """
    fw_flops = FlopCountAnalysis(model, sample).total()
    if has_autoencoder(model):
        bw_flops = 0
        if isinstance(
            model, ClientWithAutoencoder
        ):  # run FW pass until Encoder and multiply by 2
            bw_flops = FlopCountAnalysis(model.model, sample).total() * 2
        elif isinstance(
            model, ServerWithAutoencoder
        ):  # run FW pass from decoder output and multiply by 2
            bw_flops = (
                FlopCountAnalysis(model.model, model.autoencoder(sample)).total() * 2
            )
        else:
            raise NotImplementedError()
    else:
        bw_flops = 2 * fw_flops

    return {"FW": fw_flops, "BW": bw_flops}

from typing import List, Tuple

from torch import nn as nn


class Part(nn.Module):
    """
    A part of a model that has been split after a specific layer.

    Instances of this class are usually returned by calling py:meth:`~split_network_at_layer`.

    Attributes:
        layers (List[nn.Module]): The layers of this part.
    """

    def __init__(self, layers: List[nn.Module]):
        super(Part, self).__init__()
        for idx, layer in enumerate(layers):
            self.add_module(f"layer{idx}", layer)
        self.layers = layers

    def forward(self, x):
        """Calls each of its layers one after the other."""
        for layer in self.layers:
            x = layer(x)
        return x


def split_network_at_layer(network: nn.Module, cut_layer: int) -> Tuple[Part, Part]:
    """
    Splits a given network at the given layer.

    Args:
        network (nn.Module): The network to split.
        cut_layer (int): The index of the layer to cut the network at.

    Returns:
        Tuple[Part, Part]: The two parts of the network.

    Raises:
        ValueError: If the `cut_layer` is out of the model's range. I.e., if the model has less than `cut_layer + 2`
        layers.

    Notes:
        The cut_layer is included in the first part.
        Assumes that the network is a sequential model and all layers are defined in the correct order as children of
        the given network.
    """
    children = list(network.children())
    if cut_layer > len(children) - 1:
        raise ValueError("cut_layer is out of the model's range.")

    part1 = Part(children[:cut_layer])
    part2 = Part(children[cut_layer:])

    return part1, part2

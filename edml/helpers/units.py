from typing import Final

#: Factor to divide by to convert `u` into mega-u.
MEGA_FACTOR: Final[int] = 1_000_000

#: Factor to divide by to convert `u` into giga-u.
GIGA_FACTOR: Final[int] = MEGA_FACTOR * 1000

import abc
from typing import Any

import wandb
from omegaconf import DictConfig


class SimpleLogger(abc.ABC):
    """
    A simple logger abstraction.
    """

    @abc.abstractmethod
    def log(self, message: Any):
        """
        Logs the given data.
        """
        pass

    def start_experiment(self):
        """
        Event hook called at the start of an experiment.
        """
        print("Starting experiment...")

    def end_experiment(self):
        """
        Event hook called at the end of an experiment.
        """
        print("Ending experiment...")


class ConsoleLogger(SimpleLogger):
    """
    A logger implementation that logs everything to the console.

    Attributes:
        device_id (str): The device id of the device that logs the data.
    """

    def __init__(self, device_id: str):
        self.device_id = device_id

    def log(self, message: Any):
        print(f"Device {self.device_id}: {message}")


class WandbLogger(SimpleLogger):
    """
    A logger implementation logging evaluation metrics to wandb.ai.
    """

    def __init__(self, cfg: DictConfig, device_id: str):
        self.device_id = device_id
        self.wandb_enabled = False
        self.cfg = cfg

    def start_experiment(self):
        """
        Override event hook to start login and initialize wandb client.
        """
        with open(self.cfg.wandb.key_path, "r") as f:
            key = f.read().strip()
        wandb.login(key=key)
        wandb.init(
            entity=self.cfg.wandb.entity,
            project=self.cfg.experiment.project,  # project = set of experiments
            job_type=self.cfg.experiment.job,  # train or test
            group=self.cfg.group,
            name=self.cfg.own_device_id,  # name runs by device id
            config=dict(self.cfg),
        )
        self.wandb_enabled = True

    def end_experiment(self):
        """Ends the wandb run."""
        wandb.finish()
        self.wandb_enabled = False

    def log(self, message: Any):
        """
        Override to differentiate between dictionary messages and normal string messages.

        Dict values are logged to wandb as captured metrics whereas strings are logged to the console.
        """
        if type(message) is dict and self.wandb_enabled:
            wandb.log(message)
        else:
            if not self.wandb_enabled and type(message) is dict:
                print("Wandb not running, printing to stdout instead.")
            print(f"Device {self.device_id}: {message}")


def create_logger(cfg: DictConfig):
    """
    Creates the right logger based on the provided experiment configuration.
    """
    if cfg.wandb.enabled:
        return WandbLogger(cfg=cfg, device_id=cfg.own_device_id)
    else:
        return ConsoleLogger(device_id=cfg.own_device_id)

> Inheritance-based controller configurations.

Allows custom next server scheduling strategies to be configured without changing code.

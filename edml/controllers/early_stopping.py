import warnings

from edml.helpers.metrics import ModelMetricResultContainer


class EarlyStopping(object):
    def __init__(self, metric: str, patience=10, phase="val"):
        """
        Implementation of early stopping for the training loop.

        Args:
            metric (string): The metric to be monitored.
            patience (int): The number of epochs of no improvement to stop after.
            phase (string): The phase to monitor the metric on.

        Returns:
            None

        Raises:
            ValueError: If the given metric is None.

        Notes:
                Only works if the metric of interest is present in the given metrics when called later on.
                Uses the metric values obtained during validation by default.
        """
        if metric is None:
            raise ValueError("Early stopping metric must not be None")
        self.metric = metric
        self.patience = patience
        self.phase = phase
        self.best_score = None
        self.best_epoch = None
        self.counter = 0
        self.early_stop = False

    def __call__(self, metrics: ModelMetricResultContainer, epoch):
        """
        Implementation of early stopping for the training loop.

        Args:
            metrics (ModelMetricResultContainer): The metrics of the current epoch.
            epoch (int): The number the current epoch.

        Returns:
            bool: Whether to stop training.

        Raises:
            Warning if the metric of interest is not present in the given metrics.
        Notes:
                Requires the metric of interest (in the defined phase) to be present in the given metrics.
                Otherwise, a warning is raised and the round is not counted.
        """
        try:
            score = (
                metrics.get_aggregated_metrics()
                .results[(self.metric, self.phase)][0]
                .value
            )
        except KeyError:
            warnings.warn(
                f"Early stopping metric {self.metric} not found in given metrics. Skipping this round."
            )
            return False
        if self.best_score is None:
            self.best_score = score
            self.best_epoch = epoch
        elif score <= self.best_score:
            self.counter += 1
            if self.counter >= self.patience:
                return True
        else:
            self.best_score = score
            self.best_epoch = epoch
            self.counter = 0
        return False


def create_early_stopping_callback(cfg):
    """
    A factory function to create an early stopping callback.

    Args:
        cfg (OmegaConf): The configuration object.

    Returns:
        EarlyStopping: The early stopping callback.
    Raises:
        None
    Notes:
        If early stopping is deactivated, a callback that always returns false is returned.
    """
    if cfg.experiment.early_stopping:
        return EarlyStopping(
            metric=cfg.experiment.early_stopping_metric,
            patience=cfg.experiment.early_stopping_patience,
        )
    return lambda *args: False

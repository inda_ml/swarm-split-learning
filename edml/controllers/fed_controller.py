import concurrent.futures
import contextlib
import functools
import threading
from typing import Dict, List

from overrides import override

from edml.controllers.base_controller import BaseController
from edml.helpers.decorators import Timer
from edml.helpers.metrics import ModelMetricResultContainer


def _drop_empty_weights(weights: list[Dict]) -> list[Dict]:
    """Drops all weights that are empty."""
    return [weight for weight in weights if weight is not None and len(weight) > 0]


def fed_average(model_weights: list[Dict], weighting_scheme: List[float] = None):
    """Computes the federated average of the given weights.
    If a weighting scheme (percentage or number of examples per model) is specified, the weighted average is computed.
    """
    averaged_weights = {}
    model_weights = _drop_empty_weights(model_weights)
    if len(model_weights) > 0:
        if weighting_scheme is None:
            for key in model_weights[0].keys():
                averaged_weights[key] = sum(
                    weight[key] for weight in model_weights
                ) / len(model_weights)
        else:
            for key in model_weights[0].keys():
                averaged_weights[key] = sum(
                    weight[key] * weighting_scheme[i]
                    for i, weight in enumerate(model_weights)
                ) / sum(weighting_scheme)
        return averaged_weights
    return None


def simulate_parallelism(f):
    @functools.wraps(f)
    def inner(self):
        pass

    return inner


class FedController(BaseController):
    """Controller for federated learning."""

    def __init__(self, cfg):
        super().__init__(cfg)

    def _fed_train_round(self, round_no: int = -1):
        """Returns new client and server weights."""
        client_weights = []
        server_weights = []
        samples_count = []
        metrics_container = ModelMetricResultContainer()

        if self.cfg.simulate_parallelism:
            parallel_times = []
            with Timer() as elapsed_time:
                for device_id in self.active_devices:
                    with Timer() as individual_time:
                        response = self.request_dispatcher.federated_train_on(
                            device_id, round_no
                        )
                        if response is not False:
                            (
                                new_client_weights,
                                new_server_weights,
                                num_samples,
                                metrics,
                                _,
                            ) = response  # skip diagnostic metrics
                            client_weights.append(new_client_weights)
                            server_weights.append(new_server_weights)
                            samples_count.append(num_samples)
                            metrics_container.merge(metrics)
                    parallel_times.append(individual_time.execution_time)
            self.logger.log(
                {
                    "parallel_fed_time": {
                        "elapsed_time": elapsed_time.execution_time,
                        "parallel_time": max(parallel_times),
                    }
                }
            )
        else:
            with concurrent.futures.ThreadPoolExecutor(
                max_workers=max(len(self.active_devices), 1)
            ) as executor:  # avoid exception when setting 0 workers
                futures = [
                    executor.submit(
                        self.request_dispatcher.federated_train_on, device_id, round_no
                    )
                    for device_id in self.active_devices
                ]
                for future in concurrent.futures.as_completed(futures):
                    response = future.result()
                    if response is not False:
                        (
                            new_client_weights,
                            new_server_weights,
                            num_samples,
                            metrics,
                            _,
                        ) = response  # skip diagnostic metrics
                        client_weights.append(new_client_weights)
                        server_weights.append(new_server_weights)
                        samples_count.append(num_samples)
                        metrics_container.merge(metrics)

        print(f"samples count {samples_count}")
        return (
            fed_average(model_weights=client_weights, weighting_scheme=samples_count),
            fed_average(model_weights=server_weights, weighting_scheme=samples_count),
            metrics_container,
        )

    @override
    def _train(self):
        """Runs the federated training algorithm for the configured number of rounds."""
        print("Training with FedController")
        for i in range(self.cfg.experiment.max_rounds):
            print(f"Round {i}")
            self._refresh_active_devices()

            avg_client_weights, avg_server_weights, metrics = self._fed_train_round(
                round_no=i
            )

            self.logger.log(
                {"remaining_devices": {"devices": len(self.active_devices), "round": i}}
            )

            self._aggregate_and_log_metrics(metrics, i)

            early_stop = self.early_stopping(metrics, i)
            if early_stop:
                break

            self._save_weights(avg_client_weights, avg_server_weights, i)
            self._refresh_active_devices()

            # set new weights
            self._set_weights_on_all_devices(avg_client_weights, on_client=True)
            self._set_weights_on_all_devices(avg_server_weights, on_client=False)

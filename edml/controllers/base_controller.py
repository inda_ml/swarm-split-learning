import abc
import concurrent.futures
from typing import Optional

import torch

from edml.controllers.early_stopping import create_early_stopping_callback
from edml.core.device import DeviceRequestDispatcher
from edml.core.start_device import _get_models
from edml.helpers.logging import SimpleLogger, create_logger
from edml.helpers.metrics import ModelMetricResultContainer
from edml.helpers.types import DeviceBatteryStatus


class BaseController(abc.ABC):
    """Base class for all controllers. Provides the basic functionality for training on multiple devices.
    Specific training can be implemented by overriding the _train method."""

    def __init__(self, cfg):
        self.cfg = cfg
        self.cfg.own_device_id = (
            "controller"  # set own device id here since needed by the logger
        )
        self.logger: SimpleLogger = create_logger(cfg)
        self.devices = cfg.topology.devices[: cfg.num_devices]
        self.request_dispatcher = DeviceRequestDispatcher(self.devices)
        self.active_devices = self.request_dispatcher.active_devices()
        self.device_batteries_status = {}
        self.early_stopping = create_early_stopping_callback(cfg)

    def train(self):
        """Runs the training algorithm implemented by the controller."""
        print("Starting training")
        if (
            "load_weights" in self.cfg.experiment
            and not self.cfg.experiment.load_weights
        ):
            self.logger.log(
                "WARNING: loading models via experiment configurations is deprecated. Please use a "
                "dedicated model provider instead."
            )

            # if no weights are loaded, initialize the models randomly and set them on all devices
            client_model, server_model = _get_models(self.cfg)
            self._set_weights_on_all_devices(
                client_model.state_dict(), on_client=True, wait_for_ready=True
            )
            self._set_weights_on_all_devices(
                server_model.state_dict(), on_client=False, wait_for_ready=True
            )
        self._start_experiment_on_active_devices()
        self.logger.start_experiment()  # start controller logging to wandb
        self._refresh_active_devices()
        print(f"Active devices: {self.active_devices}")
        self._train()
        self._end_experiment_on_active_devices()
        self.logger.end_experiment()

    @abc.abstractmethod
    def _train(self):
        """Hook for the training algorithm of the specific controller."""
        pass

    def _start_experiment_on_active_devices(self):
        """Starts the experiment on all given devices in parallel."""
        print(f"Starting experiment on {self.active_devices}")
        with concurrent.futures.ThreadPoolExecutor(
            max_workers=max(len(self.active_devices), 1)
        ) as executor:  # avoid exception when setting 0 workers
            for device_id in self.active_devices:
                executor.submit(
                    self.request_dispatcher.start_experiment_on, device_id, True
                )

    def _end_experiment_on_active_devices(self):
        """Ends the experiment on all given devices."""
        for device_id in self.active_devices:
            self.request_dispatcher.end_experiment_on(device_id)

    def _get_battery_status(self):
        """Returns the battery status of all active devices. Inactive devices receive None as battery status."""
        battery_status: dict[str, Optional[DeviceBatteryStatus]] = {}
        for device_id in self.active_devices:
            status = self.request_dispatcher.get_battery_status_on(device_id)
            if status is not False:
                battery_status[device_id] = status
            else:
                battery_status[device_id] = None
        return battery_status

    def _update_devices_battery_status(self):
        """Updates the battery status of all active devices.
        Inactive devices receive None as battery status. Also refreshes the list of active devices.
        """
        self.device_batteries_status = {
            device_id: None for device_id in self.device_batteries_status
        }  # reset to None to detect inactive devices
        self.device_batteries_status = (
            self.device_batteries_status | self._get_battery_status()
        )  # update with new values
        self._refresh_active_devices()

    def _refresh_active_devices(self):
        """Refreshes the active devices by checking if the request dispatcher is still connected to them.
        Device failure is only detected after the next request to the device."""
        self.active_devices = self.request_dispatcher.active_devices()

    def _get_device_ids(self) -> list[str]:
        return [device.device_id for device in self.devices]

    def _save_weights(self, client_weights, server_weights, round_no: int):
        """
        Saves the weights of the given round if saving weights is configured.

        Args:
            client_weights: The weights of the client model.
            server_weights: The weights of the server model.
            round_no: The number of the current round.

        Returns:
            None

        Raises:
            None

        Notes:
                If early stopping is configured, the weights are only saved if the current round is the best round so far.
        """
        if self.cfg.experiment.save_weights:
            print("\n###SAVING WEIGHTS###")
            if (
                self.cfg.experiment.early_stopping
                and self.early_stopping.best_epoch != round_no
            ):
                print("  triggered early stopping")
                return
            if client_weights:
                print("  saving client weights...")
                torch.save(
                    client_weights,
                    f"{self.cfg.experiment.client_model_save_path}{self.__model_prefix__()}_client_{round_no}.pth",
                )
            if server_weights:
                print("  saving server weights...")
                torch.save(
                    server_weights,
                    f"{self.cfg.experiment.server_model_save_path}{self.__model_prefix__()}_server_{round_no}.pth",
                )
            print("\n")

    def _load_weights(self, round_no: int):
        """Loads the weights from the configured directory if loading weights is configured."""
        return (
            torch.load(
                f"{self.cfg.experiment.client_model_save_path}{self.__model_prefix__()}_client_{round_no}.pth"
            ),
            torch.load(
                f"{self.cfg.experiment.server_model_save_path}{self.__model_prefix__()}_server_{round_no}.pth"
            ),
        )

    def _set_weights_on_all_devices(
        self, weights, on_client=True, wait_for_ready=False
    ):
        """Sets the weights on all devices."""
        for device_id in self.active_devices:
            self.request_dispatcher.set_weights_on(
                device_id, weights, on_client, wait_for_ready=wait_for_ready
            )

    def _devices_empty_or_only_server_left(self, server_device_id):
        if len(self.active_devices) == 0:
            return True
        elif (
            len(self.active_devices) == 1 and self.active_devices[0] == server_device_id
        ):
            return True
        return False

    def _aggregate_and_log_metrics(
        self, metrics: Optional[ModelMetricResultContainer], round_no: int
    ):
        """
        Aggregates and logs the metrics of the current round.

        Args:
            metrics (ModelMetricResultContainer): The metrics of the current epoch.
            round_no (int): The number of the current round.
        Returns:
            None
        Raises:
            None
        Notes:
                None
        """
        if metrics is not None:
            aggregated_metrics = metrics.get_aggregated_metrics()
            for metric_result in aggregated_metrics.get_as_list():
                self.logger.log(metric_result.as_loggable_dict(round_no))

    def __model_prefix__(self):
        """Returns the model prefix for the current experiment."""
        return f"{self.cfg.experiment.project}_{self.cfg.group}"

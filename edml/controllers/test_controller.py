import concurrent.futures
import os

from omegaconf.errors import ConfigAttributeError

from edml.controllers.base_controller import BaseController


class TestController(BaseController):
    """Controller for evaluating the models with the test data on all devices."""

    def _train(self):
        """
        Raises:
            ValueError: If no models are found with the path is defined in the config and the prefix saved.
        Notes:
            Evaluates the test data on all devices locally using the full network.
            Assumes sufficient (or infinite) battery on all devices.
            If config defines a 'best_round' attribute, the weights numbered with the corresponding round are used.
            Otherwise, the weights with the highest postfix number are used.
        """
        try:
            best_round = self.cfg.best_round
        except ConfigAttributeError:
            # assume client and server model have same highest postfix number
            best_round = self._get_model_with_highest_postfix_number(
                self.cfg.experiment.client_model_save_path
            )
        client_weights, server_weights = self._load_weights(best_round)
        self._set_weights_on_all_devices(client_weights, on_client=True)
        self._set_weights_on_all_devices(server_weights, on_client=False)

        with concurrent.futures.ThreadPoolExecutor(
            max_workers=max(len(self.active_devices), 1)
        ) as executor:  # avoid exception when setting 0 workers
            futures = [
                executor.submit(
                    self.request_dispatcher.evaluate_global_on, device_id, False, True
                )
                for device_id in self.active_devices
            ]
            concurrent.futures.wait(futures)

    def _get_model_with_highest_postfix_number(self, model_save_path):
        """Returns the highest postfix number in the given directory for the configured model_prefix."""
        model_prefix = f"{self.__model_prefix__()}_client_"  # assume server and client weights were saved appropriately
        highest_postfix_number = 0
        for file in os.listdir(model_save_path):
            if file.startswith(model_prefix):
                postfix_number = int(file.split("_")[-1].split(".")[0])
                if postfix_number > highest_postfix_number:
                    highest_postfix_number = postfix_number
        return highest_postfix_number

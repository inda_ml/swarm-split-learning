from edml.controllers.base_controller import BaseController


class SplitController(BaseController):

    def _train(self):
        client_weights = None
        server_device = self.cfg.topology.devices[0]
        for i in range(self.cfg.experiment.max_rounds):
            print(f"Round {i}")

            self._update_devices_battery_status()
            # break if no active devices or only server device left
            if self._devices_empty_or_only_server_left(server_device.device_id):
                print("No active client devices left.")
                break

            # set latest client weights on first device to train on
            self.request_dispatcher.set_weights_on(
                device_id=self.active_devices[0],
                state_dict=client_weights,
                on_client=True,
            )

            training_response = self.request_dispatcher.train_global_on(
                server_device.device_id, epochs=1, round_no=i
            )

            self._refresh_active_devices()
            self.logger.log(
                {"remaining_devices": {"devices": len(self.active_devices), "round": i}}
            )

            if training_response is False:  # server device unavailable
                break
            else:
                client_weights, server_weights, metrics, _, _ = (
                    training_response  # no need for optimizer state and diagnostic metrics
                )

                self._aggregate_and_log_metrics(metrics, i)

                early_stop = self.early_stopping(metrics, i)
                if early_stop:
                    break

                self._save_weights(client_weights, server_weights, i)

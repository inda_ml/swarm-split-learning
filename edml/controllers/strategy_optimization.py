import math

import numpy as np
from ortools.linear_solver import pywraplp


class DeviceParams:
    def __init__(
        self,
        device_id,
        initial_battery,
        current_battery,
        train_samples,
        validation_samples,
        comp_latency_factor,
    ):
        self.device_id = device_id
        self.initial_battery = initial_battery
        self.current_battery = current_battery
        self.train_samples = train_samples
        self.validation_samples = validation_samples
        self.comp_latency_factor = comp_latency_factor


class GlobalParams:
    def __init__(
        self,
        cost_per_sec=None,
        cost_per_byte_sent=None,
        cost_per_byte_received=None,
        cost_per_flop=None,
        client_fw_flops=None,  # mandatory forward FLOPs
        server_fw_flops=None,  # mandatory forward FLOPs
        client_bw_flops=None,  # optional backward FLOPs if bw FLOPs != 2 * fw FLOPs (e.g. for AE)
        server_bw_flops=None,  # optional backward FLOPs if bw FLOPs != 2 * fw FLOPs (e.g. for AE)
        smashed_data_size=None,
        label_size=None,
        gradient_size=None,
        batch_size=None,
        client_weights_size=None,
        server_weights_size=None,
        optimizer_state_size=None,
        client_norm_fw_time=None,
        client_norm_bw_time=None,
        server_norm_fw_time=None,
        server_norm_bw_time=None,
        train_global_time=None,
        last_server_device_id=None,
    ):
        self.cost_per_sec = cost_per_sec
        self.cost_per_byte_sent = cost_per_byte_sent
        self.cost_per_byte_received = cost_per_byte_received
        self.cost_per_flop = cost_per_flop
        self.client_fw_flops = client_fw_flops
        self.server_fw_flops = server_fw_flops
        self.client_bw_flops = client_bw_flops  # optional backward FLOPs if bw FLOPs != 2 * fw FLOPs (e.g. for AE)
        self.server_bw_flops = server_bw_flops  # optional backward FLOPs if bw FLOPs != 2 * fw FLOPs (e.g. for AE)
        self.smashed_data_size = smashed_data_size
        self.batch_size = batch_size
        self.client_weights_size = client_weights_size
        self.server_weights_size = server_weights_size
        self.optimizer_state_size = optimizer_state_size
        self.train_global_time = train_global_time
        self.last_server_device_id = last_server_device_id
        # metrics per sample
        self.label_size = label_size
        self.gradient_size = gradient_size
        self.client_norm_fw_time = client_norm_fw_time
        self.client_norm_bw_time = client_norm_bw_time
        self.server_norm_fw_time = server_norm_fw_time
        self.server_norm_bw_time = server_norm_bw_time

    def fill_values_from_config(self, cfg):
        self.cost_per_sec = cfg.battery.deduction_per_second
        self.cost_per_byte_sent = cfg.battery.deduction_per_mbyte_sent / 1000000
        self.cost_per_byte_received = cfg.battery.deduction_per_mbyte_received / 1000000
        self.cost_per_flop = cfg.battery.deduction_per_mflop / 1000000
        self.batch_size = cfg.experiment.batch_size

    def all_values_set(self):
        return (
            self.cost_per_sec is not None
            and self.cost_per_byte_sent is not None
            and self.cost_per_byte_received is not None
            and self.cost_per_flop is not None
            and self.client_fw_flops is not None
            and self.server_fw_flops is not None
            and self.optimizer_state_size is not None
            and self.smashed_data_size is not None
            and self.label_size is not None
            and self.gradient_size is not None
            and self.batch_size is not None
            and self.client_norm_fw_time is not None
            and self.client_norm_bw_time is not None
            and self.server_norm_fw_time is not None
            and self.server_norm_bw_time is not None
        )


class ServerChoiceOptimizer:

    def __init__(self, device_params_list, global_params):
        self.device_params_list = device_params_list
        self.global_params = global_params

    # derived properties
    def _num_devices(self):
        return len(self.device_params_list)

    def _total_battery(self):
        return sum(device.current_battery for device in self.device_params_list)

    def _total_train_dataset_size(self):
        return sum(device.train_samples for device in self.device_params_list)

    def _total_validation_dataset_size(self):
        return sum(device.validation_samples for device in self.device_params_list)

    def _get_device_params(self, device_id):
        for device in self.device_params_list:
            if device.device_id == device_id:
                return device
        return None

    def _transmission_latency(self):
        if (
            self.global_params.train_global_time is not None
            and self.global_params.last_server_device_id is not None
        ):
            latency = (
                self.global_params.train_global_time
                - self._round_runtime_with_server_no_latency(
                    self.global_params.last_server_device_id
                )
            )
            if latency > 0:
                return latency
        return 0  # latency not known or runtime was overestimated previously

    def _round_runtime_with_server_no_latency(self, server_device_id):
        """
        Computes the runtime of a round with the given server device.
        Params:
            server_device_id: the id of the server device
        Returns:
            the runtime of a round with the given server device
        Notes:
            Does not consider any transmission latency and may underestimate the runtime
        """
        total_time = 0
        for device in self.device_params_list:
            if device.device_id == server_device_id:
                # server processes all data with its own speed on server model (fw + bw)
                total_time += device.comp_latency_factor * (
                    (
                        self._total_train_dataset_size()
                        + self._total_validation_dataset_size()
                    )
                    * self.global_params.server_norm_fw_time
                    + self._total_train_dataset_size()
                    * self.global_params.server_norm_bw_time
                )  # backprop only on train data
            # client time (for every device including server device)
            total_time += (
                (device.train_samples + device.validation_samples)
                * self.global_params.client_norm_fw_time
                + device.train_samples * self.global_params.client_norm_bw_time
            ) * device.comp_latency_factor
        return total_time

    def round_runtime_with_server(self, server_device_id):
        """
        Computes the runtime of a round with the given server device.
        Params:
            server_device_id: the id of the server device
        Returns:
            the runtime of a round with the given server device
        Notes:
        """
        return (
            self._round_runtime_with_server_no_latency(server_device_id)
            + self._transmission_latency()
        )

    def num_flops_per_round_on_device(self, device_id, server_device_id):
        device = self._get_device_params(device_id)
        total_flops = 0
        # set bw FLOPs to 2 * fw FLOPs if not provided
        if self.global_params.client_bw_flops and self.global_params.server_bw_flops:
            client_bw_flops = self.global_params.client_bw_flops
            server_bw_flops = self.global_params.server_bw_flops
        else:
            client_bw_flops = (
                2 * self.global_params.client_fw_flops
            )  # by default bw FLOPs = 2*fw FLOPs
            server_bw_flops = 2 * self.global_params.server_fw_flops
        client_fw_flops = self.global_params.client_fw_flops
        server_fw_flops = self.global_params.server_fw_flops
        if device_id == server_device_id:
            total_flops += (
                server_fw_flops + server_bw_flops
            ) * self._total_train_dataset_size()
            total_flops += server_fw_flops * self._total_validation_dataset_size()
        total_flops += (client_fw_flops + client_bw_flops) * device.train_samples
        total_flops += client_fw_flops * device.validation_samples
        return total_flops

    def num_bytes_sent_per_round_on_device(self, device_id, server_device_id):
        device = self._get_device_params(device_id)
        total_bytes = 0
        if device_id == server_device_id:
            total_bytes += (
                self.global_params.gradient_size * self._total_train_dataset_size()
            )
            total_bytes += self.global_params.client_weights_size * (
                self._num_devices() - 1
            )  # setting client weights before training
            # return server weights and optimizer state in the end
            total_bytes += self.global_params.server_weights_size
            total_bytes += self.global_params.optimizer_state_size
            # exclude server device's own gradients
            total_bytes -= self.global_params.gradient_size * device.train_samples
        else:
            total_bytes += self.global_params.label_size * (
                device.train_samples + device.validation_samples
            )
            total_bytes += self.global_params.smashed_data_size * (
                device.train_samples + device.validation_samples
            )
            total_bytes += (
                self.global_params.client_weights_size
            )  # clients return weights to server
        return total_bytes

    def num_bytes_received_per_round_on_device(self, device_id, server_device_id):
        device = self._get_device_params(device_id)
        total_bytes = 0
        if device_id == server_device_id:
            total_bytes += self.global_params.label_size * (
                self._total_train_dataset_size() + self._total_validation_dataset_size()
            )
            total_bytes += self.global_params.smashed_data_size * (
                self._total_train_dataset_size() + self._total_validation_dataset_size()
            )
            total_bytes += self.global_params.client_weights_size * (
                self._num_devices() - 1
            )  # clients return their weights to server
            # server weights and optimizer state set once in the beginning
            total_bytes += self.global_params.server_weights_size
            total_bytes += self.global_params.optimizer_state_size
            # exclude server device's own data and labels
            total_bytes -= self.global_params.label_size * (
                device.train_samples + device.validation_samples
            )
            total_bytes -= self.global_params.smashed_data_size * (
                device.train_samples + device.validation_samples
            )
        else:
            total_bytes += self.global_params.gradient_size * device.train_samples
            total_bytes += (
                self.global_params.client_weights_size
            )  # client weights set in the beginning of the training
        return total_bytes

    def energy_per_round_on_device(self, device_id, server_device_id):
        total_energy = 0
        total_energy += (
            self.num_flops_per_round_on_device(device_id, server_device_id)
            * self.global_params.cost_per_flop
        )
        total_energy += (
            self.num_bytes_sent_per_round_on_device(device_id, server_device_id)
            * self.global_params.cost_per_byte_sent
        )
        total_energy += (
            self.num_bytes_received_per_round_on_device(device_id, server_device_id)
            * self.global_params.cost_per_byte_received
        )
        total_energy += (
            self.round_runtime_with_server(server_device_id)
            * self.global_params.cost_per_sec
        )
        return total_energy

    def max_rounds_upper_bound(self):
        max_rounds = []
        for server_device in self.device_params_list:
            max_rounds_with_server = []
            for device in self.device_params_list:
                max_rounds_with_server += [
                    math.floor(
                        device.current_battery
                        / self.energy_per_round_on_device(
                            device.device_id, server_device.device_id
                        )
                    )
                ]
            max_rounds.append(max(max_rounds_with_server))
        return max(max_rounds)

    def optimize(self):
        solver = pywraplp.Solver.CreateSolver("SAT")
        # variables
        # rounds_as_server[i] = number of rounds device i is server
        rounds_as_server = {}
        for device in self.device_params_list:
            rounds_as_server[device.device_id] = solver.IntVar(
                0, self.max_rounds_upper_bound(), f"rounds_{device.device_id}"
            )
        # constraints
        energy = np.array(
            [
                self.energy_per_round_on_device(
                    device.device_id, server_device.device_id
                )
                for device in self.device_params_list
                for server_device in self.device_params_list
            ]
        )
        # energy[i][j] = energy per round for device i with server j
        energy = energy.reshape(
            (len(self.device_params_list), len(self.device_params_list))
        )
        # for each device, sum up the energy over all rounds with each device's frequency being the server device
        for i in range(len(self.device_params_list)):
            solver.Add(
                solver.Sum(
                    [
                        rounds_as_server[self.device_params_list[j].device_id]
                        * energy[i][j]
                        for j in range(len(self.device_params_list))
                    ]
                )
                <= self.device_params_list[i].current_battery
            )
        # objective: maximize the total number of rounds, i.e. the sum of times each device is server
        solver.Maximize(solver.Sum(rounds_as_server.values()))

        print(
            solver.ExportModelAsLpFormat(False).replace("\\", "").replace(",_", ","),
            sep="\n",
        )
        status = solver.Solve()

        return {
            device.device_id: rounds_as_server[device.device_id].solution_value()
            for device in self.device_params_list
        }, status


class EnergySimulator:

    def __init__(self, device_params_list, global_params):
        """
        Simulator class to simulate the different server choice algorithms. Allows to simulate the algorithms for different
        scenarios instead of running it on the actual devices. This is useful for testing and debugging but requires actual values though.
        Args:
            device_params_list: list of DeviceParams objects
            global_params: GlobalParams object
        """
        self.device_params_list = device_params_list
        self.global_params = global_params
        self.server_choice_optimizer = ServerChoiceOptimizer(
            device_params_list, global_params
        )

    def _simulate_selection(self, selection_callback=None):
        def __all_devices_alive__(device_battery_list):
            return all(battery > 0 for battery in device_battery_list)

        energy = np.array(
            [
                self.server_choice_optimizer.energy_per_round_on_device(
                    device.device_id, server_device.device_id
                )
                for device in self.device_params_list
                for server_device in self.device_params_list
            ]
        )
        # energy[i][j] = energy per round for device i with server j
        energy = energy.reshape(
            (len(self.device_params_list), len(self.device_params_list))
        )

        device_batteries = [
            device.current_battery for device in self.device_params_list
        ]
        all_devices_alive = True
        server_selection_schedule = []
        num_rounds = 0
        while all_devices_alive:
            server_device_idx = selection_callback(
                device_battery_list=device_batteries, num_rounds=num_rounds
            )
            new_batteries = device_batteries.copy()
            for idx, device in enumerate(self.device_params_list):
                new_batteries[idx] = new_batteries[idx] - energy[idx][server_device_idx]
            all_devices_alive = __all_devices_alive__(new_batteries)
            if all_devices_alive:
                device_batteries = new_batteries
                num_rounds += 1
                server_selection_schedule.append(
                    self.device_params_list[server_device_idx].device_id
                )
            else:
                break
        return num_rounds, server_selection_schedule, device_batteries

    def simulate_greedy_selection(self):
        """
        Simulates the greedy server choice algorithm.
        Returns:
            num_rounds: number of rounds until the first device runs out of battery
            server_selection_schedule: list of server device ids for each round
            device_batteries: list of battery levels for each device after the last successful round
        """

        def __get_device_with_max_battery__(device_battery_list, **kwargs):
            return max(
                range(len(device_battery_list)), key=device_battery_list.__getitem__
            )

        return self._simulate_selection(__get_device_with_max_battery__)

    def simulate_sequential_selection(self):
        """
        Simulates the sequential server choice algorithm.
        Returns:
            num_rounds: number of rounds until the first device runs out of battery
            server_selection_schedule: list of server device ids for each round
            device_batteries: list of battery levels for each device after the last successful round
        """

        def __sequential_selection__(device_battery_list, num_rounds):
            return num_rounds % len(device_battery_list)

        return self._simulate_selection(__sequential_selection__)

    def simulate_smart_selection(self):
        """
        Simulates the smart server choice algorithm.
        Returns:
            num_rounds: number of rounds until the first device runs out of battery
            solution: the solution computed by the optimizer
            device_batteries: list of battery levels for each device after in the end
        """
        solution, status = self.server_choice_optimizer.optimize()
        return sum(solution.values()), solution, self._remaining_batteries(solution)

    def _remaining_batteries(self, solution):
        device_batteries = [
            device.current_battery for device in self.device_params_list
        ]
        for server_device_id, rounds in solution.items():
            for idx, device in enumerate(self.device_params_list):
                device_batteries[idx] -= (
                    self.server_choice_optimizer.energy_per_round_on_device(
                        device.device_id, server_device_id
                    )
                    * rounds
                )
        return device_batteries

    def _fl_round_time(self):
        train_times = []
        for device in self.device_params_list:
            model_bw_time = (
                self.global_params.client_norm_bw_time
                + self.global_params.server_norm_bw_time
            )
            model_fw_time = (
                self.global_params.client_norm_fw_time
                + self.global_params.server_norm_fw_time
            )
            total_time = (
                (device.train_samples + device.validation_samples) * model_fw_time
                + device.train_samples * model_bw_time
            ) * device.comp_latency_factor
            train_times.append(total_time)
        return max(train_times)

    def _fl_flops_on_device(self, device_id):
        device = self._get_device_params(device_id)
        total_flops = 0
        total_flops += self.global_params.client_fw_flops * device.train_samples * 3
        total_flops += self.global_params.client_fw_flops * device.validation_samples
        total_flops += self.global_params.server_fw_flops * device.train_samples * 3
        total_flops += self.global_params.server_fw_flops * device.validation_samples
        return total_flops

    def _fl_data_sent_per_device(self):
        return (
            self.global_params.server_weights_size
            + self.global_params.client_weights_size
        )

    def _fl_data_received_per_device(self):
        return (
            self.global_params.server_weights_size
            + self.global_params.client_weights_size
        )

    def _fl_energy_per_round_on_device(self, device_id):
        total_energy = 0
        total_energy += (
            self._fl_flops_on_device(device_id) * self.global_params.cost_per_flop
        )
        total_energy += (
            self._fl_data_sent_per_device() * self.global_params.cost_per_byte_sent
        )
        total_energy += (
            self._fl_data_received_per_device()
            * self.global_params.cost_per_byte_received
        )
        total_energy += self._fl_round_time() * self.global_params.cost_per_sec
        return total_energy

    def simulate_federated_learning(self):
        """
        Simulates the federated learning algorithm.
        Returns:
            num_rounds: number of rounds until the first device runs out of battery
            device_batteries: list of battery levels for each device after the last successful round
        """

        def __all_devices_alive__(device_battery_list):
            return all(battery > 0 for battery in device_battery_list)

        device_batteries = [
            device.current_battery for device in self.device_params_list
        ]
        all_devices_alive = True
        energy = [
            self._fl_energy_per_round_on_device(device.device_id)
            for device in self.device_params_list
        ]
        num_rounds = 0
        while all_devices_alive:
            new_batteries = device_batteries.copy()
            for idx, device in enumerate(self.device_params_list):
                new_batteries[idx] = new_batteries[idx] - energy[idx]
            all_devices_alive = __all_devices_alive__(new_batteries)
            if all_devices_alive:
                num_rounds += 1
                device_batteries = new_batteries
        return num_rounds, device_batteries

    def _get_device_params(self, device_id):
        for device in self.device_params_list:
            if device.device_id == device_id:
                return device
        return None


def run_grid_search(
    device_params_list,
    global_params,
    batteries=None,
    latencies=None,
    partitions=None,
    cost_per_sec=None,
    cost_per_byte_sent=None,
    cost_per_byte_received=None,
    cost_per_flop=None,
):
    """
    Runs a grid search for the given device parameters and global parameters.
    Params:
        device_params_list: list of DeviceParams objects
        global_params: GlobalParams object
        **kwargs: optional parameters for the grid search: should be a list of lists for device params and a list for costs
        If e.g. cost_per_second is provided, the grid search will be run for all values in the list overriding existing values in the global params object.
        If no cost_per_second is provided, the grid search will use the value from the global params object.
    Returns:
        list of dicts containing the results for each combination of parameters
    """
    if batteries is None:
        batteries = [[device.current_battery for device in device_params_list]]
    if latencies is None:
        latencies = [[device.comp_latency_factor for device in device_params_list]]
    total_train_samples = sum(device.train_samples for device in device_params_list)
    total_val_samples = sum(device.validation_samples for device in device_params_list)
    if partitions is None:
        partitions = [
            [
                device.train_samples / total_train_samples
                for device in device_params_list
            ]
        ]
    if cost_per_sec is None:
        cost_per_sec = [global_params.cost_per_sec]
    if cost_per_byte_sent is None:
        cost_per_byte_sent = [global_params.cost_per_byte_sent]
    if cost_per_byte_received is None:
        cost_per_byte_received = [global_params.cost_per_byte_received]
    if cost_per_flop is None:
        cost_per_flop = [global_params.cost_per_flop]
    results = []
    for battery in batteries:
        for latency in latencies:
            for partition in partitions:
                for cost_sec in cost_per_sec:
                    for cost_sent in cost_per_byte_sent:
                        # for cost_received in cost_per_byte_received:
                        cost_received = cost_sent
                        for cost_flop in cost_per_flop:
                            global_params.cost_per_sec = cost_sec
                            global_params.cost_per_byte_sent = cost_sent
                            global_params.cost_per_byte_received = cost_received
                            global_params.cost_per_flop = cost_flop
                            for idx, device in enumerate(device_params_list):
                                device.current_battery = battery[idx]
                                device.comp_latency_factor = latency[idx]
                                device.train_samples = (
                                    partition[idx] * total_train_samples
                                )
                                device.validation_samples = (
                                    partition[idx] * total_val_samples
                                )
                            energy_simulator = EnergySimulator(
                                device_params_list, global_params
                            )
                            num_rounds_smart, _, _ = (
                                energy_simulator.simulate_smart_selection()
                            )
                            num_rounds_greedy, _, _ = (
                                energy_simulator.simulate_greedy_selection()
                            )
                            num_rounds_seq, _, _ = (
                                energy_simulator.simulate_sequential_selection()
                            )
                            num_rounds_fl, _ = (
                                energy_simulator.simulate_federated_learning()
                            )
                            results.append(
                                {
                                    "battery": battery,
                                    "latency": latency,
                                    "partition": partition,
                                    "cost_per_sec": cost_sec,
                                    "cost_per_byte_sent": cost_sent,
                                    "cost_per_byte_received": cost_received,
                                    "cost_per_flop": cost_flop,
                                    "num_rounds_smart": num_rounds_smart,
                                    "num_rounds_seq": num_rounds_seq,
                                    "num_rounds_greedy": num_rounds_greedy,
                                    "num_rounds_fl": num_rounds_fl,
                                }
                            )
    return results


def run_grid_search_with_variable_devices(
    num_devices_list,
    global_params,
    battery_per_device,
    total_train_samples,
    total_val_samples,
    max_latencies=None,
    max_split=None,
    cost_per_sec=None,
    cost_per_byte_sent=None,
    cost_per_byte_received=None,
    cost_per_flop=None,
):
    """
    Runs a grid search for the given device parameters and global parameters.
    Params:
        num_devices: list(number of devices)
        global_params: GlobalParams object
        battery_per_device: list(battery per device)
        total_train_samples: total number of train samples
        total_val_samples: total number of validation samples
        max_latencies: optional list(max latency per device) Sets all devices except the last one to the max latency if provided
        max_split: optional list(max split per device) Sets the largest split for last device if provided and distributes the rest equally among the other devices
        **kwargs: optional parameters for the grid search: should be a list of lists for device params and a list for costs
        If e.g. cost_per_second is provided, the grid search will be run for all values in the list overriding existing values in the global params object.
        If no cost_per_second is provided, the grid search will use the value from the global params object.
    Returns:
        list of dicts containing the results for each combination of parameters
    """
    if max_latencies is None:
        max_latencies = [1.0]
    if cost_per_sec is None:
        cost_per_sec = [global_params.cost_per_sec]
    if cost_per_byte_sent is None:
        cost_per_byte_sent = [global_params.cost_per_byte_sent]
    if cost_per_byte_received is None:
        cost_per_byte_received = [global_params.cost_per_byte_received]
    if cost_per_flop is None:
        cost_per_flop = [global_params.cost_per_flop]
    results = []
    for num_devices in num_devices_list:
        if max_split is None:
            max_split = [1 / num_devices]
        for battery in battery_per_device:
            for latency in max_latencies:
                for partition in max_split:
                    device_params_list = [
                        DeviceParams(
                            device_id=f"d{i}",
                            initial_battery=0,
                            current_battery=battery,
                            train_samples=(
                                partition * total_train_samples
                                if i == num_devices - 1
                                else total_train_samples // num_devices
                            ),
                            validation_samples=(
                                partition * total_val_samples
                                if i == num_devices - 1
                                else total_val_samples // num_devices
                            ),
                            comp_latency_factor=latency if i < num_devices - 1 else 1.0,
                        )
                        for i in range(num_devices)
                    ]
                    for cost_sec in cost_per_sec:
                        for cost_sent in cost_per_byte_sent:
                            for cost_received in cost_per_byte_received:
                                for cost_flop in cost_per_flop:
                                    global_params.cost_per_sec = cost_sec
                                    global_params.cost_per_byte_sent = cost_sent
                                    global_params.cost_per_byte_received = cost_received
                                    global_params.cost_per_flop = cost_flop
                                    energy_simulator = EnergySimulator(
                                        device_params_list, global_params
                                    )
                                    num_rounds_smart, _, _ = (
                                        energy_simulator.simulate_smart_selection()
                                    )
                                    num_rounds_greedy, _, _ = (
                                        energy_simulator.simulate_greedy_selection()
                                    )
                                    num_rounds_fl, _ = (
                                        energy_simulator.simulate_federated_learning()
                                    )
                                    results.append(
                                        {
                                            "num_devices": num_devices,
                                            "battery": battery,
                                            "latency": latency,
                                            "partition": partition,
                                            "cost_per_sec": cost_sec,
                                            "cost_per_byte_sent": cost_sent,
                                            "cost_per_byte_received": cost_received,
                                            "cost_per_flop": cost_flop,
                                            "num_rounds_smart": num_rounds_smart,
                                            "num_rounds_greedy": num_rounds_greedy,
                                            "num_rounds_fl": num_rounds_fl,
                                        }
                                    )
    return results

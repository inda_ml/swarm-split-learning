from typing import Optional, Callable, Sequence

from edml.controllers.base_controller import BaseController
from edml.controllers.scheduler.base import NextServerScheduler
from edml.helpers.types import DeviceBatteryStatus

UpdateBatteryStatusCallback = Callable[[], dict[str, Optional[DeviceBatteryStatus]]]


class MaxBatteryNextServerScheduler(NextServerScheduler):
    """
    This scheduler selects the server with the highest battery level.
    """

    KEY: str = "max_battery"

    def __init__(self):
        super().__init__()

        # We require some kind of callback that allows us to fetch the newest battery levels for all active devices.
        # This is set during initialization.
        self._update_batteries_cb: Optional[UpdateBatteryStatusCallback] = None

    def _initialize(self, controller: BaseController):
        self._update_batteries_cb = controller._get_battery_status

    def _next_server(self, _: Sequence[str], **kwargs) -> Optional[str]:
        battery_status = self._get_active_devices_battery_levels()
        if len(battery_status) == 0:
            return None
        # return the device_id with the highest battery level
        return max(battery_status, key=lambda device_id: battery_status[device_id])

    def _get_active_devices_battery_levels(self) -> dict[str, float]:
        """Returns the current battery levels of active devices only."""
        return {
            key: battery_info.current_capacity
            for key, battery_info in self._update_batteries_cb().items()
            if battery_info is not None
        }

from abc import ABC, abstractmethod
from typing import TYPE_CHECKING, Optional, Sequence

if TYPE_CHECKING:
    from edml.controllers.base_controller import BaseController


class NextServerScheduler(ABC):
    """
    This class can be used to implement a custom scheduler algorithm. Controller implementations can then use them to
    determine the next server to be used for the round.

    Attributes:
        devices: A list of device IDs that can be used for scheduler.
    """

    def __init__(self):
        self.devices: Optional[list[str]] = None

    def initialize(self, controller: "BaseController"):
        """
        Initializes the scheduler with the list of devices that can be used for scheduler.

        Args:
            controller: The controller that is using this scheduler. The instance should not be used outside of this
                        method.

        Notes:
            This method should not be overridden. Instead, the `_initialize` method should be overridden.
        """
        self.devices = controller._get_device_ids()
        self._initialize(controller)

    def _initialize(self, controller: "BaseController"):
        """Custom hook for implementations to initialize themselves."""

    def next_server(self, active_devices: Sequence[str], **kwargs) -> Optional[str]:
        """
        Returns the next active server device. This method only calls the `_next_server` method after verifying the
        list of active devices is not empty.
        For using multiple schedulers interchangeably, pass all required args for all schedulers as kwargs.

        Args:
            active_devices: The list of currently active device IDs. This list should not be empty.
            last_server_device_id: Optional kwarg, the id of the last server device.
            diagnostic_metric_container: Optional kwarg, a DiagnosticMetricResultContainer.

        Returns:
            The next server device ID. In the case that no device is available, `None` is returned.

        Raises:
            ValueError: If the list of active devices is empty.
        """
        if len(active_devices) == 0:
            raise ValueError("list cannot be empty")
        return self._next_server(active_devices, **kwargs)

    @abstractmethod
    def _next_server(self, active_devices: Sequence[str], **kwargs) -> Optional[str]:
        """
        Custom hook to return the next active device. Concrete implementations that need more arguments,
        should define these as additional keyword args, so the base signature does not have to change.
        However, when relying on polymorphism, the next_server template should receive all keyword args for the
        different schedulers. Also, the concrete implementations should define **kwargs in the signature,
        so it does not have to change when new kwargs are added.
        """
        raise NotImplementedError()

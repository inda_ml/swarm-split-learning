from random import choice
from typing import Sequence

from edml.controllers.scheduler.base import NextServerScheduler


class RandomNextServerScheduler(NextServerScheduler):
    """
    A scheduler that always returns a random server ID from the list of active devices. To seed the random number, use
    `random.seed()` before calling this scheduler.
    """

    KEY: str = "random"

    def _next_server(self, active_devices: Sequence[str], **kwargs) -> str:
        return choice(active_devices)

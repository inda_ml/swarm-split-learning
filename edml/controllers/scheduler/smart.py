from typing import Optional, Sequence

from edml.controllers.scheduler.base import NextServerScheduler
from edml.controllers.strategy_optimization import (
    GlobalParams,
    ServerChoiceOptimizer,
    DeviceParams,
)
from edml.helpers.metrics import (
    DiagnosticMetricResultContainer,
    compute_metrics_for_optimization,
)


class SmartNextServerScheduler(NextServerScheduler):
    """
    This scheduler optimizes the server device selection so that the number of rounds with all devices participating is maximized.
    Therefore, training metrics are needed, so in the first round, the device is chosen by the specified fallback scheduler.
    Afterward, the optimal selection schedule is computed. If all devices have been picked according to the schedule,
    i.e. one device will run out of battery in the next round, again the server device is chosen by the fallback.
    """

    KEY: str = "smart"

    def __init__(self, fallback_scheduler: NextServerScheduler):
        super().__init__()
        self.fallback_scheduler = fallback_scheduler
        self.selection_schedule = None

    def _initialize(self, controller: "BaseController"):
        self.cfg = controller.cfg
        self._update_batteries_cb = controller._get_battery_status
        self._data_model_cb = (
            controller._get_active_devices_dataset_sizes_and_model_flops
        )
        self.fallback_scheduler.initialize(controller=controller)

    def _next_server(
        self,
        active_devices: Sequence[str],
        last_server_device_id=None,
        diagnostic_metric_container: Optional[DiagnosticMetricResultContainer] = None,
        **kwargs,
    ) -> str:
        if diagnostic_metric_container is None:
            return self.fallback_scheduler.next_server(active_devices)
        else:
            if self.selection_schedule is None or len(self.selection_schedule) == 0:
                self.selection_schedule = self._get_selection_schedule(
                    diagnostic_metric_container, last_server_device_id
                )
                print(f"server device schedule: {self.selection_schedule}")
            try:
                return self.selection_schedule.pop(0)
            except IndexError:  # no more devices left in schedule
                return self.fallback_scheduler.next_server(
                    active_devices,
                    diagnostic_metric_container=diagnostic_metric_container,
                    kwargs=kwargs,
                )

    def _get_selection_schedule(
        self, diagnostic_metric_container, last_server_device_id=None
    ):
        device_params_list = []
        device_battery_levels = self._update_batteries_cb()
        # get num samples and flops per device
        dataset_sizes, model_flops = self._data_model_cb()
        try:
            optimization_metrics = compute_metrics_for_optimization(
                diagnostic_metric_container,
                dataset_sizes,
                self.cfg.experiment.batch_size,
            )
        except (
            KeyError
        ):  # if some metrics are not available e.g. because a device ran out of battery
            return []  # return empty schedule
        for device_id, battery_level in device_battery_levels.items():
            device_params = DeviceParams(
                device_id=device_id,
                initial_battery=battery_level.initial_capacity,
                current_battery=battery_level.current_capacity,
                train_samples=dataset_sizes[device_id][0],
                validation_samples=dataset_sizes[device_id][1],
                comp_latency_factor=optimization_metrics["comp_latency_factor"][
                    device_id
                ],
            )
            device_params_list.append(device_params)
        global_params = GlobalParams()
        global_params.fill_values_from_config(self.cfg)
        global_params.client_fw_flops = model_flops["client"]["FW"]
        global_params.server_fw_flops = model_flops["server"]["FW"]
        global_params.client_bw_flops = model_flops["client"]["BW"]
        global_params.server_bw_flops = model_flops["server"]["BW"]
        global_params.client_norm_fw_time = optimization_metrics["client_norm_fw_time"]
        global_params.client_norm_bw_time = optimization_metrics["client_norm_bw_time"]
        global_params.server_norm_fw_time = optimization_metrics["server_norm_fw_time"]
        global_params.server_norm_bw_time = optimization_metrics["server_norm_bw_time"]
        global_params.gradient_size = optimization_metrics["gradient_size"]
        global_params.label_size = optimization_metrics["label_size"]
        global_params.smashed_data_size = optimization_metrics["smashed_data_size"]
        global_params.client_weights_size = optimization_metrics["client_weight_size"]
        global_params.server_weights_size = optimization_metrics["server_weight_size"]
        global_params.optimizer_state_size = optimization_metrics[
            "optimizer_state_size"
        ]
        if "train_global_time" in optimization_metrics.keys():
            global_params.train_global_time = optimization_metrics["train_global_time"]
        global_params.last_server_device_id = last_server_device_id
        print(f"global params: {vars(global_params)}")
        print(f"device params: {[vars(device) for device in device_params_list]}")
        server_choice_optimizer = ServerChoiceOptimizer(
            device_params_list, global_params
        )
        solution, status = server_choice_optimizer.optimize()
        schedule = []
        for device_id in solution.keys():
            schedule += [device_id] * int(solution[device_id])
        return schedule

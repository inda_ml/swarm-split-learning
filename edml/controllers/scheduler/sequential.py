from typing import Optional, Sequence

from edml.controllers.scheduler.base import NextServerScheduler


class SequentialNextServerScheduler(NextServerScheduler):
    """
    A scheduler that always returns the next server by incrementing a wrapping index into the list of active
    devices.

    Args:
        last_server_device_id (optional): The device ID of the last server device used. If not provided, the first
        server device will be the first active device in the list.
    """

    KEY: str = "sequential"

    def __init__(self, last_server_device_id: Optional[str] = None):
        super().__init__()
        self._last_server_device_id = last_server_device_id

    def _next_server(
        self,
        active_devices: Sequence[str],
        last_server_device_id: Optional[str] = None,
        **kwargs
    ) -> str:
        # Special case if we do not have an initial first server. We simply return the first server ID in the list.
        if self._last_server_device_id is None:
            next_server = active_devices[0]
            self._last_server_device_id = next_server
            return next_server

        # Find the index of the last server in the list of all devices. Create a temporary list of devices with all the
        # devices after the last server and all the devices before the last server.
        # The next server is the first device in the temporary list that is also in the list of active devices.
        last_server_index = self.devices.index(self._last_server_device_id)
        remaining_devices = (
            self.devices[last_server_index + 1 :]
            + self.devices[: last_server_index + 1]
        )
        next_server = next(
            device_id for device_id in remaining_devices if device_id in active_devices
        )

        # Update the last server ID for the next call.
        self._last_server_device_id = next_server
        return next_server

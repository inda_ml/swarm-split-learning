from typing import Optional, Sequence

from edml.controllers.scheduler.base import NextServerScheduler
from edml.helpers.types import KeyedNextServerScheduler


class DispatchingNextServerScheduler(NextServerScheduler):
    def __init__(
        self, schedulers: Sequence[KeyedNextServerScheduler], active_scheduler: str
    ):
        super().__init__()
        self._schedulers = {scheduler.KEY: scheduler for scheduler in schedulers}
        self.active_scheduler = active_scheduler

    def _next_server(self, active_devices: list[str]) -> Optional[str]:
        return self._schedulers[self.active_scheduler].next_server(active_devices)

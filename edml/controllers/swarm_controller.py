from typing import Any

from edml.controllers.adaptive_threshold_mechanism import AdaptiveThresholdFn
from edml.controllers.adaptive_threshold_mechanism.static import (
    StaticAdaptiveThresholdFn,
)
from edml.controllers.base_controller import BaseController
from edml.controllers.scheduler.base import NextServerScheduler
from edml.helpers.config_helpers import get_device_index_by_id


class SwarmController(BaseController):

    def __init__(
        self,
        cfg,
        scheduler: NextServerScheduler,
        adaptive_threshold_fn: AdaptiveThresholdFn = StaticAdaptiveThresholdFn(0.0),
    ):
        super().__init__(cfg)
        scheduler.initialize(self)
        self._next_server_scheduler = scheduler
        self._adaptive_threshold_fn = adaptive_threshold_fn

    def _train(self):
        client_weights = None
        server_weights = None
        server_device_id = None
        diagnostic_metric_container = None
        optimizer_state = None
        for i in range(self.cfg.experiment.max_rounds):
            print(f"Round {i}")
            self._update_devices_battery_status()
            server_device_id = self._select_server_device(
                last_server_device_id=server_device_id,
                diagnostic_metric_container=diagnostic_metric_container,
            )

            # break if no active devices or only server device left
            if server_device_id is None or self._devices_empty_or_only_server_left(
                server_device_id
            ):
                print("No active client devices left.")
                break

            (
                client_weights,
                server_weights,
                metrics,
                optimizer_state,
                diagnostic_metric_container,
            ) = self._swarm_train_round(
                client_weights,
                server_weights,
                server_device_id,
                round_no=i,
                optimizer_state=optimizer_state,
            )

            self._refresh_active_devices()
            self.logger.log(
                {"remaining_devices": {"devices": len(self.active_devices), "round": i}}
            )
            self.logger.log(
                {
                    "server_device": {
                        "device": get_device_index_by_id(self.cfg, server_device_id)
                    },
                    "round": i,
                }
            )  # log the server device index for convenience

            if metrics is not None:
                self._aggregate_and_log_metrics(metrics, i)

                early_stop = self.early_stopping(metrics, i)
                if early_stop:
                    break

                self._save_weights(client_weights, server_weights, i)

    def _swarm_train_round(
        self,
        client_weights,
        server_weights,
        server_device_id,
        round_no: int = -1,
        optimizer_state: dict[str, Any] = None,
    ):
        self._refresh_active_devices()
        # set latest client weights on first device to train on
        self.request_dispatcher.set_weights_on(
            device_id=self.active_devices[0], state_dict=client_weights, on_client=True
        )
        # set latest server weights on server device
        self.request_dispatcher.set_weights_on(
            device_id=server_device_id, state_dict=server_weights, on_client=False
        )

        adaptive_threshold = self._adaptive_threshold_fn.invoke(round_no)
        self.logger.log({"adaptive-threshold": adaptive_threshold})
        training_response = self.request_dispatcher.train_global_on(
            server_device_id,
            epochs=1,
            round_no=round_no,
            adaptive_threshold_value=adaptive_threshold,
            optimizer_state=optimizer_state,
        )

        if training_response is not False:  # server device unavailable
            return training_response
        else:
            return (
                client_weights,
                server_weights,
                None,
                optimizer_state,
                None,
            )  # return most recent weights and no metrics

    def _select_server_device(
        self, last_server_device_id=None, diagnostic_metric_container=None
    ):
        """Returns the id of the server device for the given round."""
        if len(self.active_devices) == 0:
            return None
        return self._next_server_scheduler.next_server(
            self.active_devices,
            last_server_device_id=last_server_device_id,
            diagnostic_metric_container=diagnostic_metric_container,
        )

    def _get_active_devices_dataset_sizes_and_model_flops(self):
        """Returns the dataset sizes and model flops of active devices only."""
        dataset_sizes = {}
        model_flops = {}
        client_fw_flop_list = []
        server_fw_flop_list = []
        client_bw_flop_list = []
        server_bw_flop_list = []
        for device_id in self.active_devices:
            (
                train_samples,
                val_samples,
                client_fw_flops,
                server_fw_flops,
                client_bw_flops,
                server_bw_flops,
            ) = self.request_dispatcher.get_dataset_model_info_on(device_id)
            dataset_sizes[device_id] = (train_samples, val_samples)
            client_fw_flop_list.append(client_fw_flops)
            server_fw_flop_list.append(server_fw_flops)
            client_bw_flop_list.append(client_bw_flops)
            server_bw_flop_list.append(server_bw_flops)
        # avoid that flops are taken from a device that wasn't used for training and thus has no flops
        # apart from that, FLOPs should be the same everywhere
        model_flops["client"] = {
            "FW": max(client_fw_flop_list),
            "BW": max(client_bw_flop_list),
        }
        model_flops["server"] = {
            "FW": max(server_fw_flop_list),
            "BW": max(server_bw_flop_list),
        }
        return dataset_sizes, model_flops

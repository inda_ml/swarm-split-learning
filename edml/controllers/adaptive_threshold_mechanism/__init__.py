from abc import ABC, abstractmethod


class AdaptiveThresholdFn(ABC):
    """A function that returns the adaptive threshold value based on the current round."""

    @abstractmethod
    def invoke(self, round_no: int) -> float:
        """Return the adaptive threshold value for the given round number."""

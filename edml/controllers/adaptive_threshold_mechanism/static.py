from edml.controllers.adaptive_threshold_mechanism import AdaptiveThresholdFn


class StaticAdaptiveThresholdFn(AdaptiveThresholdFn):
    def __init__(self, threshold: float):
        super().__init__()
        self._threshold = threshold

    def invoke(self, round_no: int) -> float:
        return self._threshold

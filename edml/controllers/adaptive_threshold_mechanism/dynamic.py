import numpy as np

from edml.controllers.adaptive_threshold_mechanism import AdaptiveThresholdFn


class LogarithmicDecayAdaptiveThresholdFn(AdaptiveThresholdFn):

    def __init__(
        self, starting_value: float, approach_value: float, decay_rate: float = 1.0
    ):
        super().__init__()
        self._start = starting_value
        self._end = approach_value
        self._decay_rate = decay_rate

    def invoke(self, round_no: int):
        return self._end + (self._start - self._end) * np.exp(
            -self._decay_rate * round_no
        )

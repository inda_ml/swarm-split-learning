import threading
import time
from typing import Final

from edml.helpers.units import MEGA_FACTOR


class Battery:
    """
    A battery used to simulate energy consumption for participating devices.

    Batteries are capable of tracking energy consumption for network data usage
    and training energy consumption based on flop count.
    """

    def __init__(
        self,
        capacity: int,
        deduction_per_second: float = 1.0,
        deduction_per_mflop: float = 0.1,
        deduction_per_mbyte_received: float = 0,
        deduction_per_mbyte_sent: float = 0,
    ):
        """
        Initializes a new battery with the given deduction rates.

        Args:
            capacity (int): the total battery capacity.
            deduction_per_second (float): the deduction rate per second. Defaults to 1.0.
            deduction_per_mflop (float): the deduction rate per mega-flop. Defaults to 0.1.
            deduction_per_mbyte_received (float): The deduction rate per megabyte of received network data.
                                                  Defaults to 0.
            deduction_per_mbyte_sent (float): the deduction rate per megabyte of sent network data. Defaults to 0.
        """

        # Batteries can be in a state where they have not been "started". Meaning
        # that they do not deduce any energy consumption yet.
        # To put batteries into an active state, you have to call `start_experiment`.
        self.__running__: bool = False

        self.__capacity__: int = capacity
        self.__initial_capacity__: Final[int] = capacity
        self.__last_update__: float = time.time()
        self.__deduction_per_second__: float = deduction_per_second
        self.__deduction_per_mflop__: float = deduction_per_mflop
        self.__deduction_per_mbyte_received__: float = deduction_per_mbyte_received
        self.__deduction_per_mbyte_sent__: float = deduction_per_mbyte_sent
        self.__lock__ = threading.Lock()

    def update_flops(self, flops: int):
        """
        Decreases the battery capacity by the given amount of flops.

        Args:
            flops (int): amount of flops to decrease battery capacity.

        Raises:
            BatteryEmptyException: if the energy deduction is less or equal to
            the remaining battery capacity.
        """
        self.__decrease_by__(self.__deduction_per_mflop__ * flops / MEGA_FACTOR)

    def update_time(self):
        """
        Decreases the battery capacity based on the amount of time passed since the
        last method call of `update_time`.

        Raises:
            BatteryEmptyException: if the energy deduction is less or equal to
            the remaining battery capacity.

        Notes:
            Acquires a lock to prevent race conditions when accessing the battery state.
        """
        now = time.time()
        difference = now - self.__last_update__
        if difference > 0:
            self.__decrease_by__(self.__deduction_per_second__ * difference)
            self.__last_update__ = now

    def update_communication_received(self, size_in_bytes: int):
        """
        Reduces the remaining battery capacity based on the amount of network data received.

        The consumed energy amount is calculated by multiplying the number if received bytes with the
        `deduction_per_mbyte_received` factor.

        Args:
            size_in_bytes (int): the number of received bytes.

        Raises:
            BatteryEmptyException: if the energy deduction is less or equal to
            the remaining battery capacity.

        Notes:
            Acquires a lock to prevent race conditions when accessing the battery state.
        """
        self.__decrease_by__(
            size_in_bytes * self.__deduction_per_mbyte_received__ / MEGA_FACTOR
        )

    def update_communication_sent(self, size_in_bytes: int):
        """
        Reduces the remaining battery capacity based on the amount of network data sent.

        The consumed energy amount is calculated by multiplying the number if received bytes with the
        `deduction_per_mbyte_sent` factor.

        Args:
            size_in_bytes (int): the number of sent bytes.

        Raises:
            BatteryEmptyException: if the energy deduction is less or equal to
            the remaining battery capacity.

        Notes:
            Acquires a lock to prevent race conditions when accessing the battery state.
        """
        self.__decrease_by__(
            size_in_bytes * self.__deduction_per_mbyte_sent__ / MEGA_FACTOR
        )

    def remaining_capacity(self) -> int:
        """Returns the remaining battery capacity."""
        return self.__capacity__

    def initial_capacity(self) -> int:
        """Returns the initial battery capacity."""
        return self.__initial_capacity__

    def is_empty(self) -> bool:
        """
        Returns whether the battery is empty or not.

        Returns:
            bool: `True` if the battery is empty, `False` otherwise.

        Notes:
            Acquires a lock to prevent race conditions when accessing the battery state.
        """
        with self.__lock__:
            return self.__capacity__ <= 0

    def __decrease_by__(self, amount):
        """
        Decreases the battery capacity by the given amount.

        Raises:
            BatteryEmptyException: if the battery is empty.

        Notes:
            Decreases the capacity only if :py:meth:`~edml.core.battery.Battery.start_experiment` has been called.
        """
        with self.__lock__:
            if self.__running__:
                self.__capacity__ -= amount
        if self.is_empty():
            raise BatteryEmptyException("Battery empty")

    def start_experiment(self):
        """
        Initializes the battery and enables time-based capacity depletion.

        Notes:
            This method should be called during the `start_experiment` state of the controller. The method has to be
            called before any method that reduces the battery's capacity. If not, methods that reduce the battery's
            capacity will be a no-op.

        See:
            - :py:meth:`~edml.core.battery.Battery.update_flops`
            - :py:meth:`~edml.core.battery.Battery.update_time`
            - :py:meth:`~edml.core.battery.Battery.update_communication_received`
            - :py:meth:`~edml.core.battery.Battery.update_communication_sent`
        """
        self.__running__ = True


class BatteryEmptyException(Exception):
    """
    This exception is raised when the battery runs empty.

    The exception is caught inside gRPC network dispatchers, allowing us to gracefully
    close the TCP connections.
    """

    pass

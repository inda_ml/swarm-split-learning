import threading
from concurrent import futures

import grpc
from hydra.utils import instantiate
from omegaconf import DictConfig
from torch import nn

from edml.core.battery import Battery
from edml.core.client import DeviceClient
from edml.core.device import NetworkDevice, RPCDeviceServicer
from edml.core.server import DeviceServer
from edml.dataset_utils.mnist.mnist import single_batch_dataloaders
from edml.generated import connection_pb2_grpc
from edml.helpers.config_helpers import get_device_address_by_id, get_device_index_by_id
from edml.helpers.data_partitioning import DataPartitioner
from edml.helpers.interceptors import DeviceServerInterceptor
from edml.helpers.load_dataset import get_dataloaders
from edml.helpers.load_model import get_models
from edml.helpers.logging import SimpleLogger, create_logger
from edml.models.provider.base import ModelProvider


def _start_device_server(
    device_id,
    address,
    client: "DeviceClient",
    server: "DeviceServer",
    logger: "SimpleLogger",
    battery: "Battery",
    devices_info,
    max_threads=10,
    max_message_length=-1,
):
    stop_event = threading.Event()

    device = NetworkDevice(
        device_id=device_id, logger=logger, battery=battery, stop_event=stop_event
    )
    device.set_client(client)
    device.set_server(server)
    device.set_devices(devices_info)

    grpc_server = grpc.server(
        futures.ThreadPoolExecutor(max_workers=max_threads),
        options=[
            ("grpc.max_send_message_length", max_message_length),
            ("grpc.max_receive_message_length", max_message_length),
        ],
        interceptors=[
            DeviceServerInterceptor(
                logger=logger, stop_event=stop_event, battery=battery
            )
        ],
    )
    connection_pb2_grpc.add_DeviceServicer_to_server(
        RPCDeviceServicer(device=device), grpc_server
    )
    grpc_server.add_insecure_port(address)
    grpc_server.start()
    print(f"started server {device_id}")
    stop_event.wait()
    grpc_server.stop(grace=None)  # grace=None to stop the server immediately
    print(f"stopped server {device_id}")


def launch_device(cfg):
    device_idx = get_device_index_by_id(cfg, cfg.own_device_id)
    if cfg.experiment.load_single_batch_for_debugging:
        train, val, test = single_batch_dataloaders(cfg.experiment.batch_size)
    else:
        data_partitioner = None
        if cfg.experiment.partition:
            fractions = None
            if cfg.experiment.fractions:
                fractions = cfg.experiment.fractions
            distribution = None
            if "distribution" in cfg.dataset.keys():
                distribution = cfg.dataset.distribution
            data_partitioner = DataPartitioner(
                device_index=device_idx,
                num_devices=cfg.num_devices,
                seed=cfg.seed.value,
                fractions=fractions,
                distribution=distribution,
            )

        train, val, test = get_dataloaders(
            cfg.dataset.name,
            cfg.experiment.batch_size,
            data_partitioner=data_partitioner,
        )

    client_model, server_model = _get_models(cfg)
    latency_factor = 0.0
    if cfg.experiment.latency is not None:
        latency_factor = cfg.experiment.latency[device_idx]
    client = DeviceClient(
        client_model,
        cfg,
        train_dl=train,
        val_dl=val,
        test_dl=test,
        latency_factor=latency_factor,
    )
    loss_fn = instantiate(cfg.loss_fn)
    server = DeviceServer(
        server_model,
        loss_fn,
        cfg,
        latency_factor=latency_factor,
    )
    battery = Battery(
        cfg.topology.devices[device_idx].battery_capacity,
        cfg.battery.deduction_per_second,
        cfg.battery.deduction_per_mflop,
        cfg.battery.deduction_per_mbyte_received,
        cfg.battery.deduction_per_mbyte_sent,
    )
    logger = create_logger(cfg)
    _start_device_server(
        device_id=cfg.own_device_id,
        address=get_device_address_by_id(cfg.own_device_id, cfg),
        client=client,
        server=server,
        logger=logger,
        battery=battery,
        devices_info=cfg.topology.devices[: cfg.num_devices],
        max_threads=cfg.grpc.max_threads,
        max_message_length=cfg.grpc.max_message_length,
    )


def _get_models(cfg: DictConfig) -> tuple[nn.Module, nn.Module]:
    if "model_provider" in cfg:
        print(f">>> Using model provider: {cfg.model_provider}")
        model_provider: ModelProvider = instantiate(cfg.model_provider)
        return model_provider.models

    return get_models(cfg)

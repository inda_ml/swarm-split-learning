import datastructures_pb2 as _datastructures_pb2
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Mapping as _Mapping, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor

class SetGradientsRequest(_message.Message):
    __slots__ = ["gradients"]
    GRADIENTS_FIELD_NUMBER: _ClassVar[int]
    gradients: _datastructures_pb2.Gradients
    def __init__(self, gradients: _Optional[_Union[_datastructures_pb2.Gradients, _Mapping]] = ...) -> None: ...

class UpdateWeightsRequest(_message.Message):
    __slots__ = ["gradients"]
    GRADIENTS_FIELD_NUMBER: _ClassVar[int]
    gradients: _datastructures_pb2.Gradients
    def __init__(self, gradients: _Optional[_Union[_datastructures_pb2.Gradients, _Mapping]] = ...) -> None: ...

class SingleBatchBackwardRequest(_message.Message):
    __slots__ = ["gradients"]
    GRADIENTS_FIELD_NUMBER: _ClassVar[int]
    gradients: _datastructures_pb2.Gradients
    def __init__(self, gradients: _Optional[_Union[_datastructures_pb2.Gradients, _Mapping]] = ...) -> None: ...

class SingleBatchBackwardResponse(_message.Message):
    __slots__ = ["metrics", "gradients"]
    METRICS_FIELD_NUMBER: _ClassVar[int]
    GRADIENTS_FIELD_NUMBER: _ClassVar[int]
    metrics: _datastructures_pb2.Metrics
    gradients: _datastructures_pb2.Gradients
    def __init__(self, metrics: _Optional[_Union[_datastructures_pb2.Metrics, _Mapping]] = ..., gradients: _Optional[_Union[_datastructures_pb2.Gradients, _Mapping]] = ...) -> None: ...

class SingleBatchTrainingRequest(_message.Message):
    __slots__ = ["batch_index", "round_no"]
    BATCH_INDEX_FIELD_NUMBER: _ClassVar[int]
    ROUND_NO_FIELD_NUMBER: _ClassVar[int]
    batch_index: int
    round_no: int
    def __init__(self, batch_index: _Optional[int] = ..., round_no: _Optional[int] = ...) -> None: ...

class SingleBatchTrainingResponse(_message.Message):
    __slots__ = ["smashed_data", "labels"]
    SMASHED_DATA_FIELD_NUMBER: _ClassVar[int]
    LABELS_FIELD_NUMBER: _ClassVar[int]
    smashed_data: _datastructures_pb2.Activations
    labels: _datastructures_pb2.Labels
    def __init__(self, smashed_data: _Optional[_Union[_datastructures_pb2.Activations, _Mapping]] = ..., labels: _Optional[_Union[_datastructures_pb2.Labels, _Mapping]] = ...) -> None: ...

class TrainGlobalParallelSplitLearningRequest(_message.Message):
    __slots__ = ["round_no", "adaptive_threshold_value", "optimizer_state"]
    ROUND_NO_FIELD_NUMBER: _ClassVar[int]
    ADAPTIVE_THRESHOLD_VALUE_FIELD_NUMBER: _ClassVar[int]
    OPTIMIZER_STATE_FIELD_NUMBER: _ClassVar[int]
    round_no: int
    adaptive_threshold_value: float
    optimizer_state: _datastructures_pb2.StateDict
    def __init__(self, round_no: _Optional[int] = ..., adaptive_threshold_value: _Optional[float] = ..., optimizer_state: _Optional[_Union[_datastructures_pb2.StateDict, _Mapping]] = ...) -> None: ...

class TrainGlobalParallelSplitLearningResponse(_message.Message):
    __slots__ = ["client_weights", "server_weights", "metrics", "optimizer_state", "diagnostic_metrics"]
    CLIENT_WEIGHTS_FIELD_NUMBER: _ClassVar[int]
    SERVER_WEIGHTS_FIELD_NUMBER: _ClassVar[int]
    METRICS_FIELD_NUMBER: _ClassVar[int]
    OPTIMIZER_STATE_FIELD_NUMBER: _ClassVar[int]
    DIAGNOSTIC_METRICS_FIELD_NUMBER: _ClassVar[int]
    client_weights: _datastructures_pb2.Weights
    server_weights: _datastructures_pb2.Weights
    metrics: _datastructures_pb2.Metrics
    optimizer_state: _datastructures_pb2.StateDict
    diagnostic_metrics: _datastructures_pb2.Metrics
    def __init__(self, client_weights: _Optional[_Union[_datastructures_pb2.Weights, _Mapping]] = ..., server_weights: _Optional[_Union[_datastructures_pb2.Weights, _Mapping]] = ..., metrics: _Optional[_Union[_datastructures_pb2.Metrics, _Mapping]] = ..., optimizer_state: _Optional[_Union[_datastructures_pb2.StateDict, _Mapping]] = ..., diagnostic_metrics: _Optional[_Union[_datastructures_pb2.Metrics, _Mapping]] = ...) -> None: ...

class TrainGlobalRequest(_message.Message):
    __slots__ = ["epochs", "round_no", "adaptive_threshold_value", "optimizer_state"]
    EPOCHS_FIELD_NUMBER: _ClassVar[int]
    ROUND_NO_FIELD_NUMBER: _ClassVar[int]
    ADAPTIVE_THRESHOLD_VALUE_FIELD_NUMBER: _ClassVar[int]
    OPTIMIZER_STATE_FIELD_NUMBER: _ClassVar[int]
    epochs: int
    round_no: int
    adaptive_threshold_value: float
    optimizer_state: _datastructures_pb2.StateDict
    def __init__(self, epochs: _Optional[int] = ..., round_no: _Optional[int] = ..., adaptive_threshold_value: _Optional[float] = ..., optimizer_state: _Optional[_Union[_datastructures_pb2.StateDict, _Mapping]] = ...) -> None: ...

class TrainGlobalResponse(_message.Message):
    __slots__ = ["client_weights", "server_weights", "metrics", "optimizer_state", "diagnostic_metrics"]
    CLIENT_WEIGHTS_FIELD_NUMBER: _ClassVar[int]
    SERVER_WEIGHTS_FIELD_NUMBER: _ClassVar[int]
    METRICS_FIELD_NUMBER: _ClassVar[int]
    OPTIMIZER_STATE_FIELD_NUMBER: _ClassVar[int]
    DIAGNOSTIC_METRICS_FIELD_NUMBER: _ClassVar[int]
    client_weights: _datastructures_pb2.Weights
    server_weights: _datastructures_pb2.Weights
    metrics: _datastructures_pb2.Metrics
    optimizer_state: _datastructures_pb2.StateDict
    diagnostic_metrics: _datastructures_pb2.Metrics
    def __init__(self, client_weights: _Optional[_Union[_datastructures_pb2.Weights, _Mapping]] = ..., server_weights: _Optional[_Union[_datastructures_pb2.Weights, _Mapping]] = ..., metrics: _Optional[_Union[_datastructures_pb2.Metrics, _Mapping]] = ..., optimizer_state: _Optional[_Union[_datastructures_pb2.StateDict, _Mapping]] = ..., diagnostic_metrics: _Optional[_Union[_datastructures_pb2.Metrics, _Mapping]] = ...) -> None: ...

class SetWeightsRequest(_message.Message):
    __slots__ = ["weights", "on_client"]
    WEIGHTS_FIELD_NUMBER: _ClassVar[int]
    ON_CLIENT_FIELD_NUMBER: _ClassVar[int]
    weights: _datastructures_pb2.Weights
    on_client: bool
    def __init__(self, weights: _Optional[_Union[_datastructures_pb2.Weights, _Mapping]] = ..., on_client: bool = ...) -> None: ...

class SetWeightsResponse(_message.Message):
    __slots__ = ["diagnostic_metrics"]
    DIAGNOSTIC_METRICS_FIELD_NUMBER: _ClassVar[int]
    diagnostic_metrics: _datastructures_pb2.Metrics
    def __init__(self, diagnostic_metrics: _Optional[_Union[_datastructures_pb2.Metrics, _Mapping]] = ...) -> None: ...

class TrainEpochRequest(_message.Message):
    __slots__ = ["server", "round_no"]
    SERVER_FIELD_NUMBER: _ClassVar[int]
    ROUND_NO_FIELD_NUMBER: _ClassVar[int]
    server: _datastructures_pb2.DeviceInfo
    round_no: int
    def __init__(self, server: _Optional[_Union[_datastructures_pb2.DeviceInfo, _Mapping]] = ..., round_no: _Optional[int] = ...) -> None: ...

class TrainEpochResponse(_message.Message):
    __slots__ = ["weights", "diagnostic_metrics"]
    WEIGHTS_FIELD_NUMBER: _ClassVar[int]
    DIAGNOSTIC_METRICS_FIELD_NUMBER: _ClassVar[int]
    weights: _datastructures_pb2.Weights
    diagnostic_metrics: _datastructures_pb2.Metrics
    def __init__(self, weights: _Optional[_Union[_datastructures_pb2.Weights, _Mapping]] = ..., diagnostic_metrics: _Optional[_Union[_datastructures_pb2.Metrics, _Mapping]] = ...) -> None: ...

class TrainBatchRequest(_message.Message):
    __slots__ = ["smashed_data", "labels"]
    SMASHED_DATA_FIELD_NUMBER: _ClassVar[int]
    LABELS_FIELD_NUMBER: _ClassVar[int]
    smashed_data: _datastructures_pb2.Activations
    labels: _datastructures_pb2.Labels
    def __init__(self, smashed_data: _Optional[_Union[_datastructures_pb2.Activations, _Mapping]] = ..., labels: _Optional[_Union[_datastructures_pb2.Labels, _Mapping]] = ...) -> None: ...

class TrainBatchResponse(_message.Message):
    __slots__ = ["gradients", "diagnostic_metrics", "loss"]
    GRADIENTS_FIELD_NUMBER: _ClassVar[int]
    DIAGNOSTIC_METRICS_FIELD_NUMBER: _ClassVar[int]
    LOSS_FIELD_NUMBER: _ClassVar[int]
    gradients: _datastructures_pb2.Gradients
    diagnostic_metrics: _datastructures_pb2.Metrics
    loss: float
    def __init__(self, gradients: _Optional[_Union[_datastructures_pb2.Gradients, _Mapping]] = ..., diagnostic_metrics: _Optional[_Union[_datastructures_pb2.Metrics, _Mapping]] = ..., loss: _Optional[float] = ...) -> None: ...

class EvalGlobalRequest(_message.Message):
    __slots__ = ["validation", "federated"]
    VALIDATION_FIELD_NUMBER: _ClassVar[int]
    FEDERATED_FIELD_NUMBER: _ClassVar[int]
    validation: bool
    federated: bool
    def __init__(self, validation: bool = ..., federated: bool = ...) -> None: ...

class EvalGlobalResponse(_message.Message):
    __slots__ = ["metrics", "diagnostic_metrics"]
    METRICS_FIELD_NUMBER: _ClassVar[int]
    DIAGNOSTIC_METRICS_FIELD_NUMBER: _ClassVar[int]
    metrics: _datastructures_pb2.Metrics
    diagnostic_metrics: _datastructures_pb2.Metrics
    def __init__(self, metrics: _Optional[_Union[_datastructures_pb2.Metrics, _Mapping]] = ..., diagnostic_metrics: _Optional[_Union[_datastructures_pb2.Metrics, _Mapping]] = ...) -> None: ...

class EvalRequest(_message.Message):
    __slots__ = ["server", "validation"]
    SERVER_FIELD_NUMBER: _ClassVar[int]
    VALIDATION_FIELD_NUMBER: _ClassVar[int]
    server: _datastructures_pb2.DeviceInfo
    validation: bool
    def __init__(self, server: _Optional[_Union[_datastructures_pb2.DeviceInfo, _Mapping]] = ..., validation: bool = ...) -> None: ...

class EvalResponse(_message.Message):
    __slots__ = ["diagnostic_metrics"]
    DIAGNOSTIC_METRICS_FIELD_NUMBER: _ClassVar[int]
    diagnostic_metrics: _datastructures_pb2.Metrics
    def __init__(self, diagnostic_metrics: _Optional[_Union[_datastructures_pb2.Metrics, _Mapping]] = ...) -> None: ...

class EvalBatchRequest(_message.Message):
    __slots__ = ["smashed_data", "labels"]
    SMASHED_DATA_FIELD_NUMBER: _ClassVar[int]
    LABELS_FIELD_NUMBER: _ClassVar[int]
    smashed_data: _datastructures_pb2.Activations
    labels: _datastructures_pb2.Labels
    def __init__(self, smashed_data: _Optional[_Union[_datastructures_pb2.Activations, _Mapping]] = ..., labels: _Optional[_Union[_datastructures_pb2.Labels, _Mapping]] = ...) -> None: ...

class EvalBatchResponse(_message.Message):
    __slots__ = ["metrics", "diagnostic_metrics"]
    METRICS_FIELD_NUMBER: _ClassVar[int]
    DIAGNOSTIC_METRICS_FIELD_NUMBER: _ClassVar[int]
    metrics: _datastructures_pb2.Metrics
    diagnostic_metrics: _datastructures_pb2.Metrics
    def __init__(self, metrics: _Optional[_Union[_datastructures_pb2.Metrics, _Mapping]] = ..., diagnostic_metrics: _Optional[_Union[_datastructures_pb2.Metrics, _Mapping]] = ...) -> None: ...

class FullModelTrainRequest(_message.Message):
    __slots__ = ["round_no"]
    ROUND_NO_FIELD_NUMBER: _ClassVar[int]
    round_no: int
    def __init__(self, round_no: _Optional[int] = ...) -> None: ...

class FullModelTrainResponse(_message.Message):
    __slots__ = ["client_weights", "server_weights", "num_samples", "metrics", "diagnostic_metrics"]
    CLIENT_WEIGHTS_FIELD_NUMBER: _ClassVar[int]
    SERVER_WEIGHTS_FIELD_NUMBER: _ClassVar[int]
    NUM_SAMPLES_FIELD_NUMBER: _ClassVar[int]
    METRICS_FIELD_NUMBER: _ClassVar[int]
    DIAGNOSTIC_METRICS_FIELD_NUMBER: _ClassVar[int]
    client_weights: _datastructures_pb2.Weights
    server_weights: _datastructures_pb2.Weights
    num_samples: int
    metrics: _datastructures_pb2.Metrics
    diagnostic_metrics: _datastructures_pb2.Metrics
    def __init__(self, client_weights: _Optional[_Union[_datastructures_pb2.Weights, _Mapping]] = ..., server_weights: _Optional[_Union[_datastructures_pb2.Weights, _Mapping]] = ..., num_samples: _Optional[int] = ..., metrics: _Optional[_Union[_datastructures_pb2.Metrics, _Mapping]] = ..., diagnostic_metrics: _Optional[_Union[_datastructures_pb2.Metrics, _Mapping]] = ...) -> None: ...

class StartExperimentRequest(_message.Message):
    __slots__ = []
    def __init__(self) -> None: ...

class StartExperimentResponse(_message.Message):
    __slots__ = ["diagnostic_metrics"]
    DIAGNOSTIC_METRICS_FIELD_NUMBER: _ClassVar[int]
    diagnostic_metrics: _datastructures_pb2.Metrics
    def __init__(self, diagnostic_metrics: _Optional[_Union[_datastructures_pb2.Metrics, _Mapping]] = ...) -> None: ...

class EndExperimentRequest(_message.Message):
    __slots__ = []
    def __init__(self) -> None: ...

class EndExperimentResponse(_message.Message):
    __slots__ = ["diagnostic_metrics"]
    DIAGNOSTIC_METRICS_FIELD_NUMBER: _ClassVar[int]
    diagnostic_metrics: _datastructures_pb2.Metrics
    def __init__(self, diagnostic_metrics: _Optional[_Union[_datastructures_pb2.Metrics, _Mapping]] = ...) -> None: ...

class BatteryStatusRequest(_message.Message):
    __slots__ = []
    def __init__(self) -> None: ...

class BatteryStatusResponse(_message.Message):
    __slots__ = ["status", "diagnostic_metrics"]
    STATUS_FIELD_NUMBER: _ClassVar[int]
    DIAGNOSTIC_METRICS_FIELD_NUMBER: _ClassVar[int]
    status: _datastructures_pb2.BatteryStatus
    diagnostic_metrics: _datastructures_pb2.Metrics
    def __init__(self, status: _Optional[_Union[_datastructures_pb2.BatteryStatus, _Mapping]] = ..., diagnostic_metrics: _Optional[_Union[_datastructures_pb2.Metrics, _Mapping]] = ...) -> None: ...

class DatasetModelInfoRequest(_message.Message):
    __slots__ = []
    def __init__(self) -> None: ...

class DatasetModelInfoResponse(_message.Message):
    __slots__ = ["train_samples", "validation_samples", "client_fw_flops", "server_fw_flops", "client_bw_flops", "server_bw_flops", "diagnostic_metrics"]
    TRAIN_SAMPLES_FIELD_NUMBER: _ClassVar[int]
    VALIDATION_SAMPLES_FIELD_NUMBER: _ClassVar[int]
    CLIENT_FW_FLOPS_FIELD_NUMBER: _ClassVar[int]
    SERVER_FW_FLOPS_FIELD_NUMBER: _ClassVar[int]
    CLIENT_BW_FLOPS_FIELD_NUMBER: _ClassVar[int]
    SERVER_BW_FLOPS_FIELD_NUMBER: _ClassVar[int]
    DIAGNOSTIC_METRICS_FIELD_NUMBER: _ClassVar[int]
    train_samples: int
    validation_samples: int
    client_fw_flops: int
    server_fw_flops: int
    client_bw_flops: int
    server_bw_flops: int
    diagnostic_metrics: _datastructures_pb2.Metrics
    def __init__(self, train_samples: _Optional[int] = ..., validation_samples: _Optional[int] = ..., client_fw_flops: _Optional[int] = ..., server_fw_flops: _Optional[int] = ..., client_bw_flops: _Optional[int] = ..., server_bw_flops: _Optional[int] = ..., diagnostic_metrics: _Optional[_Union[_datastructures_pb2.Metrics, _Mapping]] = ...) -> None: ...

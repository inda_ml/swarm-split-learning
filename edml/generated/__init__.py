import sys
import os

# Add the parent directory to the path to make the import statements inside the generated grpc code work
sys.path.append(os.path.join(os.path.dirname(__file__)))

from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Mapping as _Mapping, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor

class Tensor(_message.Message):
    __slots__ = ["serialized"]
    SERIALIZED_FIELD_NUMBER: _ClassVar[int]
    serialized: bytes
    def __init__(self, serialized: _Optional[bytes] = ...) -> None: ...

class StateDict(_message.Message):
    __slots__ = ["serialized"]
    SERIALIZED_FIELD_NUMBER: _ClassVar[int]
    serialized: bytes
    def __init__(self, serialized: _Optional[bytes] = ...) -> None: ...

class Weights(_message.Message):
    __slots__ = ["weights"]
    WEIGHTS_FIELD_NUMBER: _ClassVar[int]
    weights: StateDict
    def __init__(self, weights: _Optional[_Union[StateDict, _Mapping]] = ...) -> None: ...

class Labels(_message.Message):
    __slots__ = ["labels"]
    LABELS_FIELD_NUMBER: _ClassVar[int]
    labels: Tensor
    def __init__(self, labels: _Optional[_Union[Tensor, _Mapping]] = ...) -> None: ...

class Activations(_message.Message):
    __slots__ = ["activations"]
    ACTIVATIONS_FIELD_NUMBER: _ClassVar[int]
    activations: Tensor
    def __init__(self, activations: _Optional[_Union[Tensor, _Mapping]] = ...) -> None: ...

class Gradients(_message.Message):
    __slots__ = ["gradients"]
    GRADIENTS_FIELD_NUMBER: _ClassVar[int]
    gradients: Tensor
    def __init__(self, gradients: _Optional[_Union[Tensor, _Mapping]] = ...) -> None: ...

class Metrics(_message.Message):
    __slots__ = ["metrics"]
    METRICS_FIELD_NUMBER: _ClassVar[int]
    metrics: bytes
    def __init__(self, metrics: _Optional[bytes] = ...) -> None: ...

class Predictions(_message.Message):
    __slots__ = ["predictions"]
    PREDICTIONS_FIELD_NUMBER: _ClassVar[int]
    predictions: Tensor
    def __init__(self, predictions: _Optional[_Union[Tensor, _Mapping]] = ...) -> None: ...

class Empty(_message.Message):
    __slots__ = []
    def __init__(self) -> None: ...

class DeviceInfo(_message.Message):
    __slots__ = ["device_id", "address"]
    DEVICE_ID_FIELD_NUMBER: _ClassVar[int]
    ADDRESS_FIELD_NUMBER: _ClassVar[int]
    device_id: str
    address: str
    def __init__(self, device_id: _Optional[str] = ..., address: _Optional[str] = ...) -> None: ...

class BatteryStatus(_message.Message):
    __slots__ = ["initial_battery_level", "current_battery_level"]
    INITIAL_BATTERY_LEVEL_FIELD_NUMBER: _ClassVar[int]
    CURRENT_BATTERY_LEVEL_FIELD_NUMBER: _ClassVar[int]
    initial_battery_level: float
    current_battery_level: float
    def __init__(self, initial_battery_level: _Optional[float] = ..., current_battery_level: _Optional[float] = ...) -> None: ...

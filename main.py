import random

import hydra
import numpy.random
import torch
from omegaconf import DictConfig

from edml.controllers.test_controller import TestController
from edml.core.start_device import launch_device
from edml.helpers.config_helpers import preprocess_config, instantiate_controller
from multiprocessing import Process


@hydra.main(version_base=None, config_name="default", config_path="config")
def main(cfg):
    """Starts either a device with a gRPC server or a controller depending on the config."""
    preprocess_config(cfg)

    # Fix the seed for reproducibility. Every client also has to do the same, since multiprocessing does
    # not share the same random state for all libraries we use.
    _make_deterministic(cfg.seed)

    client_processes = []

    # First, we start all the devices.
    for device in cfg.topology.devices:
        p = Process(
            target=_start_device, args=(cfg, device.device_id), name=device.device_id
        )
        p.start()
        client_processes.append(p)

    # Then we start the controller.
    _start_controller(cfg)

    # Once we reach this point, the controller has shutdown and the experiment is over. We terminate all clients.
    for p in client_processes:
        p.terminate()
        p.join()

    _run_test_evaluation(cfg)


def _start_controller(cfg: DictConfig):
    controller = instantiate_controller(cfg)
    controller.train()


def _start_device(cfg: DictConfig, device_id: str):
    # Update the device ID to be unique.
    cfg.own_device_id = device_id

    # Fix the seed for reproducibility.
    _make_deterministic(cfg.seed)

    # Start the device.
    launch_device(cfg)


def _run_test_evaluation(cfg):
    cfg.experiment.job = "test"
    cfg.experiment.partition = "False"
    cfg.experiment.latency = None
    cfg.num_devices = 1
    device_id = cfg.topology.devices[0].device_id
    p = Process(target=_start_device, args=(cfg, device_id), name=device_id)
    p.start()

    controller = TestController(cfg)
    controller.train()

    p.terminate()
    p.join()


def _make_deterministic(seed_cfg: DictConfig):
    seed = seed_cfg.value

    # Set the seed for all libraries that we use.
    random.seed(seed)
    numpy.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)

    # This is actually not enough to make PyTorch fully deterministic. This only configures CUDA to use a deterministic
    # convolution algorithm. It does not make the selection of the algorithm itself deterministic. However, the projects
    # that I encountered online did mostly only set the following flag.
    #
    # See https://pytorch.org/docs/stable/notes/randomness.html for more information.
    if seed_cfg.torch_deterministic:
        torch.backends.cudnn.deterministic = True


if __name__ == "__main__":
    main()
